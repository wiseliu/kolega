$(document).ready(function(){
    var header = document.querySelector("header");
    enquire.register("screen and (max-width: 767px)", {
        match : function() {
            classie.add(header,"mobile");
            classie.remove(header,"desktop");
        }
    });
    enquire.register("screen and (min-width: 768px)", {
        match : function() {
            classie.remove(header,"mobile");
            classie.add(header,"desktop");
            if ($(".header").hasClass("desktop")) {
                function init() {
                    window.addEventListener('scroll', function(e){
                        var distanceY = window.pageYOffset || document.documentElement.scrollTop,
                            shrinkOn = 150;
                        if (distanceY > shrinkOn) {
                            classie.add(header,"smaller");
                        } else {
                            if (classie.has(header,"smaller")) {
                                classie.remove(header,"smaller");
                            }
                        }
                    });
                }
                window.onload = init();
            }
        }
    });

    var $hamburger = $(".hamburger"),
        $pureMenuList = $(".pure-menu-list");
        $pureMenuItemHasSub = $(".pure-menu-item.has-sub");
    $hamburger.on("click", function(e) {
        $hamburger.toggleClass("is-active");
        $pureMenuList.toggleClass("open");
    });
    $pureMenuItemHasSub.on("click", function(e) {
        $(this).find("ul").toggleClass("open");
    });


    $( ".list-location li a" ).each(function(){
        $( this ).hover(
          function() {
            $( this ).parent().parent().parent().parent().parent().find( ".content.hover" ).show();
          }, function() {
           $( this ).parent().parent().parent().parent().parent().find( ".content.hover" ).hide();
          }
        );
    });


    // $(".content").height( $(".planspackage .item").height() -  $(".planspackage .item .thumb").height());

});






