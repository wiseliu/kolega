<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blogs', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->longText('content');
			$table->string('slug');
			$table->string('tags');
			$table->string('picture_url');
			$table->string('summary');
			$table->boolean('is_active')->default(false);
            $table->integer('posted_by')->unsigned()->default(0);
            $table->foreign('posted_by')->references('id')->on('users')->onDelete('restrict');
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blogs');
	}

}
