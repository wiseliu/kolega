<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedLikesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feed_likes', function (Blueprint $table) {
            $table->integer('feed_id')->unsigned()->default(0);
            $table->foreign('feed_id')->references('id')->on('feeds')->onDelete('cascade');
            $table->integer('liked_by')->unsigned()->default(0);
            $table->foreign('liked_by')->references('id')->on('users')->onDelete('cascade');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('feed_likes');
    }

}
