<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->string('address');
            $table->string('phone_number');
			$table->double('latitude');
			$table->double('longitude');
			$table->string('slug')->unique();
            $table->integer('area_id')->unsigned()->default(0);
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locations');
	}

}
