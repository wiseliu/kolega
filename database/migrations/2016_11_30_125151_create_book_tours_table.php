<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookToursTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('book_tours', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email');
            $table->integer('location_id')->unsigned()->default(0);
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->dateTime('visit_time');
            $table->text('comment');
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('book_tours');
	}

}
