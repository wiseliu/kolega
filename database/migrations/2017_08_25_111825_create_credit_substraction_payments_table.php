<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditSubstractionPaymentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_substraction_payments', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('credit_substraction_id')->unsigned()->default(0);
            $table->foreign('credit_substraction_id')->references('id')->on('credit_substractions')->onDelete('cascade');
            $table->double('amount');
            $table->boolean('is_taken_from_permanent_credit');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_substraction_payments');
	}

}
