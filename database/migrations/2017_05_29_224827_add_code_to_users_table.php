<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeToUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->string('ktp_number')->unique()->nullable();
            $table->dateTime('promo_code_created_date')->nullable();
			$table->string('promo_code')->unique()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('users', function(Blueprint $table)
		{
            $table->dropColumn('ktp_number');
            $table->dropColumn('promo_code_created_date');
            $table->dropColumn('promo_code');
		});
	}

}
