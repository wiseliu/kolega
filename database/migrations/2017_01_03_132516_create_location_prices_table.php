<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationPricesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('location_prices', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('price_text');
            $table->string('icon_code');
            $table->string('icon_note')->nullable();
            $table->integer('location_id')->unsigned()->default(0);
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('location_prices');
	}

}
