<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserKolegasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_kolegas', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_link1')->unsigned()->default(0);
            $table->foreign('user_link1')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_link2')->unsigned()->default(0);
            $table->foreign('user_link2')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('confirmed')->default(0);
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_kolegas');
	}

}
