<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentLikesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('comment_likes', function(Blueprint $table)
		{
            $table->integer('comment_id')->unsigned()->default(0);
            $table->foreign('comment_id')->references('id')->on('feed_comments')->onDelete('cascade');
            $table->integer('liked_by')->unsigned()->default(0);
            $table->foreign('liked_by')->references('id')->on('users')->onDelete('cascade');
            $table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('comment_likes');
	}

}
