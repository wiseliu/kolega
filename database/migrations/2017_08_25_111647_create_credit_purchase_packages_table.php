<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditPurchasePackagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_purchase_packages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
            $table->string('description')->nullable();
            $table->double('price');
            $table->double('credit_amount');
            $table->integer('status');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_purchase_packages');
	}

}
