<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('email')->unique();
			$table->string('password');
			$table->string('firstname');
			$table->string('lastname');
            $table->string('company')->nullable();
            $table->string('organization')->nullable();
            $table->string('job_title')->nullable();
			$table->enum('gender',array('M','F'));
			$table->date('date_of_birth');
			$table->string('phone_number')->unique();
			$table->string('address');
            $table->string('photo_url')->nullable();
            $table->boolean('is_verified')->default(false);
            $table->string('verification_code')->nullable();
            $table->string('privilege');
			$table->rememberToken();
			$table->nullableTimestamps();
            $table->string('about')->nullable();
            $table->string('interest')->nullable();
            $table->string('web')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
