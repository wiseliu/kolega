<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('feed_comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('comment');
            $table->integer('feed_id')->unsigned()->default(0);
            $table->foreign('feed_id')->references('id')->on('feeds')->onDelete('cascade');
            $table->integer('posted_by')->unsigned()->default(0);
            $table->foreign('posted_by')->references('id')->on('users')->onDelete('cascade');
			$table->nullableTimestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('feed_comments');
	}

}
