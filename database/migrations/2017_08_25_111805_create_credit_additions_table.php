<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditAdditionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('credit_additions', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('credit_purchase_package_id')->unsigned()->default(0);
            $table->foreign('credit_purchase_package_id')->references('id')->on('credit_purchase_packages')->onDelete('cascade');
            $table->integer('location_id')->unsigned()->default(0);
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->double('amount');
            $table->date('valid_until')->nullable();
            $table->boolean('is_permanent');
            $table->timestamps();
            $table->string('created_by');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('credit_additions');
	}

}
