<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model {

    protected $table = 'locations';
    protected $fillable = ['name', 'slug', 'latitude', 'longitude', 'phone_number', 'address', 'description', 'area_id'];

    public function amenities()
    {
        return $this->belongsToMany('App\Amenity')->withTimestamps();
    }

    public function photos()
    {
        return $this->hasMany('App\LocationPhoto');
    }

    public function prices()
    {
        return $this->hasMany('App\LocationPrice');
    }

    public function area(){
        return $this->belongsTo('App\Area');
    }

    public function creditAdditions(){
        return $this->hasMany('App\CreditAddition');
    }

    public function creditSubstractions(){
        return $this->hasMany('App\CreditSubstraction');
    }

}
