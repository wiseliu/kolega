<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProventMember extends Model {

    protected $table = 'provent_members';
    protected $fillable = ['provent_id','user_id','approved'];

    public function provent(){
        return$this->belongsTo('App\Provent');
    }

    public function user(){
        return$this->belongsTo('App\User');
    }

}
