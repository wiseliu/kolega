<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CommentLike extends Model {

    protected $table = 'comment_likes';

    protected $fillable = ['comment_id','liked_by'];

    public function comment(){
        return $this->belongsTo('App\FeedComment');
    }

}
