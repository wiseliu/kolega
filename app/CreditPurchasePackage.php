<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditPurchasePackage extends Model {

    protected $table = 'credit_purchase_packages';

    protected $fillable = ['name', 'description', 'price','credit_amount', 'status'];

    public $timestamps = false;

    public function creditAdditions()
    {
        return $this->hasMany('App\CreditAddition');
    }

}
