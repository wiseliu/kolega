<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{

    protected $table = 'feeds';

    protected $fillable = ['content', 'img_url', 'posted_by'];

    public function user()
    {
        return $this->belongsTo('App\User', 'posted_by');
    }

    public function comments()
    {
        return $this->hasMany('App\FeedComment', 'feed_id');
    }

    public function likes()
    {
        return $this->hasMany('App\FeedLike', 'feed_id');
    }

    public function isLikedBy($feed_id, $user_id)
    {
        $count = FeedLike::where('liked_by', $user_id)->where('feed_id', $feed_id)->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function commentsCount()
    {
        return $this->hasMany('App\FeedComment')->selectRaw('feed_id, count(feed_id) as comments_count')->groupBy('feed_id');
    }

    public function likesCount()
    {
        return $this->hasMany('App\FeedLike')->selectRaw('feed_id, count(feed_id) as likes_count')->groupBy('feed_id');
    }
}
