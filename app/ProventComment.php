<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProventComment extends Model {

	protected $table = 'provent_comments';
	protected $fillable = ['provent_id','user_id','comment'];

	public function provent(){
	    return$this->belongsTo('App\Provent');
    }

    public function user(){
        return$this->belongsTo('App\User');
    }

}
