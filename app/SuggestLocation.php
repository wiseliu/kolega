<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class SuggestLocation extends Model {

    protected $table = 'suggest_locations';
    protected $fillable = ['name', 'email', 'location'];

}
