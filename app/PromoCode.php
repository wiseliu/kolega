<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class PromoCode extends Model {

    protected $table = 'promo_codes';
    protected $fillable = ['promo_code', 'user_id'];

    public function code(){
        return $this->belongsTo('App\User', 'promo_code', 'promo_code');
    }

    public function user(){
        return $this->belongsTo('App\User', 'id', 'user_id');
    }

}
