<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class UserKolega extends Model
{

    protected $table = 'user_kolegas';
    protected $fillable = ['user_link1', 'user_link2', 'confirmed'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
