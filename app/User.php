<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['email', 'password', 'verification_code', 'firstname', 'lastname', 'company', 'organization', 'is_verified', 'gender', 'date_of_birth', 'phone_number', 'photo_url','address', 'privilege', 'about', 'web', 'interest', 'ktp_number', 'promo_code', 'promo_code_created_date'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'verification_code'];

    public function userKolega(){
        return $this->hasMany('App\UserKolega','user_link1');
    }

    public function feeds(){
        return $this->hasMany('App\Feed','posted_by');
    }

    public function feedComment(){
        return $this->hasMany('App\FeedComment','posted_by');
    }

    public function feedLikes(){
        return $this->hasMany('App\FeedLike','liked_by');
    }

    public function commentLikes(){
        return $this->hasMany('App\CommentLike','liked_by');
    }

    public function members(){
        return $this->belongsToMany('App\Provent','provent_members');
    }

    public function provents(){
        return $this->hasMany('App\Provent');
    }

    public function promoCodes(){
        return $this->hasMany('App\PromoCode','promo_code','promo_code');
    }

    public function creditAdditions(){
        return $this->hasMany('App\CreditAddition');
    }

    public function creditSubscriptionLogs(){
        return $this->hasMany('App\CreditSubscriptionLog');
    }

    public function creditSubstractions(){
        return $this->hasMany('App\CreditSubstraction');
    }

}
