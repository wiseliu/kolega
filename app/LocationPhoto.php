<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPhoto extends Model {

    protected $table = 'location_photos';
    protected $fillable = ['name', 'url', 'location_id'];

    public function location(){
        return $this->belongsTo('App\Location');
    }

}
