<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditSubstraction extends Model {

    protected $table = 'credit_substractions';

    protected $fillable = ['user_id', 'credit_usage_package_id', 'location_id','total_amount', 'valid_until', 'created_by'];

    public $timestamps = [ "created_at" ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function creditUsagePackage()
    {
        return $this->belongsTo('App\CreditUsagePackage');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

    public function payments()
    {
        return $this->hasMany('App\CreditSubstractionPayment');
    }

}
