<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedComment extends Model {

    protected $table = 'feed_comments';

    protected $fillable = ['comment','posted_by','feed_id'];

    public function feed(){
        return $this->belongsTo('App\Feed','feed_id');
    }

    public function user(){
        return $this->belongsTo('App\User','posted_by');
    }

    public function likes(){
        return $this->hasMany('App\CommentLike','comment_id');
    }

    public function isLikedBy($comment_id, $user_id){
        $count = CommentLike::where('liked_by',$user_id)->where('comment_id',$comment_id)->count();
        if($count > 0){
            return true;
        } else {
            return false;
        }
    }

}
