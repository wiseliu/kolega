<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedLike extends Model {

    protected $table = 'feed_likes';

    protected $fillable = ['feed_id', 'liked_by'];

    public function feed(){
        return $this->belongsTo('App\Feed');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

}