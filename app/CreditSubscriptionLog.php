<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditSubscriptionLog extends Model {

    protected $table = 'credit_subscription_logs';

    protected $fillable = ['user_id', 'valid_until', 'created_by'];

    public $timestamps = [ "created_at" ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
