<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model {

    protected $table = 'blogs';
    protected $fillable = ['title', 'slug', 'content', 'summary', 'tags', 'is_active', 'picture_url', 'posted_by'];

}
