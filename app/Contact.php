<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $table = 'contacts';
    protected $fillable = ['name', 'email', 'company', 'location_id', 'office_size', 'question'];

}
