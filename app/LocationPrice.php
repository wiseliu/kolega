<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationPrice extends Model {

    protected $table = 'location_prices';
    protected $fillable = ['name', 'price_text', 'icon_code', 'icon_note', 'location_id'];

    public function location(){
        return $this->belongsTo('App\Location');
    }

}
