<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditSubstractionPayment extends Model {

    protected $table = 'credit_substraction_payments';

    protected $fillable = ['credit_substraction_id', 'amount', 'is_taken_from_permanent_credit'];

    public $timestamps = false;

    public function creditSubstraction()
    {
        return $this->belongsTo('App\CreditSubstraction');
    }

}
