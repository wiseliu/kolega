<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditUsagePackage extends Model {

    protected $table = 'credit_usage_packages';

    protected $fillable = ['name', 'description', 'price','credit_amount', 'hour_duration', 'status'];

    public $timestamps = false;

    public function creditSubstractions()
    {
        return $this->hasMany('App\CreditSubstraction');
    }

}
