<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BookTour extends Model {

    protected $table = 'book_tours';
    protected $fillable = ['name', 'email', 'location_id', 'comment', 'visit_time'];

}
