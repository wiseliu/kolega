<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model {

    protected $table = 'amenities';

    protected $fillable = ['name', 'description', 'icon_code'];

    public function locations()
    {
        return $this->belongsToMany('App\Location')->withTimestamps();
    }

}
