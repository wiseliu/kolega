<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class CreditAddition extends Model {

    protected $table = 'credit_additions';

    protected $fillable = ['user_id', 'credit_purchase_package_id', 'location_id','amount', 'valid_until', 'is_permanent', 'created_by'];

    public $timestamps = [ "created_at" ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function creditPurchasePackage()
    {
        return $this->belongsTo('App\CreditPurchasePackage');
    }

    public function location()
    {
        return $this->belongsTo('App\Location');
    }

}
