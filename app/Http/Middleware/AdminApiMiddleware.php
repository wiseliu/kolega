<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;


class AdminApiMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->privilege == 'admin' && Auth::user()->is_verified) {
                return $next($request);
            } else if (Auth::user()->privilege == 'user' && Auth::user()->is_verified) {
                return Response::json(array('success' => false, 'message' => 'insufficient privilege'), 200);
            }
        }

        return Response::json(array('success' => false, 'message' => 'insufficient privilege'), 200);
    }

}
