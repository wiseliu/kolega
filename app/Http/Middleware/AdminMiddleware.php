<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
	    if(Auth::check()){
            if (strpos(Auth::user()->privilege, 'admin') !== false && Auth::user()->is_verified){
                return $next($request);
            } else if(Auth::user()->privilege == 'user' && Auth::user()->is_verified){
                return redirect('community');
            }
        }

        return redirect('login');
	}

}
