<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{

    private $openRoutes = [
        'auth/facebookAndroid',
        'auth/googleAndroid',
        'user/apiLogin',
        'user-api/list',
        'user-api/getUser',
        'user-api/logout',
        'user-api/createUser',
        'user-api/addKolega',
        'user-api/bookSpace',
        'user-api/bookMeetingroomEventspace',
        'user-api/listKolega/{user_id}',
        'user-api/verify/{confirmationCode}',
        'feed-api/list',
        'feed-api/getFeed',
        'feed-api/create',
        'feed-api/update',
        'feed-api/delete',
        'feed-api/likeFeed',
        'feed-api/unlikeFeed',
        'feed-api/likeComment',
        'feed-api/unlikeComment',
        'feed-api/createComment',
        'provent-api/create',
        'provent-api/list',
        'provent-api/getProvent',
        'provent-api/joinProvent',
        'provent-api/rejectProvent',
        'provent-api/approveProvent',
        'provent-api/rejectMember',
        'provent-api/approveMember',
        'provent-api/getProject',
        'provent-api/getEvent',
        'provent-api/joinProvent',
        'provent-api/commentProvent',
        'profile-api/updateDetail',
        'profile-api/changePassword',
        'profile-api/changeProfileImage',
    ];

//modify this function
    public function handle($request, Closure $next)
    {
        //add this condition
        foreach ($this->openRoutes as $route) {

            if ($request->is($route)) {
                return $next($request);
            }
        }

        return parent::handle($request, $next);
    }

}
