<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'WelcomeController@index']);
//Route::get('about', 'WelcomeController@about');
Route::get('plans', 'WelcomeController@plans');
Route::get('privacy-policy', 'WelcomeController@privacypolicy');
Route::get('terms', 'WelcomeController@terms');
Route::get('faq', 'WelcomeController@faq');
//Route::get('career', 'WelcomeController@career');
Route::get('download/application-form', 'WelcomeController@downloadAppForm');

Route::get('login', ['as' => 'login', 'uses' => 'UserController@index']);
Route::get('user/verify/{confirmationCode}', 'UserController@verify');
Route::post('register', 'UserController@store');
Route::post('login', 'UserController@doLogin');
Route::get('logout', 'UserController@logout');
Route::get('auth/facebook', 'Auth\AuthController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleFacebookCallback');
Route::post('auth/facebookAndroid', 'UserApiController@facebookAndroidLogin');
Route::get('auth/google', 'Auth\AuthController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\AuthController@handleGoogleCallback');
Route::post('auth/googleAndroid', 'UserApiController@googleAndroidLogin');

Route::get('location', 'LocationController@home');
Route::get('location/{slug}', 'LocationController@tempIndex');
//Route::get('location/{slug}', 'LocationController@index');

//Route::get('plan/{slug}', 'PlanController@index');

Route::get('blog', 'NewsEventController@index');
Route::get('blog/{slug}', 'NewsEventController@show');

Route::get('contact', 'ContactController@index');
Route::post('contact/save', 'ContactController@store');
Route::post('contact/suggestLocation', 'ContactController@suggestLocation');

Route::get('community', 'MemberfeedController@index');
Route::get('provent', 'ProventController@index');
Route::get('provent/events', 'ProventController@getAllEvent');
Route::get('provent/projects', 'ProventController@getAllProject');

Route::group(['middleware' => 'community'], function () {
    Route::get('booking', 'PlanController@book');
    Route::post('booking', 'PlanController@bookSave');

    Route::post('community/post', 'MemberfeedController@store');
    Route::post('community/editFeed', 'MemberfeedController@editFeed');
    Route::post('community/deleteFeed', 'MemberfeedController@deleteFeed');
    Route::post('community/likeFeed', 'MemberfeedController@likeFeed');
    Route::post('community/unlikeFeed', 'MemberfeedController@unlikeFeed');
    Route::post('community/postComment', 'MemberfeedController@storeComment');
    Route::post('community/likeComment', 'MemberfeedController@likeComment');
    Route::post('community/unlikeComment', 'MemberfeedController@unlikeComment');

    Route::get('community/{user_id}', 'ProfileController@index');
    Route::post('community/changePassword', 'ProfileController@changePassword');
    Route::post('community/editProfile', 'ProfileController@editProfile');
    Route::post('community/uploadProfilePicture', 'ProfileController@uploadProfilePicture');
    Route::post('community/suggestFriend', 'ProfileController@suggestFriend');

    Route::get('provent/search', 'ProventController@search');
    
    Route::get('provent/create', 'ProventController@showForm');
    Route::post('provent/create', 'ProventController@store');
    Route::post('provent/join', 'ProventController@joinProvent');
    Route::get('provent/{provent_id}', 'ProventController@show');
    Route::post('provent/{provent_id}', 'ProventController@addComment');
    Route::get('provent/{provent_id}/edit', 'ProventController@edit');
    Route::post('provent/{provent_id}/edit', 'ProventController@update');
    
    Route::put('provent_action/{provent_id}/{user_id}/approve', 'ProventController@approveMember');
    Route::post('provent_action/{provent_id}/{user_id}/reject', 'ProventController@rejectMember');
});

Route::group(['middleware' => 'admin'], function () {
    Route::get('admin', 'AdminController@index');

    Route::get('admin/areas', 'AreaController@index');
    Route::post('admin/areas/add', 'AreaController@store');
    Route::put('admin/areas/{id}/edit', 'AreaController@update');
    Route::delete('admin/areas/{id}/delete', 'AreaController@destroy');

    Route::get('admin/amenities', 'AmenityController@index');
    Route::post('admin/amenities/add', 'AmenityController@store');
    Route::put('admin/amenities/{id}/edit', 'AmenityController@update');
    Route::delete('admin/amenities/{id}/delete', 'AmenityController@destroy');

    Route::get('admin/places/add', 'LocationController@create');
    Route::post('admin/places/add', 'LocationController@store');
    Route::get('admin/places/edit/{id}', 'LocationController@edit');
    Route::put('admin/places/photos/{id}/delete', 'LocationController@deletePhoto');
    Route::post('admin/places/{id}/edit', 'LocationController@update');
    Route::put('admin/places/{id}/delete', 'LocationController@destroy');

    Route::get('admin/user', 'UserController@viewAdmin');
    Route::put('admin/user/{id}/editPrivilege', 'UserController@editPrivilege');
    Route::put('admin/user/{id}/editKTP', 'UserController@editKTP');
    Route::put('admin/user/{id}/registerKTP', 'UserController@registerKTP');

    Route::get('admin/credit/purchasemenu', 'CreditController@purchaseMenu');
    Route::post('admin/credit/purchasemenu/add', 'CreditController@storePurchaseMenu');
    Route::put('admin/credit/purchasemenu/{id}/edit', 'CreditController@updatePurchaseMenu');
    Route::delete('admin/credit/purchasemenu/{id}/delete', 'CreditController@destroyPurchaseMenu');

    Route::get('admin/credit/usagemenu', 'CreditController@usageMenu');
    Route::post('admin/credit/usagemenu/add', 'CreditController@storeUsageMenu');
    Route::put('admin/credit/usagemenu/{id}/edit', 'CreditController@updateUsageMenu');
    Route::delete('admin/credit/usagemenu/{id}/delete', 'CreditController@destroyUsageMenu');

    Route::get('admin/user/credits', 'UserController@creditList');
    Route::post('admin/user/credits/subscribe', 'UserController@subscribe');
    Route::post('admin/user/credits/topup', 'UserController@topup');
    Route::post('admin/user/credits/usecredit', 'UserController@usecredit');
    Route::get('admin/user/{user_id}/creditlogs', 'UserController@creditLogs');
    Route::get('admin/user/{user_id}/creditlogs/exportCsv', 'UserController@exportCsv');

    Route::get('admin/newsevent/add', 'NewsEventController@create');
    Route::post('admin/newsevent/add', 'NewsEventController@store');
    Route::get('admin/newsevent/show', 'NewsEventController@viewAdmin');
    Route::get('admin/newsevent/edit/{id}', 'NewsEventController@edit');
    Route::post('admin/newsevent/{id}/edit', 'NewsEventController@update');
    Route::delete('admin/newsevent/{id}/delete', 'NewsEventController@destroy');

    Route::get('admin/tourbooked', 'BookTourController@index');
    Route::get('admin/contact', 'ContactController@viewAdmin');
    
    Route::get('admin/provent/', 'ProventController@getAll');
    Route::get('admin/provent/nonapproved', 'ProventController@getAllNonApprovedProvent');
    
    Route::put('admin/provent/{provent_id}/approve', 'ProventController@approveProventAJAX');
    Route::put('admin/provent/{provent_id}/reject', 'ProventController@approveProventAJAX');
    
    Route::get('provent/{provent_id}/approve', 'ProventController@approveProvent');
    Route::get('provent/{provent_id}/reject', 'ProventController@rejectProvent');

    Route::get('admin/notification/', 'NotificationController@index');
    Route::post('admin/notification/sendAll', 'NotificationController@sendAll');
});


Route::post('location/book', 'BookTourController@store');


Route::resource('locations', 'LocationController');
Route::resource('tasks', 'TasksController');

Route::post('user/apiLogin', 'UserApiController@doLogin');
Route::get('provent/search', 'ProventController@search');
Route::post('user-api/createUser', 'UserApiController@store');

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user-api/list', 'UserApiController@index');
    Route::get('user-api/getCurrentUser', 'UserApiController@showCurrent');
    Route::get('user-api/getUser', 'UserApiController@show');
    Route::get('user-api/logout', 'UserApiController@logout');
    Route::get('user-api/decode', 'UserApiController@testDecode');
    Route::get('user-api/listKolega/{user_id}', 'UserApiController@listKolega');
    Route::get('user-api/verify/{confirmationCode}', 'UserApiController@verify');
    Route::post('user-api/addKolega', 'UserApiController@addKolega');
    Route::post('user-api/bookSpace', 'UserApiController@bookWorkspace');
    Route::post('user-api/bookMeetingroomEventspace', 'UserApiController@bookMeetingroomEventspace');
    Route::get('feed-api/list', 'FeedApiController@index');
    Route::get('feed-api/getFeed', 'FeedApiController@show');
    Route::post('feed-api/create', 'FeedApiController@store');
    Route::post('feed-api/update', 'FeedApiController@editFeed');
    Route::post('feed-api/createComment', 'FeedApiController@storeComment');
    Route::post('feed-api/likeFeed', 'FeedApiController@likeFeed');
    Route::post('feed-api/unlikeFeed', 'FeedApiController@unlikeFeed');
    Route::post('feed-api/likeComment', 'FeedApiController@likeComment');
    Route::post('feed-api/unlikeComment', 'FeedApiController@unlikeComment');
    Route::post('provent-api/create', 'ProventApiController@store');
    Route::post('provent-api/joinProvent', 'ProventApiController@joinProvent');
    Route::post('provent-api/rejectProvent', 'ProventApiController@rejectProvent');
    Route::post('provent-api/approveProvent', 'ProventApiController@approveProvent');
    Route::post('provent-api/rejectMember', 'ProventApiController@rejectMember');
    Route::post('provent-api/approveMember', 'ProventApiController@approveMember');
    Route::post('provent-api/commentProvent', 'ProventApiController@addComment');
    Route::get('provent-api/list', 'ProventApiController@index');
    Route::get('provent-api/getProvent', 'ProventApiController@show');
    Route::get('provent-api/getProject', 'ProventApiController@getProject');
    Route::get('provent-api/getEvent', 'ProventApiController@getEvent');
    Route::post('profile-api/updateDetail', 'ProfileApiController@editProfile');
    Route::post('profile-api/changePassword', 'ProfileApiController@changePassword');
    Route::post('profile-api/changeProfileImage', 'ProfileApiController@uploadProfilePicture');

});

