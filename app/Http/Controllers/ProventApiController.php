<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Provent;
use App\ProventComment;
use App\ProventMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProventApiController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $provents = Provent::with('user')->where('approved', 1)->orderBy('id', 'desc')->paginate(5)->toArray();
        return Response::json(array('success' => true, 'message' => $provents), 200);
    }

    public function getProject()
    {
        $projects = Provent::with('user')->where('approved', 1)->where('type', '=', 'project')->orderBy('id', 'desc')->paginate(5)->toArray();
        return Response::json(array('success' => true, 'message' => $projects), 200);
    }

    public function getEvent()
    {
        $events = Provent::with('user')->where('approved', 1)->where('type', '=', 'event')->orderBy('id', 'desc')->paginate(5)->toArray();
        return Response::json(array('success' => true, 'message' => $events), 200);
    }

    public function joinProvent()
    {
        $rules = [
            'provent_id' => 'required|integer',
        ];

        $input = Input::only(
            'provent_id'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $provent_id = Input::get('provent_id');
        $provent = Provent::find($provent_id);
        $user = JWTAuth::parseToken()->toUser();
        if ($provent->user->id == $user->id) {
            return Response::json(array('success' => false, 'message' => 'can not join your own project/event'), 200);
        }
        $existing_provent_member = ProventMember::where('provent_id', $provent_id)->where('user_id', $user->id)->first();
        if (isset($existing_provent_member)) {
            return Response::json(array('success' => false, 'message' => 'already join this provent'), 200);
        }
        $asd = ProventMember::create([
            'provent_id' => $provent_id,
            'user_id' => $user->id,
            'approved' => 0,
        ]);

        return Response::json(array('success' => true, 'message' => $asd), 200);
    }

    public function approveMember()
    {
        $rules = [
            'user_id' => 'required|integer',
            'provent_id' => 'required|integer',
        ];

        $input = Input::only(
            'user_id',
            'provent_id'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $id = Input::get('provent_id');
        $user_id = Input::get('user_id');
        $proventMember = ProventMember::where('provent_id', $id)->where('user_id', $user_id)->first();
        if (isset($proventMember)) {
            $proventMember->approved = 1;
            $proventMember->save();
            $user = User::find($proventMember->user_id);
            $mail_data = compact('user', 'proventMember');
            Mail::send('emails.provent_accepted', $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['user']->email)
                    ->subject('Your Project/Event Membership is Approved');
            });
            return Response::json(array('success' => true, 'message' => 'success approved'), 200);
        } else {
            return Response::json(array('success' => false, 'message' => 'you are not regitered to provent'), 200);

        }


    }

    public function rejectMember()
    {
        $rules = [
            'user_id' => 'required|integer',
            'provent_id' => 'required|integer',
        ];

        $input = Input::only(
            'user_id',
            'provent_id'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $id = Input::get('provent_id');
        $user_id = Input::get('user_id');
        $proventMember = ProventMember::where('provent_id', $id)->where('user_id', $user_id)->first();
        if (isset($proventMember)) {
            $user = User::find($proventMember->user_id);
            $mail_data = compact('user', 'proventMember');
            Mail::send('emails.provent_rejected', $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['user']->email)
                    ->subject('Your Project/Event Membership is Rejected');
            });
            $proventMember->delete();
            return Response::json(array('success' => true, 'message' => 'success rejected'), 200);
        } else {

        }

    }

    public function addComment()
    {
        $rules = [
            'comment' => 'required|string',
            'provent_id' => 'required|integer',
        ];

        $input = Input::only(
            'comment',
            'provent_id'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $user = JWTAuth::parseToken()->toUser();

        $comment = ProventComment::create([
            'comment' => Input::get('comment'),
            'provent_id' => Input::get('provent_id'),
            'user_id' => $user->id,
        ]);
        return Response::json(array('success' => true, 'message' => $comment), 200);
    }

    public function search()
    {
        $provents = Provent::search(Input::get('q'))->orderBy('id', 'desc')->get();
        return Response::json(array('success' => true, 'message' => $provents), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'start_date' => 'required|date|after:today',
            'end_date' => 'required|date|after:today',
            'type' => 'required|in:project,event',
            'user_id' => 'required|exists:users,id',
            'image' => 'mimes:png,jpeg',
            'location' => 'required|string',
        ];

        $input = Input::only(
            'name',
            'description',
            'start_date',
            'end_date',
            'type',
            'user_id',
            'image',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }

        $picture = Input::file('image');
        $user = JWTAuth::parseToken()->toUser();

        if ($picture != null) {
            $uploaded_picture_path = "";
            $destinationPath = 'img/provent';
            $filename = Hash::make($user->id) . "." . $picture->getClientOriginalExtension();
            $filename = str_replace('/', '', $filename);
            $upload_success = $picture->move($destinationPath, $filename);

            if ($upload_success) {
                $uploaded_picture_path = $destinationPath . "/" . $filename;
            }

            if ($uploaded_picture_path == "") {
                return Response::json(array('success' => false, 'message' => 'unable to upload picture'), 200);
            }
        } else {
            $uploaded_picture_path = '';
        }

        $provent = Provent::create([
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'start_date' => Input::get('start_date'),
            'end_date' => Input::get('end_date'),
            'type' => Input::get('type'),
            'user_id' => Input::get('user_id'),
            'location' => Input::get('location'),
            'approved' => 0,
            'img_url' => $uploaded_picture_path,
        ]);

        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_success', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Wait for Project/Event Confirmation');
        });

        return Response::json(array('success' => true, 'message' => $provent), 200);
    }

    public function getAllNonApprovedProvent()
    {
        $provents = Provent::with('user')->where('approved', false)->orderBy('id', 'desc')->paginate(5)->toArray();
        return Response::json(array('success' => true, 'message' => $provents), 200);
    }

    public function approveProvent()
    {
        $validator = Validator::make(Input::only('provent_id'), ['provent_id' => 'required|integer']);
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $provent = Provent::find(Input::get('provent_id'));
        if (isset($provent)) {
            $provent->approved = 1;
            $provent->save();
            $user = User::find($provent->user_id);
            $mail_data = compact('user', 'provent');
            Mail::send('emails.provent_accepted', $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['user']->email)
                    ->subject('Your Project/Event is accepted');
            });
            return Response::json(array('success' => true, 'message' => $provent), 200);
        } else {
            return Response::json(array('success' => false, 'message' => 'error on rejecting'), 200);
        }
    }

    public function rejectProvent()
    {
        $validator = Validator::make(Input::only('provent_id'), ['provent_id' => 'required|integer']);
        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $provent = Provent::find(Input::get('provent_id'));
        if (isset($provent)) {
            $user = User::find($provent->user_id);
            $mail_data = compact('user', 'provent');
            Mail::send('emails.provent_rejected', $mail_data, function ($message) use ($mail_data) {
                $message->to($mail_data['user']->email)
                    ->subject('Your Project/Event is Rejected');
            });
            $provent->delete();
            return Response::json(array('success' => true, 'message' => 'success rejected provent'), 200);
        } else {
            return Response::json(array('success' => false, 'message' => 'error on rejecting'), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show()
    {
        $id = Input::get('provent_id');
        $provent = Provent::with('proventMembers.user')->with('user')->with('approvedProventMembers.user')->with('proventComments.user')->find($id);
        return Response::json(array('success' => true, 'message' => $provent), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update()
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'location' => 'required|string',
            'start_date' => 'required|date_format:Y-m-d|after:today',
            'end_date' => 'required|date_format:Y-m-d|after:today',
            'type' => 'required|in:project,event',
            'image' => 'mimes:png,jpeg',
        ];

        $input = Input::only(
            'name',
            'description',
            'start_date',
            'end_date',
            'type',
            'image',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }

        $provent = Provent::find(Input::get('provent_id'));

        $picture = Input::file('image');

        $uploaded_picture_path = "";
        $destinationPath = 'img/provent';
        if ($picture != null) {
            if ($provent->img_url != null) {
                $file_path = public_path() . "/" . $provent->photo_url;
                File::delete($file_path);
            }
            $filename = Hash::make($provent->id) . "." . $picture->getClientOriginalExtension();
            $filename = str_replace('/', '', $filename);
            $upload_success = $picture->move($destinationPath, $filename);

            if ($upload_success) {
                $uploaded_picture_path = $destinationPath . "/" . $filename;
            }

            if ($uploaded_picture_path == "") {
                return Response::json(array('success' => false, 'message' => 'unable to upload image'), 500);
            }
        } else {
            $uploaded_picture_path = $provent->img_url;
        }


        $provent->name = Input::get('name');
        $provent->description = Input::get('description');
        $provent->location = Input::get('location');
        $provent->start_date = Input::get('start_date');
        $provent->end_date = Input::get('end_date');
        $provent->type = Input::get('type');
        $provent->img_url = $uploaded_picture_path;

        $provent->save();
        return Response::json(array('success' => true, 'message' => $provent), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        $provent = Provent::find(Input::get('provent_id'));
        if ($provent->img_url != null) {
            $file_path = public_path() . "/" . $provent->img_url;
            File::delete($file_path);
        }
        $provent->delete();
        return Response::json(array('success' => true, 'message' => 'delete success'), 200);
    }

}
