<?php namespace App\Http\Controllers;

use App\Location;

class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $locations = Location::all();
        return view('new/home', compact('locations'));
	}

    public function about()
    {
        return view('new/aboutplan');
    }

    public function plans()
    {
        $locations = Location::all();
        $locations = array_chunk($locations->toArray(), 3);

        return view('new/plans',compact('locations'));
    }
    
    public function privacypolicy()
    {
        return view('new/privacypolicy');
    }

    public function faq()
    {
        return view('new/faq');
    }

    public function terms()
    {
        return view('new/terms');
    }
    
    public function career()
    {
        return view('new/career');
    }

    public function downloadAppForm(){
        $file= public_path(). "/download/employment-application-form.docx";

        $headers = array(
            'Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        );

        return response()->download($file, 'employment-application-form.docx', $headers);
    }
}
