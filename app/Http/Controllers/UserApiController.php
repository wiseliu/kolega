<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\UserKolega;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Mail;


class UserApiController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = User::all();
        return Response::json(array('success' => true, 'message' => $users), 200);
    }

    public function viewAdmin()
    {
        $users = User::all();
        return Response::json(array('success' => true, 'message' => $users), 200);
    }

    public function logout()
    {
        Auth::logout(); // log the user out of our application
        return Response::json(array('success' => true, 'message' => 'logged out success'), 200);
    }

    public function editPrivilege($id)
    {
        $rules = [
            'privilege' => 'required|alpha'
        ];

        $input = Input::only(
            'privilege'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }

        $user = User::find($id);
        $user->privilege = Input::get('privilege');
        $user->save();

        return Response::json(array('success' => true, 'message' => 'privilege edited'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
            'firstname' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'lastname' => 'alpha',
            'gender' => 'alpha',
            'date_of_birth' => 'date|before:today',
            'phone_number' => 'required|numeric|unique:users',
            'address' => 'string'
        ];

        $input = Input::only(
            'email',
            'password',
            'password_confirmation',
            'firstname',
            'lastname',
            'company',
            'organization',
            'job_title',
            'gender',
            'date_of_birth',
            'phone_number',
            'address'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }
        
        if(Input::get('date_of_birth') == null){
            Input::merge(['date_of_birth' => strtotime('01-01-1970')]);
        }
        if(Input::get('gender') == null){
            Input::merge(['gender' => ' ']);
        }        
        if(Input::get('address') == null){
            Input::merge(['address' => ' ']);
        }

        $confirmation_code = str_random(15) . Hash::make(Input::get('email') . Input::get('phone_number')) . str_random(12);
        $confirmation_code = str_replace('/', '', $confirmation_code);

        $user = User::create([
            'email' => Input::get('email'),
            'password' => Hash::make(Input::get('password')),
            'verification_code' => $confirmation_code,
            'firstname' => ucwords(strtolower(Input::get('firstname'))),
            'lastname' => ucwords(strtolower(Input::get('lastname'))),
            'company' => Input::get('company'),
            'organization' => Input::get('organization'),
            'job_title' => Input::get('job_title'),
            'gender' => Input::get('gender'),
            'date_of_birth' => Input::get('date_of_birth'),
            'phone_number' => Input::get('phone_number'),
            'address' => Input::get('address'),
            'privilege' => 'user'
        ]);

        Mail::send('emails.verify', compact('confirmation_code'), function ($message) {
            $message->to(Input::get('email'), Input::get('firstname') . " " . Input::get('lastname'))
                ->subject('Verify your email address');
        });
        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    public function verify($confirmation_code)
    {
        if (!$confirmation_code) {
            return Response::json(array('success' => false, 'message' => 'no confirmation code'), 500);
        }
        $user = User::whereVerificationCode($confirmation_code)->first();
        if (!$user) {
            return Response::json(array('success' => false, 'message' => 'confirmation code is wrong'), 500);
        }
        $user->is_verified = true;
        $user->verification_code = null;
        $user->save();
        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function showCurrent()
    {
        $user = JWTAuth::parseToken()->toUser();
        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    public function show()
    {
        $user = User::find(Input::get('user_id'));
        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function addKolega()
    {
        $rules = [
            'user_id1' => 'required|numeric',
            'user_id2' => 'required|numeric',
        ];

        $input = Input::only(
            'user_id1',
            'user_id2'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }
        $user1 = User::find(Input::get('user_id1'));
        $user2 = User::find(Input::get('user_id2'));
        if (isset($user1) && isset($user2)) {
            $kolega = UserKolega::where('user_link1', $user1->id)->where('user_link1', $user1->id)->first();
            if (!isset($kolega)) {
                $kolega = UserKolega::create([
                    'user_link1' => $user1->id,
                    'user_link2' => $user2->id,
                    'confirmed' => 0,
                ]);
            }
            return Response::json(array('success' => true, 'message' => $kolega), 200);
        } else {
            return Response::json(array('success' => false, 'message' => 'no user with that id'), 200);
        }

    }

    public function listKolega($user_id)
    {
        $rules = [
            'user_id' => 'required|numeric',
        ];

        $input = ['user_id' => $user_id];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }
        $kolega = User::find($user_id)->with('userKolega')->first();
        return Response::json(array('success' => true, 'message' => $kolega), 200);

    }

    public function testApi()
    {
        dd(Hash::make('password'));
    }

    public function doLogin()
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];

        $input = Input::only(
            'email',
            'password'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $credentials = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'is_verified' => 1
        ];

        if (!Auth::attempt($credentials)) {
            return Response::json(array('success' => false, 'message' => 'wrong passwor or not verified'), 200);
        }

        $customData = ['isAdmin' => false];

        if (Auth::user()->privilege == 'admin') {
            $customData['isAdmin'] = true;
        }
        return JWTAuth::fromUser(Auth::user(), $customData);
    }

    public function testDecode()
    {
        return 'asd';
    }

    public function bookWorkspace()
    {
        $rules = [
            'place' => 'required|string',
            'date' => 'required|string'
        ];

        $input = Input::only(
            'place',
            'date'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $place = Input::get('place');
        $date = Input::get('date');
        $type = 'Workspace';
        $user = User::find(JWTAuth::parseToken()->toUser()->id);
        $mail_data = compact('user', 'place', 'date','type');
        Mail::send('emails.bookspace', $mail_data, function ($message) use ($mail_data) {
            $message->to("kolegajkt@gmail.com")
                ->subject('Booking Space at ' . $mail_data['place'] . ' by ' . $mail_data['user']->firstname . ' on ' . $mail_data['date']);
        });
        return Response::json(array('success' => true, 'message' => 'booking sent'), 200);
    }

    public function bookMeetingroomEventspace()
    {
        $rules = [
            'place' => 'required|string',
            'date' => 'required|string',
            'type' => 'required',
            'timefrom' => 'required',
            'timeto' => 'required',
            'pplNumber' => 'required|Integer|Min:1'
        ];

        $input = Input::only(
            'place',
            'date',
            'type',
            'timefrom',
            'timeto',
            'pplNumber'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $place = Input::get('place');
        $date = Input::get('date').' from '.Input::get('timefrom').' until '.Input::get('timeto');
        $type = Input::get('type').' for '.Input::get('pplNumber').' people';
        $user = User::find(JWTAuth::parseToken()->toUser()->id);
        $mail_data = compact('user', 'place', 'date', 'type');
        Mail::send('emails.bookspace', $mail_data, function ($message) use ($mail_data) {
            $message->to("kolegajkt@gmail.com")
                ->subject('Booking Space at ' . $mail_data['place'] . ' by ' . $mail_data['user']->firstname . ' on ' . $mail_data['date']);
        });
        return Response::json(array('success' => true, 'message' => 'booking sent'), 200);
    }

    public function facebookAndroidLogin()
    {
        $rules = [
            'id' => 'required',
            'email' => 'required|email',
            'name' => 'required',
            'gender' => 'required',
            'date' => 'required|date'
        ];

        $input = Input::only(
            'id',
            'email',
            'name',
            'gender',
            'date'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $authUser = User::where('email', Input::get('email'))->first();
        $customData = ['isAdmin' => false];

        if ($authUser){
            if ($authUser->privilege == 'admin') {
                $customData['isAdmin'] = true;
            }
            return JWTAuth::fromUser($authUser, $customData);
        }

        $gender = '';
        if(Input::get('gender') == 'male'){
            $gender = 'M';
        } else if(Input::get('gender') == 'female') {
            $gender = 'F';
        }

        $fullname_chunk = explode(" ",Input::get('name'));
        $lastname = '';
        if(sizeof($fullname_chunk) > 1){
            $lastname = $fullname_chunk[sizeof($fullname_chunk) - 1];
        }

        $firstname = '';

        for($i =0; $i < sizeof($fullname_chunk) - 1; $i++){
            $firstname .= $fullname_chunk[$i];
            if($i != sizeof($fullname_chunk) - 2){
                $firstname .= ' ';
            }
        }

        $authUser = User::create([
            'email' => Input::get('email'),
            'password' => 'social-login',
            'verification_code' => Input::get('id'),
            'firstname' => ucwords(strtolower($firstname)),
            'lastname' => ucwords(strtolower($lastname)),
            'company' => '',
            'organization' => '',
            'job_title' => '',
            'gender' => $gender,
            'date_of_birth' => Input::get('date'),
            'phone_number' => 'fb-'.Input::get('id'),
            'address' => '',
            'privilege' => 'user',
            'is_verified' => true
        ]);

        return JWTAuth::fromUser($authUser, $customData);
    }

    public function googleAndroidLogin()
    {
        $rules = [
            'id' => 'required',
            'email' => 'required|email',
            'name' => 'required'
        ];

        $input = Input::only(
            'id',
            'email',
            'name'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $authUser = User::where('email', Input::get('email'))->first();
        $customData = ['isAdmin' => false];

        if ($authUser){
            if ($authUser->privilege == 'admin') {
                $customData['isAdmin'] = true;
            }
            return JWTAuth::fromUser($authUser, $customData);
        }

        $fullname_chunk = explode(" ",Input::get('name'));
        $lastname = '';
        if(sizeof($fullname_chunk) > 1){
            $lastname = $fullname_chunk[sizeof($fullname_chunk) - 1];
        }

        $firstname = '';

        for($i =0; $i < sizeof($fullname_chunk) - 1; $i++){
            $firstname .= $fullname_chunk[$i];
            if($i != sizeof($fullname_chunk) - 2){
                $firstname .= ' ';
            }
        }

        $authUser = User::create([
            'email' => Input::get('email'),
            'password' => 'social-login',
            'verification_code' => Input::get('id'),
            'firstname' => ucwords(strtolower($firstname)),
            'lastname' => ucwords(strtolower($lastname)),
            'company' => '',
            'organization' => '',
            'job_title' => '',
            'gender' => '',
            'date_of_birth' => '1900-01-01',
            'phone_number' => 'google-'.Input::get('id'),
            'address' => '',
            'privilege' => 'user',
            'is_verified' => true
        ]);

        return JWTAuth::fromUser($authUser, $customData);
    }

}
