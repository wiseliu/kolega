<?php namespace App\Http\Controllers;

use App\BookTour;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class BookTourController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $book_tours = BookTour::all();
        return view('admin.tourbooked', compact('book_tours'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'location' => 'required|integer',
            'visit_time' => 'required|after:today',
            'comment' => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'location',
            'visit_time',
            'comment'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        BookTour::create([
            'name' => ucwords(Input::get('name')),
            'email' => Input::get('email'),
            'location_id' => Input::get('location'),
            'visit_time' => Input::get('visit_time'),
            'comment' => Input::get('comment'),
        ]);

        $name = ucwords(Input::get('name'));
        $email = Input::get('email');
        $location = Location::find(Input::get('location'));
        $visit_time = Input::get('visit_time');
        $comment = Input::get('comment');

        Mail::send('emails.booktour', compact('name','email', 'visit_time', 'location', 'comment'), function($message) {
            $message->to("kolegajkt@gmail.com", ucwords(Input::get('name')))
                ->subject('Tour Booked by '.Input::get('email'));
        });

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
