<?php namespace App\Http\Controllers;

use App\Area;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AreaController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $areas = Area::all();
        return view('admin.areas', compact('areas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash|unique:areas'
        ];

        $input = Input::only(
            'name',
            'slug'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        Area::create([
            'name' => Input::get('name'),
            'slug' => strtolower(Input::get('slug'))
        ]);

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash|unique:areas'
        ];

        $input = Input::only(
            'name',
            'slug'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            //return Redirect::back()->withInput()->withErrors($validator);
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $area = Area::find($id);
        $area->name = Input::get('name');
        $area->slug = strtolower(Input::get('slug'));
        $area->save();

        //return Redirect::back()->with('action', 'esuccess');
        return Response::json(array('success' => true), 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$area = Area::find($id);
		$area->delete();
        return Redirect::back()->with('action', 'dsuccess');
	}

}
