<?php namespace App\Http\Controllers;

use App\Amenity;
use App\Area;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use App\LocationPhoto;
use App\LocationPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class PlanController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{
        if($slug == 'coworkingarea'){
            $img = '/img/new/plan1.jpg';
            $name = "Coworking Area";
            $desc = 'Coworking Area plans are furnished for you who needs flexible space. Where you can have different tables each day. With this plan, you pick your preffered location, show up when you like, take any desk in the Coworking Area, and immediately kick-off on your work. Give a boost to your networking and collaborating chance by working on a different desk each day.';
            $access = 'Get full access, 12 hrs a day to your preferred location of workspace.<br>9am-9pm on weekdays, 9pm-5pm on weekends';
            $amenities = 'For our monthly members, you can have free drinking water, get free scans and printings, Fast-Connection Internet, One key one locker storage (please contact our team for locker availability), Free access to Events held by Kolega';
            $benefits = 'In house event, to warm up the atmosphere of our community<br><br>'.
                        'Credits at your disposal for working in other locations, or to book a meeting room. Credits given varies on membership plans<br><br>'.
                        'And many more to come.';
            $other_img1 = '/img/new/plan2.jpg';
            $other_name1 = 'Dedicated Desk';
            $other_slug1 = 'dedicateddesk';
            $other_img2 = '/img/new/plan3.jpg';
            $other_name2 = 'Office Suite';
            $other_slug2 = 'officesuite';

            return view('new/plan',compact('img','name','desc','access','amenities','benefits','other_img1','other_name1','other_slug1','other_img2','other_name2','other_slug2'));
        } else if($slug == 'dedicateddesk'){
            $img = '/img/new/plan2.jpg';
            $name = 'Dedicated Desk';
            $desc = 'The Dedicated Desk gives you, and your team, reserved desks to work on all day. This gives you, and your team, the opportunity to connect with the colleagues in the Coworking Area while still having space to do your own business on your space. With this plan, you can have a 10 hrs/mo time slot in the meeting room.';
            $access = 'Get full access, 12 hrs a day to your preferred location of workspace.<br>9am-9pm on weekdays, 9pm-5pm on weekends';
            $amenities = 'For our monthly members, you can have free drinking water, get free scans and printings, Fast-Connection Internet (for private internet connection contact our team), One key one locker storage (please contact our team for locker availability), Free access to Events held by Kolega';
            $benefits = 'In house event, to warm up the atmosphere of our community<br><br>'.
                        'Credits at your disposal for working in other locations, or to book a meeting room. Credits given varies on membership plans<br><br>'.
                        'And many more to come.';
            $other_img1 = '/img/new/plan1.jpg';
            $other_name1 = 'Coworking Area';
            $other_slug1 = 'coworkingarea';
            $other_img2 = '/img/new/plan3.jpg';
            $other_name2 = 'Office Suite';
            $other_slug2 = 'officesuite';

            return view('new/plan',compact('img','name','desc','access','amenities','benefits','other_img1','other_name1','other_slug1','other_img2','other_name2','other_slug2'));
        } else if($slug == 'officesuite'){
            $img = '/img/new/plan3.jpg';
            $name = 'Office Suite';
            $desc = 'The Office Suites in Kolega could accomodate companies with 10-100+ so you can have that feeling of togetherness while being a part of the extensive community of Kolega Coworking Space. The offices are in move-ready condition and can be customized according to your needs.';
            $access = 'Get full access, 12 hrs a day to your preferred location of workspace.<br>9am-9pm on weekdays, 9pm-5pm on weekends';
            $amenities = 'For our monthly members, you can have free drinking water, get free scans and printings, Fast-Connection Internet (for private internet connection contact our team), One key one locker storage (please contact our team for locker availability), Free access to Events held by Kolega';
            $benefits = 'In house event, to warm up the atmosphere of our community<br><br>'.
                        'Credits at your disposal for working in other locations, or to book a meeting room. Credits given varies on membership plans<br><br>'.
                        'And many more to come.';
            $other_img1 = '/img/new/plan2.jpg';
            $other_name1 = 'Dedicated Desk';
            $other_slug1 = 'dedicateddesk';
            $other_img2 = '/img/new/plan1.jpg';
            $other_name2 = 'Coworking Area';
            $other_slug2 = 'coworkingarea';

            return view('new/plan',compact('img','name','desc','access','amenities','benefits','other_img1','other_name1','other_slug1','other_img2','other_name2','other_slug2'));
        }
        return view('errors/404');
	}

	public function book()
    {
        $locations = Location::take(3)->get();
        return view('new/book',compact('locations'));
    }

    public function bookSave()
    {
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'location' => 'required|integer',
            'plan' => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'location',
            'plan'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $name = ucwords(Input::get('name'));
        $email = Input::get('email');
        $location = Location::find(Input::get('location'));
        $plan = Input::get('plan');

        Mail::send('emails.bookplan', compact('name','email', 'location', 'plan'), function($message) {
            $message->to("kolegajkt@gmail.com", ucwords(Input::get('name')))
                ->subject(Input::get('plan').' in '.Location::find(Input::get('location'))->name.' booked by '.Input::get('email'));
        });

        return Redirect::back()->with('action', 'success')->with('msg','Thank you for booking a plan in Kolega '.$location->name.'.<br>In 1 or 2 days, you will receive an availability confirmation email.<br>To contact our Kolega '.$location->name.' Team, dial '.$location->phone_number);
    }

    public function home()
    {
        $locations = Location::all();
        return view('new/locations', compact('locations'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $areas = Area::all();
	    $amenities = Amenity::all();
        return view('admin.locationAdd', compact('areas','amenities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash|unique:locations',
            'latitude' => 'required|Numeric',
            'longitude' => 'required|Numeric',
            'phone_number' => 'required|numeric',
            'address' => 'required',
            'area' => 'required|Integer',
            'description' => 'required',
            'priceList' => 'required'
        ];

        $input = Input::only(
            'name',
            'slug',
            'latitude',
            'longitude',
            'phone_number',
            'address',
            'area',
            'description',
            'amenities',
            'pictures',
            'priceList'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $pictures = Input::file('pictures');
        $pictures_count = count($pictures);
        $uploaded_pictures_path = array();
        $uploadcount = 0;
        foreach($pictures as $picture) {
            $rules = array('picture' => 'required|mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('picture'=> $picture), $rules);
            if($validator->passes()){
                $destinationPath = 'img/location';
                $filename = strtolower(Input::get('slug'))."_".$uploadcount.".".$picture->getClientOriginalExtension();
                $upload_success = $picture->move($destinationPath, $filename);
                if($upload_success) {
                    $uploaded_pictures_path[$uploadcount] = $destinationPath."/".$filename;
                    $uploadcount++;
                }
            }
        }

        if($uploadcount != $pictures_count){
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $location = Location::create([
            'name' => ucwords(Input::get('name')),
            'slug' => strtolower(Input::get('slug')),
            'latitude' => Input::get('latitude'),
            'longitude' => Input::get('longitude'),
            'phone_number' => Input::get('phone_number'),
            'address' => Input::get('address'),
            'description' => Input::get('description'),
            'area_id' => Input::get('area')
        ]);

        foreach($uploaded_pictures_path as $uploaded_picture_path) {
            LocationPhoto::create([
                'name' => $location->name,
                'url' => $uploaded_picture_path,
                'location_id' => $location->id
            ]);
        }

        $amenities = Input::get('amenities');
        foreach ($amenities as $amenityId){
            $amenity = Amenity::find($amenityId);
            $location->amenities()->save($amenity);
        }

        $price_lists = explode("|", Input::get('priceList'));
        foreach ($price_lists as $price_list){
            if(strpos($price_list, '~') !== false) {
                $price_data = explode("~", $price_list);
                LocationPrice::create([
                    'name' => $price_data[0],
                    'price_text' => $price_data[1],
                    'icon_code' => $price_data[2],
                    'icon_note' => $price_data[3],
                    'location_id' => $location->id
                ]);
            }
        }

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $full_areas = Area::all();
        $full_amenities = Amenity::all();
        $location = Location::find($id);
        $location_photos = $location->photos;
        $location_prices = $location->prices;
        $amenities = $location->amenities;
        return view('admin.locationEdit', compact('full_areas','full_amenities','location','location_photos','location_prices','amenities'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash',
            'latitude' => 'required|Numeric',
            'longitude' => 'required|Numeric',
            'phone_number' => 'required|numeric',
            'address' => 'required',
            'area' => 'required|Integer',
            'description' => 'required',
            'priceList' => 'required'
        ];

        $input = Input::only(
            'name',
            'slug',
            'latitude',
            'longitude',
            'phone_number',
            'address',
            'area',
            'description',
            'amenities',
            'pictures',
            'priceList'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $location = Location::find($id);
        $location->name = ucwords(Input::get('name'));
        $location->slug = strtolower(Input::get('slug'));
        $location->latitude = Input::get('latitude');
        $location->longitude = Input::get('longitude');
        $location->phone_number = Input::get('phone_number');
        $location->address = Input::get('address');
        $location->description = Input::get('description');
        $location->area_id = Input::get('area');

        $location->save();

        $pictures = Input::file('pictures');
        $pictures_count = count($pictures);

        if($pictures[0] != null){
            if($pictures_count > 0 ){
                $old_photos = $location->photos;
                foreach($old_photos as $old_photo){
                    $file_path = public_path()."/".$old_photo->url;
                    File::delete($file_path);
                    $old_photo->delete();
                }
            }

            $uploaded_pictures_path = array();
            $uploadcount = 0;
            foreach($pictures as $picture) {
                $rules = array('picture' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('picture'=> $picture), $rules);
                if($validator->passes()){
                    $destinationPath = 'img/location';
                    $filename = strtolower(Input::get('slug'))."_".$uploadcount.".".$picture->getClientOriginalExtension();
                    $upload_success = $picture->move($destinationPath, $filename);
                    if($upload_success) {
                        $uploaded_pictures_path[$uploadcount] = $destinationPath."/".$filename;
                        $uploadcount++;
                    }
                }
            }

            if($uploadcount != $pictures_count){
                return Redirect::back()->withInput()->withErrors($validator);
            }

            foreach($uploaded_pictures_path as $uploaded_picture_path) {
                LocationPhoto::create([
                    'name' => $location->name,
                    'url' => $uploaded_picture_path,
                    'location_id' => $location->id
                ]);
            }
        }


        $location->amenities()->detach();

        $amenities = Input::get('amenities');
        foreach ($amenities as $amenityId){
            $amenity = Amenity::find($amenityId);
            $location->amenities()->save($amenity);
        }

        $location->prices()->delete();

        $price_lists = explode("|", Input::get('priceList'));
        foreach ($price_lists as $price_list){
            if(strpos($price_list, '~') !== false) {
                $price_data = explode("~", $price_list);
                LocationPrice::create([
                    'name' => $price_data[0],
                    'price_text' => $price_data[1],
                    'icon_code' => $price_data[2],
                    'icon_note' => $price_data[3],
                    'location_id' => $location->id
                ]);
            }
        }

        return Redirect::back()->with('action', 'success');
	}

    public function deletePhoto($id)
    {
        //$photo = LocationPhoto::find($id);
        //File::delete($photo->url);
        //$photo->delete();
        return Response::json(array('success' => true), 200);
        /*if(File::delete($photo->url) && $photo->delete()){
            return Response::json(array('success' => true), 200);
        } else {
            return Response::json(array('success' => false), 200);
        }*/
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$location = Location::find($id);
        $old_photos = $location->photos;
        foreach($old_photos as $old_photo){
            $file_path = public_path()."/".$old_photo->url;
            File::delete($file_path);
        }
        if($location->delete()){
            return Response::json(array('success' => true), 200);
        } else {
            return Response::json(array('success' => false), 200);
        }
	}

}
