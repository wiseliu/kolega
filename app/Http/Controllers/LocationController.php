<?php namespace App\Http\Controllers;

use App\Amenity;
use App\Area;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use App\LocationPhoto;
use App\LocationPrice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LocationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($slug)
	{
		$location = Location::where('slug',$slug)->first();
        return view('new/location', compact('location'));
	}

    public function tempIndex($slug)
    {
        $locations = Location::all();
        $location = Location::where('slug',$slug)->first();
        $otherLocations = Location::where('slug','!=',$slug)->orderBy('id')->get();
        if($slug == 'tebet'){
            $desc = 'Terletak di salah satu Lokasi nongkrong di Tebet. Kolega di Tebet memberikan Anda suasana kerja di tengah keramaian dunia urban Jakarta. Kolega Tebet berlokasi di hook antara jalan Tebet Raya dan Tebet Utara Dalam. Dimana disekitarnya terdapat berbagai macam kuliner dan juga pusat niaga. Berada diatas sebuah restoran, Kolega tebet memiliki dua lantai, dimana satu lantai berisi ruang meeting/conference, dan terdapat coworking area dan working space di lantai tiga dengan layout commons dimana Anda dapat bekerja dengan tenang dan mendapat kesempatan untuk memperluas jaringan dan menciptakan kolaborasi antar member.';
            $monfri = '10am-9pm';
            $satsun = '10am-5pm';
            $phone = '+6221-8355-216';
            $facilities = array(
                    '<i class="fa fa-wifi fa-3x" aria-hidden="true"></i><p>Fast Wifi</p>',
                    '<i class="fa fa-spoon fa-3x" aria-hidden="true"></i><p>Pantry</p>',
                    '<i class="fa fa-lock fa-3x" aria-hidden="true"></i><p>Locker</p>',
                    '<i class="fa fa-users fa-3x" aria-hidden="true"></i><p>Meeting Rooms</p>',
                    '<i class="fa fa-cutlery fa-3x" aria-hidden="true"></i><p>Right above a Cafe</p>',
                    '<i class="fa fa-beer fa-3x" aria-hidden="true"></i><p>Free flow drinking water</p>',
                    '<i class="fa fa-print fa-3x" aria-hidden="true"></i><p>Printing</p>',
                '<i class="fa fa-road fa-3x" aria-hidden="true"></i><p>Food district</p>');

            $dailyAccessPrice = 'Rp 125.000,-';
            $coworkingAreaPrice = 'Rp 1.430.000,-';

            $meetingRoom1 = (object) [
                'size' => 'Small',
                'price' => 'Rp 165.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 6 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRoom2 = (object) [
                'size' => 'Medium',
                'price' => 'Rp 220.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 8 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRoom3 = (object) [
                'size' => 'Large',
                'price' => 'Rp 330.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 14 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRooms = array($meetingRoom1,$meetingRoom2,$meetingRoom3);

            $eventSpace1 = (object)[
                'name' => 'Closed for Public Events',
                'price' => 'Rp 1.650.000,- / 2 hours',
                'desc' => 'When you want to have a rather large event that could fit up to 70 attendees. Or you want to have a yoga class. Than this is the right service for you.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water','Modular-super-modifiable space','Including whiteboard and power outlets',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];

            $eventSpace2 = (object)[
                'name' => 'Open for Public Events',
                'price' => 'Rp 1.100.000,- / 2 hours',
                'desc' => 'This plan will give you the access to use our mini-auditorium that could fit up to 30 person. This is suitable for intimate-sharing events such as coffee talks, and other light-sharing sessions.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water','Including whiteboard and power outlets',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $eventSpaces = array($eventSpace1,$eventSpace2);

            return view('new/location',compact('desc','monfri','satsun','phone','facilities','dailyAccessPrice','coworkingAreaPrice','meetingRooms','eventSpaces','location','otherLocations'));
        } else if($slug == 'senopati'){
            $desc = 'Kolega Senopati terletak di depan Lapangan Blok S, Jakarta Selatan. Tepat di depan Halte Lapangan Blok S, Kolega Senopati memiliki kesan refreshing. Dapatkan inspirasi dengan desain ruangan yang cerah di Kolega Senopati. Apabila merasa lapar, di sekitar Kolega Senopati terdapat berbagai macam kuliner, dari fine dining hingga jajanan kaki lima tersedia di sepanjang Jalan Senopati dan Pujasera Lapangan Blok S.';
            $monfri = '9am-9pm';
            $satsun = '9am-5pm';
            $phone = '+6221-2751-6906';
            $facilities = array(
                '<i class="fa fa-wifi fa-3x" aria-hidden="true"></i><p>Fast Wifi</p>',
                '<i class="fa fa-spoon fa-3x" aria-hidden="true"></i><p>Pantry</p>',
                '<i class="fa fa-lock fa-3x" aria-hidden="true"></i><p>Locker</p>',
                '<i class="fa fa-users fa-3x" aria-hidden="true"></i><p>Meeting Rooms</p>',
                '<i class="fa fa-university  fa-3x" aria-hidden="true"></i><p>Private Event Space</p>',
                '<i class="fa fa-beer fa-3x" aria-hidden="true"></i><p>Free flow drinking water</p>',
                '<i class="fa fa-print fa-3x" aria-hidden="true"></i><p>Scanning & Printing</p>',
                '<i class="fa fa-road fa-3x" aria-hidden="true"></i><p>Food district & Coffee Shop Inbound</p>');

            $dailyAccessPrice = 'Rp 175.000,-';
            $coworkingAreaPrice = 'Rp 1.650.000,-';
            $dedicatedDeskPrice = 'Rp 3.300.000,-';
            $officeSuitesPrice = 'Starts from Rp 24.000.000,- / month per unit';
            $officeSuitesDesc = 'The suites in Kolega Senopati could fit from 8 person size team to 15 person size team. This is a good place, with good vibes to start your own!';

            $meetingRoom1 = (object) [
                'size' => 'Medium',
                'price' => 'Rp 220.000,- / hour',
                'desc' => 'This meeting room is suitable for up to 10 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRooms = array($meetingRoom1);

            $eventSpace1 = (object)[
                'name' => 'Closed for Public Events',
                'price' => 'Rp 1.650.000,- / 2 hours',
                'desc' => 'When you want to have a rather large event that could fit up to 70 attendees. Or you want to have a yoga class. Than this is the right service for you.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water','Modular-super-modifiable space','Including whiteboard and power outlets',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];

            $eventSpace2 = (object)[
                'name' => 'Open for Public Events',
                'price' => 'Rp 1.100.000,- / 2 hours',
                'desc' => 'This plan will give you the access to use our mini-auditorium that could fit up to 30 person. This is suitable for intimate-sharing events such as coffee talks, and other light-sharing sessions.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water','Including whiteboard and power outlets',
                    'Directly above a restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $eventSpaces = array($eventSpace1,$eventSpace2);

            return view('new/location',compact('desc','monfri','satsun','phone','facilities','dailyAccessPrice','coworkingAreaPrice','dedicatedDeskPrice','officeSuitesPrice','officeSuitesDesc','meetingRooms','eventSpaces','location','otherLocations'));
        } else if($slug == 'antasari'){
            $desc = 'Untuk coworkers yang bertempat tinggal di daerah Jakarta Selatan, Kolega Antasari memberikan Anda pilihan untuk bekerja dekat dengan Rumah. Kolega Antasari adalah Kolega ke tiga yang telah beroperasi di Jakarta. Raskan memiliki kantor dengan pemandangan dari Lantai 5  dan saat malam, bisa langsung makan di Restoran yang unik. Di Kolega Antasari, Anda dapat merasakan ekosistem pertemanan di suasana kantor. Dengan setiap lantai memiliki fungsi yang berbeda, memberikan kesempatan untuk Anda dan kawan sepekerjaan untuk berkembang.';
            $monfri = '9am-9pm';
            $satsun = '9am-5pm';
            $phone = '+6221-7591-6199';
            $facilities = array(
                '<i class="fa fa-wifi fa-3x" aria-hidden="true"></i><p>Fast Wifi</p>',
                '<i class="fa fa-spoon fa-3x" aria-hidden="true"></i><p>Pantry in Each Floor</p>',
                '<i class="fa fa-lock fa-3x" aria-hidden="true"></i><p>Locker</p>',
                '<i class="fa fa-users fa-3x" aria-hidden="true"></i><p>Meeting Rooms</p>',
                '<i class="fa fa-university  fa-3x" aria-hidden="true"></i><p>Private Event Space</p>',
                '<i class="fa fa-beer fa-3x" aria-hidden="true"></i><p>Free flow drinking water</p>',
                '<i class="fa fa-print fa-3x" aria-hidden="true"></i><p>Scanning & Printing</p>',
                '<i class="fa fa-car fa-3x" aria-hidden="true"></i><p>Parking space information, please ask our teams.</p>');

            $dailyAccessPrice = 'Rp 150.000,-';
            $coworkingAreaPrice = 'Rp 1.650.000,-';
            $officeSuitesPrice = 'Starts from Rp 7.000.000,- / month per unit';
            $officeSuitesDesc = 'The office suites in Kolega Antasari have various capacities. You can have an office for 2, 4, and 6 person per unit. These rooms are perfect if you already have multiple teams. Or maybe, you want to work remotely with your team?';

            $meetingRoom1 = (object) [
                'size' => 'Small',
                'price' => 'Rp 220.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 4 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a coffee shop and restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRoom2 = (object) [
                'size' => 'Medium',
                'price' => 'Rp 330.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 8 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a coffee shop and restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRoom3 = (object) [
                'size' => 'Large',
                'price' => 'Rp 550.000,- / 2 hours',
                'desc' => 'This meeting room is suitable for up to 14 person.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water (Self-service)','Including whiteboard and power outlet',
                    'Directly above a coffee shop and restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $meetingRooms = array($meetingRoom1,$meetingRoom2,$meetingRoom3);

            $eventSpace1 = (object)[
                'name' => 'Events',
                'price' => 'Rp 1.650.000,- / 2 hours',
                'desc' => 'When you want to have a rather large event that could fit up to 30 attendees. This event space is suitable to have panel discussions, or a small scale seminar/workshop.',
                'facilities' => array('Internet Connection','Free Flow Mineral Water','Modular-super-modifiable space','Including whiteboard and power outlets',
                    'Directly above a coffee shop and restaurant so you are able to order foods','For screen projector and flip-chart could be provided by request')
            ];
            $eventSpaces = array($eventSpace1);

            return view('new/location',compact('desc','monfri','satsun','phone','facilities','dailyAccessPrice','coworkingAreaPrice','officeSuitesPrice','officeSuitesDesc','meetingRooms','eventSpaces','location','otherLocations'));
        } else {
            $desc = 'We provide customizable offices that could accommodate up to 150 seats companies.<br><br>For more information on this location, contact us through <b>info@kolega.co</b>';
            $monfri = '9am-9pm';
            $satsun = '9am-5pm';
            $facilities = array(
                '<i class="fa fa-wifi fa-3x" aria-hidden="true"></i><p>Fast Wifi</p>',
                '<i class="fa fa-spoon fa-3x" aria-hidden="true"></i><p>Pantry in Each Floor</p>',
                '<i class="fa fa-lock fa-3x" aria-hidden="true"></i><p>Locker</p>',
                '<i class="fa fa-users fa-3x" aria-hidden="true"></i><p>Meeting Rooms</p>',
                '<i class="fa fa-university  fa-3x" aria-hidden="true"></i><p>Private Event Space</p>',
                '<i class="fa fa-beer fa-3x" aria-hidden="true"></i><p>Free flow drinking water</p>',
                '<i class="fa fa-print fa-3x" aria-hidden="true"></i><p>Scanning & Printing</p>',
                '<i class="fa fa-car fa-3x" aria-hidden="true"></i><p>Parking space information, please ask our teams.</p>');

            $dailyAccessPrice = 'Rp -';
            $coworkingAreaPrice = 'Rp -';
            $dedicatedDeskPrice = 'Rp -';
            $officeSuitesPrice = 'Rp - / month per unit';
            $officeSuitesDesc = 'Access to the selected, tailor-made, private office suite(s) for you and your team, while being able to access the coworking area to huddle-up with the community and colleagues.';

            return view('new/location',compact('desc','monfri','satsun','phone','facilities','dailyAccessPrice','coworkingAreaPrice','dedicatedDeskPrice','officeSuitesPrice','officeSuitesDesc','location','otherLocations'));
        }

        return view('errors/404');
    }

    public function home()
    {
        $locations = Location::all();
        return view('new/locations', compact('locations'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    $areas = Area::all();
	    $amenities = Amenity::all();
        return view('admin.locationAdd', compact('areas','amenities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash|unique:locations',
            'latitude' => 'required|Numeric',
            'longitude' => 'required|Numeric',
            'phone_number' => 'required|numeric',
            'address' => 'required',
            'area' => 'required|Integer',
            'description' => 'required',
            'priceList' => 'required'
        ];

        $input = Input::only(
            'name',
            'slug',
            'latitude',
            'longitude',
            'phone_number',
            'address',
            'area',
            'description',
            'amenities',
            'pictures',
            'priceList'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $pictures = Input::file('pictures');
        $pictures_count = count($pictures);
        $uploaded_pictures_path = array();
        $uploadcount = 0;
        foreach($pictures as $picture) {
            $rules = array('picture' => 'required|mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('picture'=> $picture), $rules);
            if($validator->passes()){
                $destinationPath = 'img/location';
                $filename = strtolower(Input::get('slug'))."_".$uploadcount.".".$picture->getClientOriginalExtension();
                $upload_success = $picture->move($destinationPath, $filename);
                if($upload_success) {
                    $uploaded_pictures_path[$uploadcount] = $destinationPath."/".$filename;
                    $uploadcount++;
                }
            }
        }

        if($uploadcount != $pictures_count){
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $location = Location::create([
            'name' => ucwords(Input::get('name')),
            'slug' => strtolower(Input::get('slug')),
            'latitude' => Input::get('latitude'),
            'longitude' => Input::get('longitude'),
            'phone_number' => Input::get('phone_number'),
            'address' => Input::get('address'),
            'description' => Input::get('description'),
            'area_id' => Input::get('area')
        ]);

        foreach($uploaded_pictures_path as $uploaded_picture_path) {
            LocationPhoto::create([
                'name' => $location->name,
                'url' => $uploaded_picture_path,
                'location_id' => $location->id
            ]);
        }

        $amenities = Input::get('amenities');
        foreach ($amenities as $amenityId){
            $amenity = Amenity::find($amenityId);
            $location->amenities()->save($amenity);
        }

        $price_lists = explode("|", Input::get('priceList'));
        foreach ($price_lists as $price_list){
            if(strpos($price_list, '~') !== false) {
                $price_data = explode("~", $price_list);
                LocationPrice::create([
                    'name' => $price_data[0],
                    'price_text' => $price_data[1],
                    'icon_code' => $price_data[2],
                    'icon_note' => $price_data[3],
                    'location_id' => $location->id
                ]);
            }
        }

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $full_areas = Area::all();
        $full_amenities = Amenity::all();
        $location = Location::find($id);
        $location_photos = $location->photos;
        $location_prices = $location->prices;
        $amenities = $location->amenities;
        return view('admin.locationEdit', compact('full_areas','full_amenities','location','location_photos','location_prices','amenities'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'slug' => 'required|AlphaDash',
            'latitude' => 'required|Numeric',
            'longitude' => 'required|Numeric',
            'phone_number' => 'required|numeric',
            'address' => 'required',
            'area' => 'required|Integer',
            'description' => 'required',
            'priceList' => 'required'
        ];

        $input = Input::only(
            'name',
            'slug',
            'latitude',
            'longitude',
            'phone_number',
            'address',
            'area',
            'description',
            'amenities',
            'pictures',
            'priceList'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $location = Location::find($id);
        $location->name = ucwords(Input::get('name'));
        $location->slug = strtolower(Input::get('slug'));
        $location->latitude = Input::get('latitude');
        $location->longitude = Input::get('longitude');
        $location->phone_number = Input::get('phone_number');
        $location->address = Input::get('address');
        $location->description = Input::get('description');
        $location->area_id = Input::get('area');

        $location->save();

        $pictures = Input::file('pictures');
        $pictures_count = count($pictures);

        if($pictures[0] != null){
            if($pictures_count > 0 ){
                $old_photos = $location->photos;
                foreach($old_photos as $old_photo){
                    $file_path = public_path()."/".$old_photo->url;
                    File::delete($file_path);
                    $old_photo->delete();
                }
            }

            $uploaded_pictures_path = array();
            $uploadcount = 0;
            foreach($pictures as $picture) {
                $rules = array('picture' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('picture'=> $picture), $rules);
                if($validator->passes()){
                    $destinationPath = 'img/location';
                    $filename = strtolower(Input::get('slug'))."_".$uploadcount.".".$picture->getClientOriginalExtension();
                    $upload_success = $picture->move($destinationPath, $filename);
                    if($upload_success) {
                        $uploaded_pictures_path[$uploadcount] = $destinationPath."/".$filename;
                        $uploadcount++;
                    }
                }
            }

            if($uploadcount != $pictures_count){
                return Redirect::back()->withInput()->withErrors($validator);
            }

            foreach($uploaded_pictures_path as $uploaded_picture_path) {
                LocationPhoto::create([
                    'name' => $location->name,
                    'url' => $uploaded_picture_path,
                    'location_id' => $location->id
                ]);
            }
        }


        $location->amenities()->detach();

        $amenities = Input::get('amenities');
        foreach ($amenities as $amenityId){
            $amenity = Amenity::find($amenityId);
            $location->amenities()->save($amenity);
        }

        $location->prices()->delete();

        $price_lists = explode("|", Input::get('priceList'));
        foreach ($price_lists as $price_list){
            if(strpos($price_list, '~') !== false) {
                $price_data = explode("~", $price_list);
                LocationPrice::create([
                    'name' => $price_data[0],
                    'price_text' => $price_data[1],
                    'icon_code' => $price_data[2],
                    'icon_note' => $price_data[3],
                    'location_id' => $location->id
                ]);
            }
        }

        return Redirect::back()->with('action', 'success');
	}

    public function deletePhoto($id)
    {
        //$photo = LocationPhoto::find($id);
        //File::delete($photo->url);
        //$photo->delete();
        return Response::json(array('success' => true), 200);
        /*if(File::delete($photo->url) && $photo->delete()){
            return Response::json(array('success' => true), 200);
        } else {
            return Response::json(array('success' => false), 200);
        }*/
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$location = Location::find($id);
        $old_photos = $location->photos;
        foreach($old_photos as $old_photo){
            $file_path = public_path()."/".$old_photo->url;
            File::delete($file_path);
        }
        if($location->delete()){
            return Response::json(array('success' => true), 200);
        } else {
            return Response::json(array('success' => false), 200);
        }
	}

}
