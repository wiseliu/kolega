<?php namespace App\Http\Controllers;

use App\CommentLike;
use App\Feed;
use App\FeedComment;
use App\FeedLike;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class FeedApiController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $feeds = Feed::orderBy('id', 'desc')->with('user')->with('likesCount')->with('commentsCount')->with('likes')->paginate(5)->toArray();
        $asd = compact('feeds');
        return Response::json(array('success' => true, 'message' => $asd), 200);
    }

    public function testCount()
    {
        $feed = Feed::find(Input::get('feed_id'))->comments;
        return Response::json(array('success' => true, 'message' => $feed), 200);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'feed' => 'required_without:image|string',
            'posted_by' => 'required|numeric',
            'image' => 'required_without:feed|mimes:png,jpeg',
        ];

        $input = Input::only(
            'feed',
            'posted_by',
            'image'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }
        $picture = Input::file('image');
        $user = JWTAuth::parseToken()->toUser();
        $uploaded_picture_path = null;
        if ($picture != null) {
            $rules = array('image' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('image' => $picture), $rules);

            if ($validator->passes()) {
                $destinationPath = 'img/feeds';
                $filename = str_random(15) . $user->id . "." . $picture->getClientOriginalExtension();
                $upload_success = $picture->move($destinationPath, $filename);

                if ($upload_success) {
                    $uploaded_picture_path = $destinationPath . "/" . $filename;
                }
            }

            if ($uploaded_picture_path == null) {
                return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
            }

        } else {
            $uploaded_picture_path = '';
        }

        $feed = Feed::create([
            'content' => Input::get('feed'),
            'img_url' => $uploaded_picture_path,
            'posted_by' => $user->id
        ]);

        return Response::json(array('success' => true, 'feed' => $feed), 200);


    }

    public function editFeed()
    {
        $rules = [
            'feed_id' => 'required|numeric',
            'feed' => 'required_without:image|string',
            'image' => 'required_without:image|mimes:png,jpeg',
        ];

        $input = Input::only(
            'feed',
            'feed_id',
            'image'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 500);
        }

        $feed = Feed::find(Input::get('feed_id'));

        $picture = Input::file('image');

        $uploaded_picture_path = "";
        $destinationPath = 'img/feeds';
        if ($picture != null) {
            if ($feed->img_url != null) {
                $file_path = public_path() . "/" . $feed->photo_url;
                File::delete($file_path);
            }
            $filename = Hash::make($feed->id) . "." . $picture->getClientOriginalExtension();
            $filename = str_replace('/', '', $filename);
            $upload_success = $picture->move($destinationPath, $filename);

            if ($upload_success) {
                $uploaded_picture_path = $destinationPath . "/" . $filename;
            }

            if ($uploaded_picture_path == "") {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            $uploaded_picture_path = $feed->img_url;
        }

        $feed->content = Input::get('feed');
        $feed->img_url = $uploaded_picture_path;
        $feed->save();

        return Response::json(array('success' => true, 'message' => $feed), 200);
    }

    public function deleteFeed()
    {
        $feed = Feed::find(Input::get('feed_id'));

        if ($feed->img_url != null) {
            $file_path = public_path() . "/" . $feed->img_url;
            File::delete($file_path);
        }

        $feed->delete();

        return Response::json(array('success' => true, 'message' => 'feed deleted'), 200);
    }

    public function likeFeed()
    {
        $feed_like = FeedLike::where('feed_id', Input::get('feed_id'))->where('liked_by', Input::get('liked_by'))->first();
        if (isset($feed_like)) {
            return Response::json(array('success' => false, 'message' => 'already liked'), 200);
        }
        FeedLike::create([
            'feed_id' => Input::get('feed_id'),
            'liked_by' => Input::get('liked_by')
        ]);

        $likeCount = count(FeedLike::where('feed_id', Input::get('feed_id'))->get());
        $feedId = Input::get('feed_id');
        $data = compact('likeCount', 'feedId');

        return Response::json(array('success' => true, 'message' => $data), 200);
    }

    public function unlikeFeed()
    {
        $feed_like = FeedLike::where('feed_id', Input::get('feed_id'))->where('liked_by', Input::get('liked_by'));
        $feed_like->delete();
        $likeCount = count(FeedLike::where('feed_id', Input::get('feed_id'))->get());
        $feedId = Input::get('feed_id');
        $data = compact('likeCount', 'feedId');

        return Response::json(array('success' => true, 'message' => $data), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show()
    {
        $feed = Feed::with('comments.likes')->with('comments.user')->with('likes')->with('user')->find(Input::get('feed_id'));
        return Response::json(array('success' => true, 'message' => $feed), 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function likeComment()
    {
        $comment_like = CommentLike::where('comment_id', Input::get('comment_id'))->where('liked_by', Input::get('liked_by'))->first();
        if (isset($comment_like)) {
            return Response::json(array('success' => false, 'message' => 'already liked'), 200);
        }
        CommentLike::create([
            'comment_id' => Input::get('comment_id'),
            'liked_by' => Input::get('liked_by')
        ]);

        $count = count(CommentLike::where('comment_id', Input::get('comment_id'))->get());

        $commentId = Input::get('feed_id');
        $data = compact('count', 'commentId');

        return Response::json(array('success' => true, 'message' => $data), 200);
    }

    public function unlikeComment()
    {

        $comment_like = CommentLike::where('comment_id', Input::get('comment_id'))->where('liked_by', Input::get('liked_by'));
        $comment_like->delete();

        $count = count(CommentLike::where('comment_id', Input::get('comment_id'))->get());

        $commentId = Input::get('feed_id');
        $data = compact('count', 'commentId');

        return Response::json(array('success' => true, 'message' => $data), 200);
    }

    public function storeComment()
    {
        FeedComment::create([
            'comment' => Input::get('comment'),
            'feed_id' => Input::get('feed_id'),
            'posted_by' => JWTAuth::parseToken()->toUser()->id

        ]);

        $feed = Feed::with('comments')->find(Input::get('feed_id'));
        return Response::json(array('success' => true, 'message' => $feed), 200);

    }

}
