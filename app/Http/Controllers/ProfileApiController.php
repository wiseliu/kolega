<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Facades\JWTAuth;

class ProfileApiController extends Controller
{

    public function index($user_id)
    {
        $user = User::find($user_id);
        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    public function changePassword()
    {

        $rules = [
            'password' => 'required|confirmed|min:8',
            'old_password' => 'required|min:8'
        ];

        $input = Input::only(
            'password',
            'old_password',
            'password_confirmation'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $user = JWTAuth::ParseToken()->toUser();
        $credentials = [
            'email' => $user->email,
            'password' => Input::get('old_password'),
            'is_verified' => 1
        ];
        if (Auth::attempt($credentials)) {
            $user->password = Hash::make(Input::get('password'));
            $user->save();
            return Response::json(array('success' => true, 'message' => $user), 200);
        } else {
            return Response::json(array('success' => false, 'message' => 'old password missmatch'), 200);

        }


    }

    public function editProfile()
    {
        $rules = [
            'job_title' => 'string',
            'web' => 'regex:/^(http(s?):\/\/)?(www\.)+[a-zA-Z0-9\.\-\_]+(\.[a-zA-Z]{2,3})+(\/[a-zA-Z0-9\_\-\s\.\/\?\%\#\&\=]*)?$/',
            'interest' => 'string',
            'about' => 'string',
            'company' => 'string',
        ];

        $input = Input::only(
            'job_title',
            'company',
            'web',
            'interest',
            'about'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $user = JWTAuth::ParseToken()->toUser();
        $user->job_title = Input::get('job_title');
        $user->company = Input::get('company');
        $user->web = Input::get('web');
        $user->interest = Input::get('interest');
        $user->about = Input::get('about');
        $user->save();

        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    public function uploadProfilePicture()
    {
        $user = JWTAuth::ParseToken()->toUser();

        if (Input::hasFile('profile_picture')) {
            $picture = Input::file('profile_picture');
            $uploaded_picture_path = "";
            $rules = array('profile_picture' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('profile_picture' => $picture), $rules);

            if ($validator->passes()) {
                if ($user->photo_url != null) {
                    $file_path = public_path() . "/" . $user->photo_url;
                    File::delete($file_path);
                }

                $destinationPath = 'img/user';
                $filename = Hash::make($user->id) . "." . $picture->getClientOriginalExtension();
                $filename = str_replace('/', '', $filename);
                $upload_success = $picture->move($destinationPath, $filename);

                if ($upload_success) {
                    $uploaded_picture_path = $destinationPath . "/" . $filename;
                }
            }

            if ($uploaded_picture_path == "") {
                return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
            }

            $user->photo_url = $uploaded_picture_path;

        } else {
            if ($user->photo_url != null) {
                $file_path = public_path() . "/" . $user->photo_url;
                File::delete($file_path);
            }
            $user->photo_url = null;
        }

        $user->save();

        return Response::json(array('success' => true, 'message' => $user), 200);
    }

    public function suggestFriend()
    {
        $rules = [
            'email' => 'required|email'
        ];

        $input = Input::only(
            'email'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array('success' => false, 'message' => $validator->errors()), 200);
        }

        $sender = Auth::user();

        Mail::send('emails.invitation', compact('sender'), function ($message) {
            $message->to(Input::get('email'), "Kolega")
                ->subject("Invitation to Koléga");
        });

        return Response::json(array('success' => true, 'message' => 'success suggesting friend'), 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
