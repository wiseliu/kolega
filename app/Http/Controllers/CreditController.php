<?php namespace App\Http\Controllers;

use App\CreditPurchasePackage;
use App\CreditUsagePackage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class CreditController extends Controller {

	public function purchaseMenu()
	{
        $credits = CreditPurchasePackage::where('status',1)->get();
        return view('admin.creditPurchaseMenu', compact('credits'));
	}

    public function storePurchaseMenu()
    {
        $rules = [
            'name' => 'required',
            'price' => 'required|numeric|min:0',
            'credit' => 'required|numeric|min:0'
        ];

        $input = Input::only(
            'name',
            'price',
            'credit',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        CreditPurchasePackage::create([
            'name' => Input::get('name'),
            'price' => Input::get('price'),
            'credit_amount' => Input::get('credit'),
            'description' => Input::get('description'),
            'status' => 1
        ]);

        return Redirect::back()->with('action', 'success');
    }

    public function updatePurchaseMenu($id)
    {
        $rules = [
            'name' => 'required',
            'price' => 'required|numeric|min:0',
            'credit' => 'required|numeric|min:0'
        ];

        $input = Input::only(
            'name',
            'price',
            'credit',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $credit = CreditPurchasePackage::find($id);
        $credit->name = Input::get('name');
        $credit->price = Input::get('price');
        $credit->credit_amount = Input::get('credit');
        $credit->description = Input::get('description');
        $credit->save();

        return Response::json(array('success' => true), 200);
    }

    public function destroyPurchaseMenu($id)
    {
        $credit = CreditPurchasePackage::find($id);
        $credit->status = 0;
        $credit->save();
        return Redirect::back()->with('action', 'dsuccess');
    }

    public function usageMenu()
    {
        $credits = CreditUsagePackage::where('status',1)->get();
        return view('admin.creditUsageMenu', compact('credits'));
    }

    public function storeUsageMenu()
    {
        $rules = [
            'name' => 'required',
            'hour' => 'numeric|min:0',
            'credit' => 'required|numeric|min:0'
        ];

        $input = Input::only(
            'name',
            'hour',
            'credit',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $hour = null;
        if(!empty(Input::get('hour'))){
            $hour = Input::get('hour');
        }

        CreditUsagePackage::create([
            'name' => Input::get('name'),
            'hour_duration' => $hour,
            'credit_amount' => Input::get('credit'),
            'description' => Input::get('description'),
            'status' => 1
        ]);

        return Redirect::back()->with('action', 'success');
    }

    public function updateUsageMenu($id)
    {
        $rules = [
            'name' => 'required',
            'hour' => 'required|numeric|min:0',
            'credit' => 'required|numeric|min:0'
        ];

        $input = Input::only(
            'name',
            'hour',
            'credit',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $credit = CreditUsagePackage::find($id);
        $credit->name = Input::get('name');
        $credit->hour_duration = Input::get('hour');
        $credit->credit_amount = Input::get('credit');
        $credit->description = Input::get('description');
        $credit->save();

        return Response::json(array('success' => true), 200);
    }

    public function destroyUsageMenu($id)
    {
        $credit = CreditUsagePackage::find($id);
        $credit->status = 0;
        $credit->save();
        return Redirect::back()->with('action', 'dsuccess');
    }

}
