<?php namespace App\Http\Controllers;

use App\CommentLike;
use App\Feed;
use App\FeedComment;
use App\FeedLike;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class MemberfeedController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $feeds = Feed::orderBy('id', 'desc')->paginate(5);

        if ($request->ajax()) {
            $view = view('community.feeds', compact('feeds'))->render();
            return response()->json(['html' => $view]);
        }

        $query = DB::select("SELECT `posted_by`, SUM(`count`) AS `Total` " .
            "FROM ( " .
            "SELECT `posted_by`, COUNT(*) AS `count` " .
            "FROM feeds " .
            "GROUP BY `posted_by` " .
            "UNION ALL " .
            "SELECT `posted_by`, COUNT(*) AS `count` " .
            "FROM feed_comments " .
            "GROUP BY `posted_by` " .
            "UNION ALL " .
            "SELECT `liked_by` as `posted_by`, COUNT(*) AS `count` " .
            "FROM feed_likes " .
            "GROUP BY `liked_by` " .
            "UNION ALL " .
            "SELECT `liked_by` as `posted_by`, COUNT(*) AS `count` " .
            "FROM comment_likes " .
            "GROUP BY `liked_by` " .
            ") AS `union` " .
            "GROUP BY `posted_by` ORDER BY `Total` DESC LIMIT 1");
        return view('community.memberfeed', compact('feeds', 'query'));
    }

    public function store()
    {
        $picture = Input::file('image');
        $uploaded_picture_path = null;
        if ($picture != null) {
            $rules = array('image' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('image' => $picture), $rules);

            if ($validator->passes()) {
                $destinationPath = 'img/feeds';
                $filename = str_random(15) . Auth::user()->id . "." . $picture->getClientOriginalExtension();
                $upload_success = $picture->move($destinationPath, $filename);

                if ($upload_success) {
                    $uploaded_picture_path = $destinationPath . "/" . $filename;
                }
            }

            if ($uploaded_picture_path == null) {
                return Response::json(array(
                    'success' => false,
                    'errors' => $validator->getMessageBag()->toArray()
                ), 200);
            }
        }

        $feed = Feed::create([
            'content' => Input::get('feed'),
            'img_url' => $uploaded_picture_path,
            'posted_by' => Auth::user()->id
        ]);

        return Response::json(array('success' => true, 'feed' => $feed), 200);
    }

    public function editFeed()
    {
        $feed = Feed::find(Input::get('feed_id'));

        $feed->content = Input::get('feed');
        $feed->save();

        return Response::json(array('success' => true, 'newContent' => $feed->content), 200);
    }

    public function deleteFeed()
    {
        $feed = Feed::find(Input::get('feed_id'));

        if ($feed->img_url != null) {
            $file_path = public_path() . "/" . $feed->img_url;
            File::delete($file_path);
        }

        $feed->delete();

        return Response::json(array('success' => true), 200);
    }

    public function likeFeed()
    {
        $feed_like = FeedLike::where('feed_id', Input::get('feed_id'))->where('liked_by', Input::get('liked_by'))->first();
        if (isset($feed_like)) {
            return Response::json(array('success' => false, 'message' => 'already liked'), 200);
        }
        FeedLike::create([
            'feed_id' => Input::get('feed_id'),
            'liked_by' => Input::get('liked_by')
        ]);

        $count = count(FeedLike::where('feed_id', Input::get('feed_id'))->get());

        return Response::json(array('success' => true, 'likeCount' => $count, 'feed_id' => Input::get('feed_id')), 200);
    }

    public function unlikeFeed()
    {

        $feed_like = FeedLike::where('feed_id', Input::get('feed_id'))->where('liked_by', Input::get('liked_by'));
        $feed_like->delete();

        $count = count(FeedLike::where('feed_id', Input::get('feed_id'))->get());

        return Response::json(array('success' => true, 'likeCount' => $count, 'feed_id' => Input::get('feed_id')), 200);
    }

    public function storeComment()
    {
        FeedComment::create([
            'comment' => Input::get('comment'),
            'feed_id' => Input::get('feed_id'),
            'posted_by' => Auth::user()->id
        ]);

        $feed = Feed::find(Input::get('feed_id'));

        $html = "<div class=\"row feed-item\">
                    <ul class=\"media-list comments-list clearlist\">
                        <li class=\"media\">";

        $user = User::find($feed->posted_by);

        $html .= "<a class=\"pull-left\">";

        if ($user->photo_url != null) {
            $html .= "<img class=\"media-object comment-avatar round-image\" src=\"" . URL::to('/') . "/" . $user->photo_url . "\" alt=\"" . $user->firstname . " " . $user->lastname . "\" width=\"60\" height=\"60\">";
        } else {
            $html .= "<img class=\"media-object comment-avatar round-image default-profile-picture\" data-name=\"" . $user->firstname . "\" width=\"60\" height=\"60\" />";
        }

        $html .= "</a>";
        $html .= "<div class=\"media-body\">";
        $html .= "    <div class=\"comment-info\">";
        $html .= "        <div class=\"comment-author\" style=\"margin-bottom: -4px;\">";
        $html .= "            <a href=\"/community/" . $user->id . "\" style=\"font-size: large;\"><strong>" . $user->firstname . " " . $user->lastname . "</strong></a>";

        if ($user->id == Auth::user()->id) {
            $html .= "<div class=\"dropdown pull-right\">";
            $html .= "<button type=\"button\" class=\"close dropdown-toggle\" data-toggle=\"dropdown\">";
            $html .= "<span class=\"fa fa-angle-down\"></span>";
            $html .= "</button>";
            $html .= "<ul class=\"dropdown-menu pull-right\" role=\"menu\" style=\"padding: 0px; min-width: 80px\">";
            $html .= "<li><a href=\"javascript:;\" onclick=\"editFeed(this)\">Edit</a></li>";
            $html .= "<li><a href=\"javascript:;\" onclick=\"deleteFeed(this,'" . $feed->id . "')\">Delete</a></li>";
            $html .= "</ul>";
            $html .= "</div>";
        }

        $html .= "        </div>";
        $html .= "        <span style=\"font-size: medium;\">" . $user->job_title;

        if ($user->company != null) {
            $html .= " at " . $user->company;
        }

        $html .= "</span>";
        $html .= "        <p style=\"line-height: 8px; font-size: small;\"><time class=\"timeago\" datetime=\"" . $feed->updated_at . "\"></time></p>";
        $html .= "   </div>";
        $html .= "</div>";
        $html .= "<p style=\"margin-top: -10px;margin-bottom:0;line-height: 20px;\" data-full=\"" . $feed->content . "\">" . $feed->content . "</p>";

        if ($user->id == Auth::user()->id) {
            $html .= "<form method=\"post\" action=\"\" id=\"feed-form\" role=\"form\" class=\"contact-comments\" style=\"display: none\">";
            $html .= "<textarea name=\"feed\" class=\"cmnt-text form-control\" rows=\"6\" style=\"resize: none; height: 150px\">" . $feed->content . "</textarea>";
            $html .= "<div style=\"border-top: 1px solid #D9DEE4;\"></div>";
            $html .= "<input type=\"hidden\" name=\"_token\" value=\"" . csrf_token() . "\">";
            $html .= "<input type=\"hidden\" name=\"feed_id\" value=\"" . $feed->id . "\">";
            $html .= "<button class=\"btn btn-dark-solid\" onclick=\"saveEditedFeed(this)\" style=\"float:right;margin-right: 0;margin-bottom: 20px\">";
            $html .= "Save";
            $html .= "</button>";
            $html .= "<p style=\"margin: 0;float:right;display:none\">";
            $html .= "<img src=\"" . URL::asset('img/AjaxLoader.gif') . "\" style=\"margin-right: 10px\">";
            $html .= "</p>";
            $html .= "<button class=\"btn btn-dark-solid\" onclick=\"cancelEdit(this)\" style=\"float:right;margin-right: 2px;margin-bottom: 20px\">";
            $html .= "Cancel";
            $html .= "</button>";
            $html .= "</form>";
        }

        if ($feed->img_url != null) {
            $html .= "<img src=\"" . URL::to('/') . "/" . $feed->img_url . "\" alt=\"" . $user->firstname . "'s feed image\" width=\"100%\">";
        }

        $html .= "<div class=\"divider before-like-comment\" style=\"margin: 0\"></div>";

        if ($feed->isLikedBy($feed->id, Auth::user()->id)) {
            $html .= "<a href=\"javascript:void(0);\" style=\"color:#d6b161; margin-right: 30px\" onclick=\"unlikeFeed(this,'" . $feed->id . "')\"><i class=\"fa fa-thumbs-o-up\"></i> " . count($feed->likes) . "</a>";
        } else {
            $html .= "<a href=\"javascript:void(0);\" style=\"color:#7e7e7e; margin-right: 30px\" onclick=\"likeFeed(this,'" . $feed->id . "')\"><i class=\"fa fa-thumbs-o-up\"></i> " . count($feed->likes) . "</a>";
        }

        $html .= "<a style=\"color:#7e7e7e;\" ><i class=\"fa fa-comment-o\"></i> " . count($feed->comments) . "</a>";
        $html .= "<div class=\"divider\" style=\"margin: 0;top: -10px\"></div>";
        $html .= "<ul class=\"media-list comments-list clearlist\">";
        $html .= "<li class=\"media\" style=\"display: none\">";
        $html .= "<a class=\"pull-left\" href=\"javascript:;\" onclick=\"showAllComments(this)\">";
        $html .= "View all comments";
        $html .= "</a>";
        $html .= "</li>";

        foreach ($feed->comments as $comment) {
            $comment_user = User::find($comment->posted_by);

            $html .= "<li class=\"media\">";
            $html .= "    <a class=\"pull-left\" href=\"/community/" . $comment_user->id . "\">";

            if ($comment_user->photo_url != null) {
                $html .= "<img class=\"media-object comment-avatar round-image\" src=\"" . URL::to('/') . "/" . $comment_user->photo_url . "\" alt=\"" . $comment_user->firstname . " " . $comment_user->lastname . "\" width=\"40\" height=\"40\">";
            } else {
                $html .= "<img class=\"media-object comment-avatar round-image default-profile-picture\" data-name=\"" . $comment_user->firstname . "\" width=\"40\" height=\"40\" />";
            }

            $html .= "    </a>";
            $html .= "  <div class=\"media-body\">";
            $html .= "      <div class=\"comment-info\" style=\"margin-top:-5px\">";
            $html .= "          <div class=\"comment-author\" style=\"margin-bottom: -8px;\">";
            $html .= "              <a href=\"/community/" . $comment_user->id . "\" style=\"color: #323232\">" . $comment_user->firstname . " " . $comment_user->lastname . "</a>";
            $html .= "          </div>";
            $html .= "          <span style=\"font-size: small;\">" . $comment_user->job_title;

            if ($comment_user->company != null) {
                $html .= " at " . $comment_user->company;
            }

            $html .= "</span>";
            $html .= "          <p style=\"line-height: 3px; font-size: smaller;margin-bottom: 10px\"><time class=\"timeago\" datetime=\"" . $comment->updated_at . "\"></time></p>";
            $html .= "      </div>";
            $html .= "      <p style=\"margin:0;line-height: 20px;\">";
            $html .= $comment->comment;
            $html .= "          <br>";

            if ($comment->isLikedBy($comment->id, Auth::user()->id)) {
                $html .= "<a href=\"javascript:void(0);\" style=\"color:#d6b161; margin-right: 30px\" onclick=\"unlikeComment(this,'" . $comment->id . "')\"><i class=\"fa fa-thumbs-o-up\"></i> " . count($comment->likes) . "</a>";
            } else {
                $html .= "<a href=\"javascript:void(0);\" style=\"color:#7e7e7e; margin-right: 30px\" onclick=\"likeComment(this,'" . $comment->id . "')\"><i class=\"fa fa-thumbs-o-up\"></i> " . count($comment->likes) . "</a>";
            }

            $html .= "      </p>";
            $html .= "  </div>";
            $html .= "</li>";
        }

        $html .= "<li class=\"media\">";
        $html .= "  <a class=\"pull-left\" >";

        if ($user->photo_url != null) {
            $html .= "<img class=\"media-object comment-avatar round-image\" src=\"" . URL::to('/') . "/" . $user->photo_url . "\" alt=\"" . $user->firstname . " " . $user->lastname . "\" width=\"40\" height=\"40\">";
        } else {
            $html .= "<img class=\"media-object comment-avatar round-image default-profile-picture\" data-name=\"" . $user->firstname . "\" width=\"40\" height=\"40\" />";
        }

        $html .= "</a>";
        $html .= "<div class=\"media-body\">";
        $html .= "  <form method=\"post\" action=\"\" id=\"comment-form\" role=\"form\" class=\"contact-comments\">";
        $html .= "      <input type=\"text\" name=\"comment\" id=\"comment\" class=\"form-control\" placeholder=\"Write a comment..\" maxlength=\"255\">";
        $html .= "      <input type=\"hidden\" name=\"feed_id\" value=\"" . $feed->id . "\">";
        $html .= "      <input type=\"hidden\" name=\"_token\" value=\"" . csrf_token() . "\">";
        $html .= "  </form>";
        $html .= "</div>";
        $html .= "</li>";
        $html .= "</ul>";
        $html .= "</li>";
        $html .= "</ul>";
        $html .= "</div>";

        return Response::json(array('success' => true, 'htmlRespond' => $html), 200);
    }

    public function likeComment()
    {
        $comment_like = CommentLike::where('comment_id', Input::get('comment_id'))->where('liked_by', Input::get('liked_by'))->first();
        if (isset($comment_like)) {
            return Response::json(array('success' => false, 'message' => 'already liked'), 200);
        }
        CommentLike::create([
            'comment_id' => Input::get('comment_id'),
            'liked_by' => Input::get('liked_by')
        ]);

        $count = count(CommentLike::where('comment_id', Input::get('comment_id'))->get());

        return Response::json(array('success' => true, 'likeCount' => $count, 'comment_id' => Input::get('comment_id')), 200);
    }

    public function unlikeComment()
    {

        $comment_like = CommentLike::where('comment_id', Input::get('comment_id'))->where('liked_by', Input::get('liked_by'));
        $comment_like->delete();

        $count = count(CommentLike::where('comment_id', Input::get('comment_id'))->get());

        return Response::json(array('success' => true, 'likeCount' => $count, 'comment_id' => Input::get('comment_id')), 200);
    }

}
