<?php namespace App\Http\Controllers;

use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use App\SuggestLocation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ContactController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $locations = Location::all();
        return view('contact', compact('locations'));
	}

    public function viewAdmin()
    {
        $contacts = Contact::all();
        return view('admin.contact', compact('contacts'));
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'location' => 'required|integer',
            'office_size' => 'required|integer',
            'question' => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'company',
            'location',
            'office_size',
            'question'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        Contact::create([
            'name' => ucwords(Input::get('name')),
            'email' => Input::get('email'),
            'company' => Input::get('company'),
            'location_id' => Input::get('location'),
            'office_size' => Input::get('office_size'),
            'question' => Input::get('question'),
        ]);

        $name = ucwords(Input::get('name'));
        $email = Input::get('email');
        $company = Input::get('company');
        $location = Location::find(Input::get('location'));
        $office_size = Input::get('office_size');
        $question = Input::get('question');

        if($office_size == 11){
            $office_size = "10+";
        }

        Mail::send('emails.contact', compact('name','email', 'company', 'location', 'office_size', 'question'), function($message) {
            $message->to("kolegajkt@gmail.com", ucwords(Input::get('name')))
                ->subject('Contact from '.Input::get('email'));
        });

        return Redirect::back()->with('action', 'success');
	}

    public function suggestLocation()
    {
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'email' => 'required|email',
            'location' => 'required'
        ];

        $input = Input::only(
            'name',
            'email',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        SuggestLocation::create([
            'name' => ucwords(Input::get('name')),
            'email' => Input::get('email'),
            'location' => Input::get('location')
        ]);

        $name = ucwords(Input::get('name'));
        $email = Input::get('email');
        $location = Input::get('location');

        Mail::send('emails.suggestLocation', compact('name','email', 'location'), function($message) {
            $message->to("kolegajkt@gmail.com", ucwords(Input::get('name')))
                ->subject('Suggestion from '.Input::get('email'));
        });

        return Redirect::back()->with('action', 'suggest-success');
    }

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
