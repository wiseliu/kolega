<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Google_Client;
use Google_Service_People;
use Google_Service_People_Birthday;
use Google_Service_Plus;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Laravel\Socialite\Facades\Socialite;

class AuthController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;

		$this->middleware('guest', ['except' => 'getLogout']);
	}

    public function redirectToFacebook()
    {
        return Socialite::driver('facebook')->fields([
            'first_name', 'last_name', 'email', 'gender', 'birthday'
        ])->scopes([
            'email', 'user_birthday'
        ])->redirect();
    }

    public function handleFacebookCallback()
    {
        try {
            $user = Socialite::driver('facebook')->fields([
                'first_name', 'last_name', 'email', 'gender', 'birthday'
            ])->user();
        } catch (Exception $e) {
            return Redirect::to('/auth/facebook');
        }

        if($user->email == null || $user->user['gender'] == null || $user->user['birthday'] == null || $user->user['id'] == null){
            return Redirect::to('/login')->with('action', 'socialite-fail');
        }

        $authUser = User::where('email', $user->email)->first();

        if ($authUser){
            Auth::login($authUser, true);
            return Redirect::to('/community');
        }

        $gender = '';
        if($user->user['gender'] == 'male'){
            $gender = 'M';
        } else if($user->user['gender'] == 'female') {
            $gender = 'F';
        }

        $authUser = User::create([
            'email' => $user->email,
            'password' => 'social-login',
            'verification_code' => $user->user['id'],
            'firstname' => $user->user['first_name'],
            'lastname' => $user->user['last_name'],
            'company' => '',
            'organization' => '',
            'job_title' => '',
            'gender' => $gender,
            'date_of_birth' => date("Y-m-d", strtotime($user->user['birthday'])),
            'phone_number' => 'fb-'.$user->user['id'],
            'address' => '',
            'privilege' => 'user',
            'is_verified' => true
        ]);

        Auth::login($authUser, true);
        return Redirect::to('/community');

    }

    public function redirectToGoogle()
    {
        return Socialite::driver('google')->scopes(['openid', 'profile', 'email',
            //Google_Service_People::USER_BIRTHDAY_READ,
            //Google_Service_People::USER_PHONENUMBERS_READ,
            //Google_Service_People::USER_ADDRESSES_READ,
            //Google_Service_People::USERINFO_PROFILE,
            //Google_Service_People::CONTACTS_READONLY
        ])->redirect();
    }

    public function handleGoogleCallback()
    {
        try {
            $user = Socialite::driver('google')->user();
        } catch (Exception $e) {
            return Redirect::to('/auth/google');
        }

        if($user->email == null || $user->name == null || $user->id == null){
            return Redirect::to('/login')->with('action', 'socialite-fail');
        }

        $authUser = User::where('email', $user->email)->first();

        if ($authUser){
            Auth::login($authUser, true);
            return Redirect::to('/community');
        }

        $fullname_chunk = explode(" ",$user->name);
        $lastname = '';
        if(sizeof($fullname_chunk) > 1){
            $lastname = $fullname_chunk[sizeof($fullname_chunk) - 1];
        }

        $firstname = '';

        for($i =0; $i < sizeof($fullname_chunk) - 1; $i++){
            $firstname .= $fullname_chunk[$i];
            if($i != sizeof($fullname_chunk) - 2){
                $firstname .= ' ';
            }
        }

        $authUser = User::create([
            'email' => $user->email,
            'password' => 'social-login',
            'verification_code' => $user->id,
            'firstname' => ucwords(strtolower($firstname)),
            'lastname' => ucwords(strtolower($lastname)),
            'company' => '',
            'organization' => '',
            'job_title' => '',
            'gender' => '',
            'date_of_birth' => '1900-01-01',
            'phone_number' => 'google-'.$user->id,
            'address' => '',
            'privilege' => 'user',
            'is_verified' => true
        ]);

        Auth::login($authUser, true);
        return Redirect::to('/community');

/*
        $google_client_token = [
            'access_token' => $user->token,
            'refresh_token' => $user->refreshToken,
            'expires_in' => $user->expiresIn
        ];

        $client = new Google_Client();
        $client->setApplicationName("Kolega");
        $client->setDeveloperKey(env('GOOGLE_SECRET'));
        $client->setAccessToken(json_encode($google_client_token));

        $service = new Google_Service_People($client);

        $optParams = array('requestMask.includeField' => 'person.phone_numbers,person.names,person.email_addresses');
        $results = $service->people_connections->listPeopleConnections('people/me',$optParams);

        dd($results);
*/
    }

}
