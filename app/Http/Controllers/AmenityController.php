<?php namespace App\Http\Controllers;

use App\Amenity;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AmenityController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $amenities = Amenity::all();
        return view('admin.amenities', compact('amenities'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'icon-code' => 'required'
        ];

        $input = Input::only(
            'name',
            'icon-code',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        Amenity::create([
            'name' => Input::get('name'),
            'icon_code' => strtolower(Input::get('icon-code')),
            'description' => Input::get('description')
        ]);

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = [
            'name' => 'required|regex:/^[(a-zA-Z0-9\s)]+$/u',
            'iconCode' => 'required'
        ];

        $input = Input::only(
            'name',
            'iconCode',
            'description'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $amenity = Amenity::find($id);
        $amenity->name = Input::get('name');
        $amenity->icon_code = strtolower(Input::get('iconCode'));
        $amenity->description = Input::get('description');
        $amenity->save();

        return Response::json(array('success' => true), 200);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$amenity = Amenity::find($id);
        $amenity->delete();
        return Redirect::back()->with('action', 'dsuccess');
	}

}
