<?php

namespace App\Http\Controllers;

use App\Feed;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Provent;
use App\ProventComment;
use App\ProventMember;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class ProventController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $provents = Provent::with('proventMembers')->with('proventComments')->orderBy('id', 'desc')->paginate(5);

        if ($request->ajax()) {
            $view = view('provent.feeds', compact('provents'))->render();
            return response()->json(['html' => $view]);
        }
//        dd($provents);
        return view('provent.memberfeed', compact('provents'));
    }
    
    public function getAll()
    {
        $provents = Provent::with('proventMembers')->with('proventComments')->orderBy('id', 'desc')->get();
//        dd($provents);
        return view('admin.provents', compact('provents'));
    }

    public function getAllProject(Request $request)
    {
        $provents = Provent::with('proventMembers')->with('proventComments')->where('type', 'project')->orderBy('id', 'desc')->paginate(5);

        if ($request->ajax()) {
            $view = view('provent.feeds', compact('provents'))->render();
            return response()->json(['html' => $view]);
        }
//        dd($provents);
        return view('provent.memberfeed', compact('provents'));
    }

    public function getAllEvent(Request $request)
    {
        $provents = Provent::with('proventMembers')->with('proventComments')->where('type', 'event')->orderBy('id', 'desc')->paginate(5);

        if ($request->ajax()) {
            $view = view('provent.feeds', compact('provents'))->render();
            return response()->json(['html' => $view]);
        }
        return view('provent.memberfeed', compact('provents'));
    }


    public function joinProvent()
    {
        $user = Auth::user();
        $provent_id =Input::get('provent_id');
        $provent = Provent::find($provent_id);
        if ($provent->user->id == $user->id) {
            return Redirect::back()->withInput();
        }
        ProventMember::create([
            'provent_id' => $provent_id,
            'user_id' => $user->id,
            'approved' => 0,
        ]);

        return Redirect::back()->with('action', 'Join Request Sent');
    }

    public function approveMember($provent_id,$user_id)
    {
        $proventMember = ProventMember::where('provent_id', $provent_id)->where('user_id', $user_id)->update(['approved' => 1]);
//        $proventMember->approved = 1;
//        $proventMember->save();
//        $user = User::find($user_id);
//        $mail_data = compact('user', 'proventMember');
//        Mail::send('emails.provent_approved', $mail_data, function ($message) use ($mail_data) {
//            $message->to($mail_data['user']->email)
//                ->subject('Your Project/Event Membership is Approved');
//        });
        return Response::json(array('success' => true), 200);
    }

    public function rejectMember($provent_id,$user_id)
    {
//        $provent_id = Input::get('provent_id');
//        $member_id = Input::get('member_id');
        $proventMember = ProventMember::where('provent_id', $provent_id)->where('member_id', $member_id)->first();
//        $user = User::find($user_id);
//        $mail_data = compact('user', 'proventMember');
//        Mail::send('emails.provent_rejected', $mail_data, function ($message) use ($mail_data) {
//            $message->to($mail_data['user']->email)
//                ->subject('Your Project/Event Membership is Rejected');
//        });
        return Redirect::back()->with('action', 'success');
    }

    public function addComment($provent_id)
    {
        $rules = [
            'comment' => 'required|string'
        ];

        $input = Input::only(
            'comment'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $user = Auth::user();

        ProventComment::create([
            'comment' => Input::get('comment'),
            'provent_id' => $provent_id,
            'user_id' => $user->id,
        ]);
        return Redirect::to('provent/' . $provent_id)->with('action', 'Comment added');
    }

    public function search()
    {
        $provents = Provent::search(Input::get('q'))->get();
                return view('provent.memberfeed', compact('provents'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function showForm()
    {
        return view('provent.create-event');
    }

    public function create()
    {
//        return view('provents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'start_date' => 'required|date_format:Y-m-d|after:today',
            'start_time' => 'required|date_format:H:i',
            'end_date' => 'required|date_format:Y-m-d|after:today',
            'end_time' => 'required|date_format:H:i',
            'type' => 'required|in:project,event',
            'user_id' => 'required|exists:users,id',
            'image' => 'mimes:png,jpeg',
            'location' => 'required|string',
        ];

        $input = Input::only(
            'name',
            'description',
            'start_date',
            'end_date',
            'type',
            'user_id',
            'image',
            'location',
            'start_time',
            'end_time'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $user = Auth::user();
        $picture = Input::file('image');

        if ($picture != null) {
            $uploaded_picture_path = "";
            $destinationPath = 'img/provent';
            $filename = Hash::make($user->id) . "." . $picture->getClientOriginalExtension();
            $filename = str_replace('/', '', $filename);
            $upload_success = $picture->move($destinationPath, $filename);

            if ($upload_success) {
                $uploaded_picture_path = $destinationPath . "/" . $filename;
            }

            if ($uploaded_picture_path == "") {
                return Redirect::back()->withInput()->withErrors();
            }
        } else {
            $uploaded_picture_path = '';
        }

        $provent = Provent::create([
            'name' => Input::get('name'),
            'description' => Input::get('description'),
            'start_date' => Input::get('start_date') . ' ' . Input::get('start_time') . ':00',
            'end_date' => Input::get('end_date') . ' ' . Input::get('end_time') . ':00',
            'type' => Input::get('type'),
            'user_id' => Input::get('user_id'),
            'location' => Input::get('location'),
            'approved' => 0,
            'img_url' => $uploaded_picture_path
        ]);

        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_success', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Wait for Project/Event Confirmation');
        });

        return Redirect::to('provent');
    }

    public function getAllNonApprovedProvent()
    {
        $provents = Provent::where('approved', false)->get();
        return view('admin.provents', compact('provents'));
    }

    public function approveProvent($provent_id)
    {
        $id = $provent_id;
        $provent = Provent::find($id);
        $provent->approved = 1;
        $provent->save();
        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_success', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Your Project/Event is Approved');
        });
        return Redirect::back()->with('action', 'success');
    }

    public function rejectProvent($provent_id)
    {
        $id = $provent_id;
        $provent = Provent::find($id);
        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_rejected', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Your Project/Event is Rejected');
        });
        return Redirect::back()->with('action', 'success');
    }
    
    public function approveProventAJAX($provent_id)
    {
        $provent = Provent::find($provent_id);
        $provent->approved = 1;
        $provent->save();
        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_success', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Your Project/Event is Approved');
        });
        return Response::json(array('success' => true), 200);
    }
    
    public function rejectProventAJAX($provent_id)
    {
        $id = $provent_id;
        $provent = Provent::find($id);
        $user = User::find($provent->user_id);
        $mail_data = compact('user', 'provent');
        Mail::send('emails.provent_rejected', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['user']->email)
                ->subject('Your Project/Event is Rejected');
        });
        return Response::json(array('success' => true), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($provent_id)
    {
        $provent = Provent::with('members')->with('proventMembers')->with('proventComments')->with('approvedProventMembers')->with('nonApprovedProventMembers')->find($provent_id);
        $provent_member = [];
        foreach ($provent->proventMembers as $member){
            array_push($provent_member, $member['user_id']);
        }
//        dd($provent_member);
        if ($provent != null) {
            return view('provent.single-event', compact('provent','provent_member'));
        } else {
            return view('errors.404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($provent_id)
    {
        $provent = Provent::find($provent_id);
        return view('provent.edit-event', compact('provent'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update()
    {
        $rules = [
            'name' => 'required|string',
            'description' => 'required|string',
            'location' => 'required|string',
            'start_date' => 'required|date_format:Y-m-d|after:today',
            'start_time' => 'required|date_format:H:i',
            'end_date' => 'required|date_format:Y-m-d|after:today',
            'end_time' => 'required|date_format:H:i',
            'type' => 'required|in:project,event',
            'image' => 'mimes:png,jpeg',
        ];

        $input = Input::only(
            'name',
            'description',
            'start_date',
            'end_date',
            'type',
            'image',
            'location',
            'start_time',
            'end_time'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $provent = Provent::find(Input::get('provent_id'));

        if ($provent->photo_url != null) {
            $file_path = public_path() . "/" . $provent->photo_url;
            File::delete($file_path);
        }
        $picture = Input::file('image');

        $uploaded_picture_path = "";
        if ($picture != null) {
            $destinationPath = 'img/provent';
            $filename = Hash::make($provent->id) . "." . $picture->getClientOriginalExtension();
            $filename = str_replace('/', '', $filename);
            $upload_success = $picture->move($destinationPath, $filename);

            if ($upload_success) {
                $uploaded_picture_path = $destinationPath . "/" . $filename;
            }

            if ($uploaded_picture_path == "") {
                return Redirect::back()->withInput()->withErrors($validator);
            }
        } else {
            $uploaded_picture_path == "";
        }


        $provent->name = Input::get('name');
        $provent->description = Input::get('description');
        $provent->location = Input::get('location');
        $provent->start_date = Input::get('start_date') . ' ' . Input::get('start_time') . ':00';
        $provent->end_date = Input::get('end_date') . ' ' . Input::get('end_time') . ':00';
        $provent->type = Input::get('type');
        $provent->img_url = $uploaded_picture_path;

        $provent->save();
        return Redirect::to('provent/'.$provent->id)->with('action', 'Edit Successful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy()
    {
        $provent = Provent::find(Input::get('provent_id'));
        $provent->delete();
        return Redirect::back()->with('action', 'success');
    }

}
