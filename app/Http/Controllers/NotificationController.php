<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use OneSignal;

class NotificationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        return view('admin.notification');
	}

    public function sendAll()
    {
        $rules = [
            'summary' => 'required',
            'message' => 'required'
        ];

        $input = Input::only(
            'summary',
            'message'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $data = [ 'content' => Input::get('message') ];
        OneSignal::sendNotificationToAll(Input::get('summary'), $url = null, $data, $buttons = null, $schedule = null);

//        $contents = array(
//            "en" => Input::get('summary')
//        );
//        $params = array(
//            'contents' => $contents,
//            'included_segments' => array('DEV'),
//            'detailmessage' => Input::get('message')
//        );
//        OneSignal::sendNotificationCustom($params);

        return Redirect::back()->with('action', 'success');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
