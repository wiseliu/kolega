<?php namespace App\Http\Controllers;

use App\CreditAddition;
use App\CreditPurchasePackage;
use App\CreditSubscriptionLog;
use App\CreditSubstraction;
use App\CreditSubstractionPayment;
use App\CreditUsagePackage;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Location;
use App\PromoCode;
use App\User;
use App\UserKolega;
use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {
            return Redirect::home();
        }
        return view('login', ['action' => 'none']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function viewAdmin()
    {
        $users = User::all();
        return view('admin.users', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed|min:8',
            'firstname' => 'required|regex:/^[(a-zA-Z\s)]+$/u',
            'lastname' => 'alpha',
            'gender' => 'alpha',
            'date_of_birth' => 'date|before:today',
            'phone_number' => 'numeric|unique:users',
            'address' => 'string'
        ];

        $input = Input::only(
            'email',
            'password',
            'password_confirmation',
            'firstname',
            'lastname',
            'company',
            'organization',
            'job_title',
            'gender',
            'date_of_birth',
            'phone_number',
            'address'
        );
        



        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::except('password'))->withErrors($validator)->with('action', 'register');
        }
        
        if(Input::get('date_of_birth') == null){
            Input::merge(['date_of_birth' => strtotime('01-01-1970')]);
        }
        if(Input::get('gender') == null){
            Input::merge(['gender' => ' ']);
        }        
        if(Input::get('address') == null){
            Input::merge(['address' => ' ']);
        }
        $confirmation_code = str_random(15) . Hash::make(Input::get('email') . Input::get('phone_number')) . str_random(12);
        $confirmation_code = str_replace('/', '', $confirmation_code);

        User::create([
            'email' => Input::get('email'),
            'password' => Hash::make(Input::get('password')),
            'verification_code' => $confirmation_code,
            'firstname' => ucwords(strtolower(Input::get('firstname'))),
            'lastname' => ucwords(strtolower(Input::get('lastname'))),
            'company' => Input::get('company'),
            'organization' => Input::get('organization'),
            'job_title' => Input::get('job_title'),
            'gender' => Input::get('gender'),
            'date_of_birth' => Input::get('date_of_birth'),
            'phone_number' => Input::get('phone_number'),
            'address' => Input::get('address'),
            'privilege' => 'user'
        ]);

        Mail::send('emails.verify', compact('confirmation_code'), function ($message) {
            $message->to(Input::get('email'), Input::get('firstname') . " " . Input::get('lastname'))
                ->subject('Verify your email address');
        });
        return Redirect::back()->with('action', 'success');
    }

    public function verify($confirmation_code)
    {
        if (!$confirmation_code) {
            return Redirect::home();
        }
        $user = User::whereVerificationCode($confirmation_code)->first();
        if (!$user) {
            return Redirect::home();
        }
        $user->is_verified = true;
        $user->verification_code = null;
        $user->save();
        return Redirect::route('login')->with('action', 'vsuccess');
    }

    public function doLogin()
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];

        $input = Input::only(
            'email',
            'password'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::except('password'))->withErrors($validator)->with('action', 'login');
        }

        $credentials = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'is_verified' => 1
        ];

        if (!Auth::attempt($credentials)) {
            return Redirect::back()->withInput(Input::except('password'))
                ->withErrors([
                    'credentials' => 'Sorry! We were unable to sign you in.'
                ])->with('action', 'login');
        }

        if (strpos(Auth::user()->privilege, 'admin') !== false ) {
            return Redirect::to('/admin');
        } else if (Auth::user()->privilege == 'user') {
            return Redirect::to('/');
        }

        return Redirect::home();
    }

    public function logout()
    {
        Auth::logout(); // log the user out of our application
        return Redirect::home();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    public function editPrivilege($id)
    {
        $rules = [
            'privilege' => 'required|alpha'
        ];

        $input = Input::only(
            'privilege'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $user = User::find($id);
        $user->privilege = Input::get('privilege');
        $user->save();

        return Response::json(array('success' => true), 200);
    }

    public function editKTP($id)
    {
        $rules = [
            'ktp' => 'required|numeric|unique:users,ktp_number'
        ];

        $input = Input::only(
            'ktp'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $user = User::find($id);
        $user->ktp_number = Input::get('ktp');
        $user->save();

        return Response::json(array('success' => true), 200);
    }

    public function registerKTP($id)
    {
        $rules = [
            'ktp' => 'required|numeric|unique:users,ktp_number'
        ];

        $input = Input::only(
            'ktp',
            'code'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()

            ), 200);
        }

        $code_user = User::find($id);

        if(Input::get('code') != null && Input::get('code') != ''){

            $check_code_exist = User::where('promo_code',Input::get('code'))->count();

            if($check_code_exist > 0){
                PromoCode::create([
                    'promo_code' => strtoupper(Input::get('code')),
                    'user_id' => $id
                ]);

                $code_owner = User::where('promo_code',Input::get('code'))->firstOrFail();
                $code_counter = PromoCode::where('promo_code',Input::get('code'))->count();

                $mail_data = compact('code_counter','code_user','code_owner');
                Mail::send('emails.code_used', $mail_data, function ($message) use ($mail_data) {
                    $message->to($mail_data['code_owner']->email, $mail_data['code_owner']->firstname." ".$mail_data['code_owner']->lastname)
                        ->subject('Your code has been used');
                });
            } else {
                return Response::json(array(
                    'success' => false,
                    'errors' => array("Referral code is wrong. Registration failed.")

                ), 200);
            }

        }

        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $new_promo_code = '';
        $max = strlen($characters) - 1;

        do{
            for ($i = 0; $i < 6; $i++) {
                $new_promo_code .= $characters[mt_rand(0, $max)];
            }
            $check_user = User::where('promo_code',$new_promo_code)->count();
        }while($check_user > 0);

        $code_user->ktp_number = Input::get('ktp');
        $code_user->promo_code_created_date = date('Y-m-d H:i:s');
        $code_user->promo_code = $new_promo_code;
        $code_user->save();

        $mail_data = compact('code_user');
        Mail::send('emails.code_created', $mail_data, function ($message) use ($mail_data) {
            $message->to($mail_data['code_user']->email, $mail_data['code_user']->firstname." ".$mail_data['code_user']->lastname)
                ->subject('Your code has been created');
        });

        return Response::json(array('success' => true, 'code' => $new_promo_code), 200);
    }

    public function creditList(){
        $users = User::all();
        $locations = Location::all();
        $purchasePackages = CreditPurchasePackage::where('status',1)->get();
        $usagePackages = CreditUsagePackage::where('status',1)->get();
        return view('admin.userCredits', compact('users','locations','purchasePackages','usagePackages'));
    }

    public function subscribe(){
        $rules = [
            'month' => 'required|numeric|min:1',
            'location' => 'required'
        ];

        $input = Input::only(
            'month',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200);
        }

        $freeCredit = CreditPurchasePackage::where('name','free')
            ->where('description','bonus after add credit / subscribe')
            ->where('price',0)
            ->where('status',2)
            ->first();

        if(empty($freeCredit)){
            return Response::json(array(
                'success' => false,
                'errors' => array("Can't find bonus package in purchase menu, please ask main admin.")
            ), 200);
        }

        DB::transaction(function($freeCredit) use ($freeCredit){

            $user = User::find(Input::get('user_id'));

            $subscriptionValidUntil = (new DateTime("+".Input::get('month')." months"))->format("Y-m-d");
            if($user->subscribed_until != null){
                $subscriptionValidUntil = date('Y-m-d', strtotime("+".Input::get('month')." months", strtotime($user->subscribed_until)));
            }

            $user->subscribed_until = $subscriptionValidUntil;
            $user->save();

            CreditSubscriptionLog::create([
                "user_id" => Input::get('user_id'),
                "valid_until" =>  $subscriptionValidUntil,
                "created_by" => Auth::user()->email
            ]);

            CreditAddition::create([
                "user_id" => Input::get('user_id'),
                "credit_purchase_package_id" =>  $freeCredit->id,
                "location_id" =>  Input::get('location'),
                "amount" => $freeCredit->credit_amount,
                "valid_until" =>  $subscriptionValidUntil,
                "is_permanent" => 0,
                "created_by" => Auth::user()->email
            ]);
        });

        return Response::json(array('success' => true), 200);
    }

    public function topup(){
        $rules = [
            'ppackage' => 'required',
            'location' => 'required'
        ];

        $input = Input::only(
            'ppackage',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200);
        }

        DB::transaction(function() {
            $user = User::find(Input::get('user_id'));
            $purchasePackage = CreditPurchasePackage::find(Input::get('ppackage'));

            CreditAddition::create([
                "user_id" => Input::get('user_id'),
                "credit_purchase_package_id" =>  Input::get('ppackage'),
                "location_id" =>  Input::get('location'),
                "amount" => $purchasePackage->credit_amount,
                "is_permanent" => 1,
                "created_by" => Auth::user()->email
            ]);

            $user->permanent_credit_amount = $user->permanent_credit_amount + $purchasePackage->credit_amount;
            $user->save();
        });

        return Response::json(array('success' => true), 200);
    }

    public function usecredit(){
        $rules = [
            'upackage' => 'required'
        ];

        $input = Input::only(
            'upackage',
            'location'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Response::json(array(
                'success' => false,
                'errors' => $validator->getMessageBag()->toArray()
            ), 200);
        }

        $user = User::find(Input::get('user_id'));
        $credit_permanent = 0;
        $credit_temporary = array();
        foreach ($user->creditAdditions as $creditAddition){
            if($creditAddition->is_permanent){
                $credit_permanent += $creditAddition->amount;
            } else {
                array_push($credit_temporary,array(
                    "amount" => $creditAddition->amount,
                    "valid_until" => $creditAddition->valid_until
                ));
            }
        }

        foreach ($user->creditSubstractions as $creditSubstraction){
            foreach ($creditSubstraction->payments as $subtractionPayment){
                if($subtractionPayment->is_taken_from_permanent_credit){
                    $credit_permanent -= $subtractionPayment->amount;
                }else{
                    $substractAmount = $subtractionPayment->amount;
                    foreach ($credit_temporary as $key => $ctemp){
                        if($creditSubstraction->created_at <= $ctemp["valid_until"] && $ctemp["amount"] > 0){
                            if($ctemp["amount"] >= $substractAmount){
                                $credit_temporary[$key]["amount"] -= $substractAmount;
                                break;
                            } else{
                                $substractAmount -= $ctemp["amount"];
                                $credit_temporary[$key]["amount"] = 0;
                            }
                        }
                    }
                }
            }
        }

        $usableCredit = $credit_permanent;
        foreach ($credit_temporary as $ctemp){
            if($ctemp['valid_until'] >= date('Y-m-d')){
                $usableCredit += $ctemp["amount"];
            }
        }

        $usagePackage = CreditUsagePackage::find(Input::get('upackage'));

        if($usableCredit < $usagePackage->credit_amount){
            return Response::json(array(
                'success' => false,
                'errors' => array("Credit amount is insufficient.")
            ), 200);
        }

        DB::transaction(function() use ($user, $usagePackage, $credit_temporary) {

            $locationId = null;
            $validUntil = null;

            if(!empty(Input::get('location'))){
                $locationId = Input::get('location');
            }

            if(!empty($usagePackage->hour_duration)){
                $validUntil = date("Y-m-d H:i:s", strtotime('+'.$usagePackage->hour_duration.' hours'));
            }

            $substractAmount = $usagePackage->credit_amount;

            $creditSubstraction = CreditSubstraction::create([
                "user_id" => Input::get('user_id'),
                "credit_usage_package_id" =>  Input::get('upackage'),
                "location_id" => $locationId,
                "total_amount" => $substractAmount,
                "valid_until" => $validUntil,
                "created_by" => Auth::user()->email
            ]);

            foreach ($credit_temporary as /*$key =>*/ $ctemp){
                if($ctemp['valid_until'] >= date('Y-m-d') && $ctemp["amount"] > 0){
                    if($ctemp["amount"] >= $substractAmount){
                        CreditSubstractionPayment::create([
                            "credit_substraction_id" => $creditSubstraction->id,
                            "amount" => $substractAmount,
                            "is_taken_from_permanent_credit" => 0
                        ]);
//                        $credit_temporary[$key]["amount"] -= $substractAmount;
                        $substractAmount = 0;
                        break;
                    } else{
                        CreditSubstractionPayment::create([
                            "credit_substraction_id" => $creditSubstraction->id,
                            "amount" => $ctemp["amount"],
                            "is_taken_from_permanent_credit" => 0
                        ]);
                        $substractAmount -= $ctemp["amount"];
//                        $credit_temporary[$key]["amount"] = 0;
                    }
                }
            }

            if($substractAmount > 0){
                CreditSubstractionPayment::create([
                    "credit_substraction_id" => $creditSubstraction->id,
                    "amount" => $substractAmount,
                    "is_taken_from_permanent_credit" => 1
                ]);
                $user->permanent_credit_amount = $user->permanent_credit_amount - $substractAmount;
                $user->save();
            }

        });

        return Response::json(array('success' => true), 200);
    }

    public function creditLogs($user_id)
    {
        $user = User::find($user_id);
        $userLogs = array();
        foreach ($user->creditSubscriptionLogs as $subscriptionLog){
            array_push($userLogs,array(
                "datetime" => $subscriptionLog->created_at,
                "location" => "-",
                "creditamount" => "-",
                "package" => "SUBSCRIPTION",
                "description" => "Subscription until <b>".date('d F Y', strtotime($subscriptionLog->valid_until))."</b>",
                "class" => "info"
            ));
        }

        foreach ($user->creditAdditions as $creditAddition){
            $location = $creditAddition->location->name;

            $description = "Topup <b>".$creditAddition->amount." credits</b> at <b>".$location."</b>";
            $class = "success";
            $package = "TOP UP: ".$creditAddition->creditPurchasePackage->name;
            if(!$creditAddition->is_permanent){
                $description = "Bonus <b>".$creditAddition->amount." credits</b> from <b>".$location."</b> valid until <b>".date('d F Y', strtotime($creditAddition->valid_until))."</b>";
                $class = "warning";
                $package = "BONUS";
            }

            array_push($userLogs,array(
                "datetime" => $creditAddition->created_at,
                "location" => $location,
                "creditamount" => $creditAddition->amount,
                "package" => $package,
                "description" => $description,
                "class" => $class
            ));
        }

        foreach ($user->creditSubstractions as $creditSubstraction){
            $location = "Everywhere";
            if(!empty($creditSubstraction->location_id)){
                $location = $creditSubstraction->location->name;
            }

            $packageName = $creditSubstraction->creditUsagePackage->name;

            $description = "Use <b>".$creditSubstraction->total_amount." credits</b> for <b>".$packageName."</b> valid at <b>".$location."</b>";
            if(!empty($creditSubstraction->valid_until)){
                $description .= " until <b>".date('d F Y h:i:s', strtotime($creditSubstraction->valid_until))."</b>";
            }

            array_push($userLogs,array(
                "datetime" => $creditSubstraction->created_at,
                "location" => $location,
                "creditamount" => $creditSubstraction->total_amount,
                "package" => "USE CREDIT: ".$packageName,
                "description" => $description,
                "class" => "danger"
            ));
        }

        usort($userLogs,function($a,$b){
            return strtotime($a["datetime"]) - strtotime($b["datetime"]);
        });

        $locations = Location::all();
        $purchasePackages = CreditPurchasePackage::where('status',1)->get();
        $usagePackages = CreditUsagePackage::where('status',1)->get();

        return view('admin.userCreditsLog', compact('user','userLogs','locations','purchasePackages','usagePackages'));
    }

    public function exportCsv($user_id){
        $user = User::find($user_id);
        $userLogs = array();
        foreach ($user->creditSubscriptionLogs as $subscriptionLog){
            array_push($userLogs,array(
                "datetime" => $subscriptionLog->created_at,
                "location" => "-",
                "creditamount" => "-",
                "package" => "SUBSCRIPTION",
                "description" => "Subscription until ".date('d F Y', strtotime($subscriptionLog->valid_until)),
                "class" => "info"
            ));
        }

        foreach ($user->creditAdditions as $creditAddition){
            $location = $creditAddition->location->name;

            $description = "Topup ".$creditAddition->amount." credits at ".$location;
            $class = "success";
            $package = "TOP UP: ".$creditAddition->creditPurchasePackage->name;
            if(!$creditAddition->is_permanent){
                $description = "Bonus ".$creditAddition->amount." credits from ".$location." valid until ".date('d F Y', strtotime($creditAddition->valid_until));
                $class = "warning";
                $package = "BONUS";
            }

            array_push($userLogs,array(
                "datetime" => $creditAddition->created_at,
                "location" => $location,
                "creditamount" => $creditAddition->amount,
                "package" => $package,
                "description" => $description,
                "class" => $class
            ));
        }

        foreach ($user->creditSubstractions as $creditSubstraction){
            $location = "Everywhere";
            if(!empty($creditSubstraction->location_id)){
                $location = $creditSubstraction->location->name;
            }

            $packageName = $creditSubstraction->creditUsagePackage->name;

            $description = "Use ".$creditSubstraction->total_amount." credits for ".$packageName." valid at ".$location;
            if(!empty($creditSubstraction->valid_until)){
                $description .= " until ".date('d F Y h:i:s', strtotime($creditSubstraction->valid_until));
            }

            array_push($userLogs,array(
                "datetime" => $creditSubstraction->created_at,
                "location" => $location,
                "creditamount" => $creditSubstraction->total_amount,
                "package" => "USE CREDIT: ".$packageName,
                "description" => $description,
                "class" => "danger"
            ));
        }

        usort($userLogs,function($a,$b){
            return strtotime($a["datetime"]) - strtotime($b["datetime"]);
        });

        $headers = array(
            'Content-Type'        => 'application/vnd.ms-excel; charset=utf-8',
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-Disposition' => 'attachment; filename=creditlogs.csv',
            'Expires'             => '0',
            'Pragma'              => 'public',
        );

        $filename = $user->email."_CreditLogs.csv";
        $csvPath = 'csv/'.$filename;
        $handle = fopen($csvPath, 'w');
        fputcsv($handle, [
            "Date / Time",
            "Location",
            "Credit Amount",
            "Package",
            "Description"
        ]);

        foreach ($userLogs as $log) {
            fputcsv($handle, [
                $log["datetime"],
                $log["location"],
                $log["creditamount"],
                $log["package"],
                $log["description"]
            ]);
        }

        fclose($handle);
        return Response::download($csvPath, $filename, $headers);
    }

    public function destroy($id)
    {
        //
    }

    public function addKolega()
    {
        $rules = [
            'user_id1' => 'required|numeric',
            'user_id2' => 'required|numeric',
        ];

        $input = Input::only(
            'user_id1',
            'user_id2'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors();
        }
        $user1 = User::find(Input::get('user_id1'));
        $user2 = User::find(Input::get('user_id2'));
        if (isset($user1) && isset($user2)) {
            $kolega = UserKolega::where('user_link1', $user1->id)->where('user_link1', $user1->id)->first();
            if (!isset($kolega)) {
                $kolega = UserKolega::create([
                    'user_link1' => $user1->id,
                    'user_link2' => $user2->id,
                    'confirmed' => 0,
                ]);
            }
            return view('admin.users', compact('kolega_list'));
        } else {
            return Redirect::back()->withInput()->withErrors();
        }

    }

    public function listKolega()
    {
        $rules = [
            'user_id' => 'required|numeric',
        ];

        $input = Input::only(
            'user_id'
        );

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return Redirect::back()->withInput()->withErrors();
        }
        $kolega_list = UserKolega::where(Input::get('user_id'))->get();
        return view('admin.users_with_kolega', compact('kolega_list'));
    }

}
