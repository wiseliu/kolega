<?php namespace App\Http\Controllers;

use App\Blog;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class NewsEventController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $blogs = Blog::where('is_active',1)->get();
        return view('newsevent',compact('blogs'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.blogAdd');
	}

    public function viewAdmin()
    {
        $blogs = Blog::all();
        return view('admin.blogShow', compact('blogs'));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $rules = [
            'title' => 'required',
            'slug' => 'required|AlphaDash|unique:blogs',
            'content' => 'required',
            'summary' => 'required',
            'tags' => 'required'
        ];

        $input = Input::only(
            'title',
            'slug',
            'content',
            'summary',
            'tags'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $picture = Input::file('picture');
        $uploaded_picture_path = "";
        $rules = array('picture' => 'required|mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
        $validator = Validator::make(array('picture'=> $picture), $rules);

        if($validator->passes()){
            $destinationPath = 'img/newsevent';
            $filename = strtolower(Input::get('slug')).".".$picture->getClientOriginalExtension();
            $upload_success = $picture->move($destinationPath, $filename);

            if($upload_success) {
                $uploaded_picture_path = $destinationPath."/".$filename;
            }
        }

        if($uploaded_picture_path == ""){
            return Redirect::back()->withInput()->withErrors($validator);
        }

        Blog::create([
            'title' => Input::get('title'),
            'slug' => strtolower(Input::get('slug')),
            'content' => Input::get('content'),
            'summary' => Input::get('summary'),
            'tags' => Input::get('tags'),
            'picture_url' => $uploaded_picture_path,
            'is_active' => Input::has('isactive'),
            'posted_by' => Auth::user()->id
        ]);

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{
        $blog = Blog::where('slug',$slug)->first();
        return view('newseventSingle', compact('blog'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $blog = Blog::find($id);
        return view('admin.blogEdit', compact('blog'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $rules = [
            'title' => 'required',
            'slug' => 'required|AlphaDash',
            'content' => 'required',
            'summary' => 'required',
            'tags' => 'required'
        ];

        $input = Input::only(
            'title',
            'slug',
            'content',
            'summary',
            'tags'
        );

        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $blog = Blog::find($id);

        if (Input::hasFile('picture')) {
            $picture = Input::file('picture');
            $uploaded_picture_path = "";
            $rules = array('picture' => 'mimes:png,jpeg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('picture'=> $picture), $rules);

            if($validator->passes()){
                $file_path = public_path()."/".$blog->picture_url;
                File::delete($file_path);
                $destinationPath = 'img/newsevent';
                $filename = strtolower(Input::get('slug')).".".$picture->getClientOriginalExtension();
                $upload_success = $picture->move($destinationPath, $filename);

                if($upload_success) {
                    $uploaded_picture_path = $destinationPath."/".$filename;
                }
            }

            if($uploaded_picture_path == ""){
                return Redirect::back()->withInput()->withErrors($validator);
            }
        }


        $blog->title = Input::get('title');
        $blog->content = Input::get('content');
        $blog->summary = Input::get('summary');
        $blog->tags = Input::get('tags');
        $blog->is_active = Input::has('isactive');
        $blog->posted_by = Auth::user()->id;

        $blog->save();

        return Redirect::back()->with('action', 'success');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $blog = Blog::find($id);
        $file_path = public_path()."/".$blog->picture_url;
        File::delete($file_path);
        $blog->delete();
        return Redirect::back();

	}

}
