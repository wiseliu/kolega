<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

class Provent extends Model
{

    use SearchableTrait;

    /**
     * Searchable rules.
     *
     * @var array
     */
    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'provents.name' => 30,
            'provents.description' => 10,
        ]
    ];
    protected $table = 'provents';
    protected $fillable = ['name', 'description', 'start_date', 'end_date', 'approved', 'type', 'user_id', 'img_url', 'location'];

    public function proventMembers()
    {
        return $this->hasMany('App\ProventMember');
    }

    public function approvedProventMembers()
    {
        return $this->hasMany('App\ProventMember')->where('approved', 1);
    }
    
    public function nonApprovedProventMembers()
    {
        return $this->hasMany('App\ProventMember')->where('approved', 0);
    }

    public function proventComments()
    {
        return $this->hasMany('App\ProventComment');
    }

    public function members()
    {
        return $this->belongsToMany('App\User', 'provent_members');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
