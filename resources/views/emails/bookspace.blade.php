@extends('emails.master')

@section('title', 'Booking')

@section('introduction', $user->firstname.' '.$user->lastname.' has just booked a space in Kolega!')

@section('content')
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                        <tr>
                                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" class="textContent">
                                                                        <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">{{$type}} has been booked in {{$place}} on {{$date}} by {{$user->firstname.' '.$user->lastname}} (<a href="mailto:{{$user->email}}">{{$user->email}}</a>). Please respond as soon as possible.</h3>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // FLEXIBLE CONTAINER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // CENTERING TABLE -->
                    </td>
                </tr>
                <!-- // MODULE ROW -->
            </table>
            <!-- // CENTERING TABLE -->
        </td>
    </tr>
@stop