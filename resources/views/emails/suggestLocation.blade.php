@extends('emails.master')

@section('title', 'User Location Suggestion')

@section('introduction', 'Someone just suggest you to open a new location fr your expansion!')

@section('content')
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                        <tr>
                                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" class="textContent">
                                                                        <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">{{$name}} (<a href="mailto:{{$email}}">{{$email}}</a>) has just suggested you to open at {{$location}}</h3>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // FLEXIBLE CONTAINER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // CENTERING TABLE -->
                    </td>
                </tr>
                <!-- // MODULE ROW -->
            </table>
            <!-- // CENTERING TABLE -->
        </td>
    </tr>

    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr style="padding-top:0;">
                    <td align="center" valign="top">
                        <table border="0" cellpadding="30" cellspacing="0" width="500" class="flexibleContainer">
                            <tr>
                                <td style="padding-top:0;" align="center" valign="top" width="500" class="flexibleContainerCell">
                                    <table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton" style="background-color: #222;">
                                        <tr>
                                            <td align="center" valign="middle" class="buttonContent" style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
                                                <a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;" href="{{ URL::to('admin') }}" target="_blank">Go to admin page</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
@stop