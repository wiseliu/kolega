@extends('emails.master')

@section('title', 'Accepted')

@section('introduction', 'Thanks for registering to Kolega! Please verify your email.')

@section('content')
    <tr>
        <td align="center" valign="top">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="500" class="flexibleContainer">
                                        <tr>
                                            <td align="center" valign="top" width="500" class="flexibleContainerCell">
                                                <table border="0" cellpadding="30" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="center" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" class="textContent">
                                                                        <h3 style="color:#5F5F5F;line-height:125%;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:normal;margin-top:0;margin-bottom:3px;text-align:left;">Your Project/Event has been accepted.</h3>
                                                                        <div style="text-align:left;font-family:Helvetica,Arial,sans-serif;font-size:15px;margin-bottom:0;margin-top:3px;color:#5F5F5F;line-height:135%;">One of your Project/Event has been accepted.</div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // FLEXIBLE CONTAINER -->
                                </td>
                            </tr>
                        </table>
                        <!-- // CENTERING TABLE -->
                    </td>
                </tr>
                <!-- // MODULE ROW -->
            </table>
            <!-- // CENTERING TABLE -->
        </td>
    </tr>
@stop