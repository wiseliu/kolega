<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Kol&eacute;ga">

        <!--favicon icon-->
        <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

        <title>Kol&eacute;ga Community - @yield('title')</title>

        <!--common style-->

        <link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('css/custom.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/shortcodes/shortcodes.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.theme.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/default-theme.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/swal-forms.css') }}" rel="stylesheet">

        <style>
            .top-profile-image{
                width: 29px;
                height: 29px;
                border-radius: 50%;
                margin-right: 10px;
            }
        </style>

        <link href="{{ URL::asset('css/style-community.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/responsive.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/swiper.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/dark.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/font-icons.css') }}" rel="stylesheet">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style>
            .top-profile-image{
                width: 29px;
                height: 29px;
                border-radius: 50%;
                margin-right: 10px;
            }
        </style>

        @yield('additionalCss')

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
        <script src="{{ URL::asset('js/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body class="stretched side-header left-nav-fixed dark-side-nav" style="background-color: #f2f2f2">

        <!--  preloader start -->
        <div id="tb-preloader">
            <div class="tb-preloader-wave"></div>
        </div>
        <!-- preloader end -->


        <div class="wrapper">

            <!--header start-->
            <!--    <header id="header" class=" header-full-width ">
            
                    <div class="dark-header">
            
                        <div class="container">
                            <div id="massive-menu" class="menuzord">
            
                                <div class="logo-area text-center">
                                    logo start
                                    <a href="/" class="logo-brand">
                                        <img class="retina" src="{{ URL::asset('img/logo-dark.png') }}" alt="Kolega logo"/>
                                    </a>
                                    logo end
                                </div>
            
                                mega menu start
                                <ul class="menuzord-menu">
                                    <li class="">
                                        <a href="/">
                                            <i class="fa fa-home" style="margin: 8px 5px 0 0;"></i>Home
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="/provent">
                                            <i class="fa fa-comments" style="margin: 8px 5px 0 0;"></i>Project and Events
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="/community">
                                            <i class="fa fa-map-marker" style="margin: 8px 5px 0 0;"></i>Locations
                                        </a>
                                    </li>
                                    @if(Auth::check())
                                        <li id="responsive-profile">
                                            <a href="/community/{{Auth::user()->id}}">
                                                @if(Auth::user()->photo_url != null)
                                                    <img src="{{ URL::asset(Auth::user()->photo_url) }}" alt="{{Auth::user()->firstname." ".Auth::user()->lastname}}">
                                                @else
                                                    <img data-name="{{Auth::user()->firstname}}" class="top-profile-image" />
                                                @endif
                                                {{Auth::user()->firstname." ".Auth::user()->lastname}}
                                            </a>
                                            <ul class="dropdown">
                                                <li><a href="/community/{{Auth::user()->id}}">My Profile</a></li>
                                                <li><a href="javascript:;" onclick="openSuggestFriendDialog()">Suggest a Friend</a></li>
                                                <li><a href="/logout">Log Out</a></li>
                                            </ul>
                                        </li>
                                    @endif
                                </ul>
                                mega menu end
                            </div>
                        </div>
                    </div>
            
                    <div class="side-social-link clearfix visible-md visible-lg">
                        <a href="https://www.facebook.com/kolegaco/"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/kolegaco/"><i class="fa fa-instagram"></i></a>
                        <a href="https://plus.google.com/u/1/105455213192384785868"><i class="fa fa-google-plus"></i></a>
                    </div>
                </header>-->

            <header id="header" class="no-sticky">

                <div id="header-wrap">

                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id="logo" class="nobottomborder">
                            <!-- <a href="index.html" class="standard-logo" data-dark-logo="images/logo-side-dark.png"><h1 style="color:white;font-style: italic;">KOLEGA</h1></a> -->
                            <a href="/" class="standard-logo" data-dark-logo="{{ URL::asset('images/Logo-kolega-white.png') }}"><img src="{{ URL::asset('images/Logo-kolega-white.png')}}" alt="Canvas Logo"></a>
                            <a href="/" class="retina-logo" data-dark-logo="{{ URL::asset('images/Logo-kolega-white.png')}}"><img src="{{ URL::asset('images/Logo-kolega-white.png')}}" alt="Canvas Logo"></a>
                        </div><!-- #logo end -->

                        <!-- Primary Navigation
                        ============================================= -->
                        <nav id="primary-menu">

                            <ul style="-webkit-padding-start: 0px;">
                                <li class="">
                                    <a href="/">
                                        <div>|  HOME</div>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/community">
                                        <div>|  Community</div>
                                    </a>
                                </li>
                                <li class="">
                                    <a href="/provent">
                                        <div>|  Project & Events</div>
                                    </a>
                                </li>

                                @if(Auth::check())
                                <li id="responsive-profile"><a href="/community/{{Auth::user()->id}}">My Profile</a></li>
                                <li id="responsive-profile"><a href="javascript:;" onclick="openSuggestFriendDialog()">Suggest a Friend</a></li>
                                <li id="responsive-profile"><a href="/logout">Log Out</a></li>
                                @endif
                            </ul>

                            <div class="side-social-link clearfix visible-md visible-lg">
                                <a href="https://www.facebook.com/kolegaco/"><i class="fa fa-facebook"></i></a>
                                <a href="https://www.instagram.com/kolegaco/"><i class="fa fa-instagram"></i></a>
                                <a href="https://plus.google.com/u/1/105455213192384785868"><i class="fa fa-google-plus"></i></a>
                            </div>

                        </nav><!-- #primary-menu end -->

                    </div>

                </div>

            </header><!-- #header end -->

            @if(Auth::check())
            <div class="top_nav">
                <div class="nav_menu" style="background-color: #ffffff;position: fixed;height: 59px;z-index: 100;">
                    <nav>
                        <ul class="nav navbar-nav navbar-right" style="position: fixed;top: 0;right: 0;">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    @if(Auth::user()->photo_url != null)
                                    <img src="{{ URL::asset(Auth::user()->photo_url) }}" alt="{{Auth::user()->firstname." ".Auth::user()->lastname}}">
                                    @else
                                    <img data-name="{{Auth::user()->firstname}}" class="top-profile-image"/>
                                    @endif
                                    {{Auth::user()->firstname." ".Auth::user()->lastname}}
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="/community/{{Auth::user()->id}}">My Profile</a></li>
                                    <li><a href="javascript:;" onclick="openSuggestFriendDialog()">Suggest a Friend</a></li>
                                    <li><a href="/logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            @endif

            @yield('content')

        </div>


        <!-- Placed js at the end of the document so the pages load faster -->
        <script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/menuzord.js') }}"></script>
        <script src="{{ URL::asset('js/breakpoint.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.flexslider-min.js') }}"></script>
        <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.isotope.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.countTo.js') }}"></script>
        <script src="{{ URL::asset('js/breakpoint.js') }}"></script>
        <script src="{{ URL::asset('js/smooth.js') }}"></script>
        <script src="{{ URL::asset('js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('js/imagesloaded.js') }}"></script>
        <script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>
        <script src="{{ URL::asset('js/swal-forms.js') }}"></script>
        <script src="{{ URL::asset('js/initial.min.js') }}"></script>
        <script src="{{ URL::asset('js/linkify.min.js') }}"></script>
        <script src="{{ URL::asset('js/linkify-jquery.min.js') }}"></script>
        <script src="{{ URL::asset('js/functions.js') }}"></script>
        <!--common scripts-->
        <script src="{{ URL::asset('js/scripts.js?6') }}"></script>

        <script type="text/javascript">
                                        $(document).ready(function () {
                                            $('.top-profile-image').initial();
                                            $('.default-profile-picture').initial();
                                            var bodyclass = $("body").attr("class");
                                            var responsiveProfile = $("li#responsive-profile");
                                            $.BreakPoint({
                                                breakpoints: {
                                                    tablet: {
                                                        max: 768,
                                                        callback: function () {
                                                            $("body").removeClass(bodyclass);
                                                            $(".top_nav").hide();
                                                            responsiveProfile.show();
                                                        }
                                                    },
                                                    desktop: {
                                                        min: 769,
                                                        callback: function () {
                                                            $("body").addClass(bodyclass);
                                                            $(".top_nav").show();
                                                            responsiveProfile.hide();
                                                        }
                                                    }
                                                }
                                            });
                                        });

                                        function openSuggestFriendDialog() {
                                            swal.withFormAsync({
                                                title: "Your friend's email",
                                                text: "Enjoy Koléga Community? Invite your friend now!",
                                                showCancelButton: true,
                                                confirmButtonColor: '#222',
                                                confirmButtonText: 'Send',
                                                closeOnConfirm: false,
                                                showLoaderOnConfirm: true,
                                                formFields: [
                                                    {id: 'email', name: 'email', placeholder: "Email address", required: true}
                                                ]
                                            }).then(function (context) {
                                                if (context._isConfirm) {
                                                    var formData = {
                                                        email: context.swalForm.email,
                                                        _token: '{{csrf_token()}}'
                                                    };
                                                    $.ajax({
                                                        type: 'POST',
                                                        url: '/community/suggestFriend',
                                                        data: formData,
                                                        dataType: 'json',
                                                        success: function (data) {
                                                            if (data.success) {
                                                                swal({
                                                                    title: "Sent!",
                                                                    text: "Thank you for inviting your friend.",
                                                                    type: "success",
                                                                    confirmButtonColor: "#222"
                                                                });
                                                            } else {
                                                                var errorMessage = "";
                                                                $.each(data.errors, function (index, value) {
                                                                    errorMessage += value + "\n";
                                                                });
                                                                swal({
                                                                    title: "Failed",
                                                                    text: errorMessage,
                                                                    type: "error",
                                                                    confirmButtonColor: "#222"
                                                                });
                                                            }
                                                        },
                                                        error: function (data) {
                                                            console.log('Error:', data);
                                                        }
                                                    });
                                                }
                                            })
                                        }
        </script>

        @yield('additionalJs')

    </body>
</html>

