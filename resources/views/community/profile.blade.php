@extends('community.master')

@section('title', $user->firstname." ".$user->lastname)

@section('content')

    <!--page title start-->
    <section class="page-title banner-background" style="background-color: #f2f2f2">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    @if($user->id == Auth::user()->id)
                        <div class="team-member">
                            <div class="team-img" onclick="uploadImage()">
                                @if($user->photo_url != null)
                                    <img class="round-image pull-right user-pp" src="{{ URL::asset($user->photo_url) }}" style="width: 250px; height:250px">
                                @else
                                    <img class="round-image pull-right default-profile-picture user-pp" style="width: 250px; height:250px" data-name="{{$user->firstname}}" >
                                @endif
                            </div>
                            <div class="round-image team-hover pull-right" style="width: 250px;height: 250px;left:auto" onclick="uploadImage()">
                                <div class="desk">
                                    <h4><br><br><br><br><br><i class="icon-camera"></i><br>Change profile picture</h4>
                                </div>
                            </div>
                        </div>
                        <form method="post" action="/community/uploadProfilePicture" enctype="multipart/form-data">
                            <input type="file" name="profile_picture" id="profile_picture" onchange="readUrl(this)" style="display: none;" accept="image/jpeg, image/png" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            @if($user->photo_url != null)
                                <div class="row" style="margin: 0">
                                    <button id="remove-button" class="btn btn-small btn-dark-solid pull-right" value="remove" style="margin-right: 77px;margin-top: 10px" onclick="return removeProfilePicture()">Remove</button>
                                </div>
                            @endif
                            <button id="upload-button" class="btn btn-small btn-dark-solid pull-right" value="upload" style="margin-right: 79px;margin-top: 10px; display: none" onclick="return checkFileSize()">Upload</button>
                        </form>
                    @else
                        @if($user->photo_url != null)
                            <img class="round-image pull-right" src="{{ URL::asset($user->photo_url) }}" style="width: 250px; height:250px">
                        @else
                            <img class="round-image pull-right default-profile-picture" style="width: 250px; height:250px" data-name="{{$user->firstname}}" >
                        @endif
                    @endif
                </div>
                <div class="col-md-6">
                    <section class="icon-box-tabs ">
                        <ul class="nav nav-pills">
                            <li class="active">
                                <a data-toggle="tab" href="#tab-1">
                                    <i class="icon-documents"></i>
                                </a>
                            </li>
                            <li class="">
                                <a data-toggle="tab" href="#tab-2">
                                    <i class="icon-comment_with_dot"></i>
                                </a>
                            </li>
                            @if($user->id == Auth::user()->id)
                                <li class="" id="edit-profile">
                                    <a data-toggle="tab" href="#tab-3" id="tab-3-head">
                                        <i class="icon-basic_settings"></i>
                                    </a>
                                </li>
                            @endif
                        </ul>
                        <div class="panel-body">
                            <div class="tab-content">
                                <div id="tab-1" class="tab-pane active">
                                    <h1 class="text-uppercase">{{$user->firstname." ".$user->lastname}}</h1>
                                    <h5><a href="mailto:{{$user->email}}">{{$user->email}}</a></h5>
                                    <span class="text-uppercase">
                                        {{$user->job_title}}
                                        @if($user->company != null)
                                            {{" at ".$user->company}}
                                        @endif
                                        @if($user->web != null)
                                            / <a href="{{$user->web}}" target="_blank" style="text-transform: none"> {{$user->web}}</a>
                                        @endif
                                    </span>
                                    <span>{{$user->interest}}</span>
                                </div>
                                <div id="tab-2" class="tab-pane">
                                    @if($user->about == null)
                                        Currently there is no description about me..
                                    @else
                                        {{$user->about}}
                                    @endif
                                </div>
                                @if($user->id == Auth::user()->id)
                                    <div id="tab-3" class="tab-pane">
                                        <dl class="toggle" style="width: 100%;">
                                            @if(Session::get('action') == 'epsuccess')
                                                <div class="alert alert-success" role="alert">
                                                    <i class="fa fa-lg fa-check"></i> <strong>Success!</strong> Your password has been changed.
                                                </div>
                                            @elseif(Session::get('action') == 'eusuccess')
                                                <div class="alert alert-success" role="alert">
                                                    <i class="fa fa-lg fa-check"></i> <strong>Success!</strong> Your profile has been updated.
                                                </div>
                                            @elseif(count($errors))
                                                <div class="alert danger-border" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    @foreach($errors->all() as $error)
                                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                                    @endforeach
                                                </div>
                                            @endif
                                            <dt>
                                                <a href="">Change Password</a>
                                            </dt>
                                            <dd>
                                                <form method="post" action="/community/changePassword" id="form" role="form" class="contact-comments">
                                                    <div class="form-group">
                                                        <input type="password" name="password" class="form-control " placeholder="Password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="password" name="password_confirmation" class="form-control " placeholder="Confirm Password" required>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button class="btn btn-small btn-dark-solid pull-right" value="change">Change</button>
                                                    </div>
                                                </form>
                                            </dd>
                                            <dt>
                                                <a href="">Edit Profile</a>
                                            </dt>
                                            <dd>
                                                <form method="post" action="/community/editProfile" id="form" role="form" class="contact-comments">
                                                    <div class="form-group col-md-6" style="padding-right: 5px; padding-left: 0px">
                                                        <input type="text" name="job_title" value="{{$user->job_title}}" class="form-control" placeholder="Job Title *" required>
                                                    </div>
                                                    <div class="form-group col-md-6" style="padding-left: 5px; padding-right: 0px">
                                                        <input type="text" name="company" value="{{$user->company}}" class="form-control" placeholder="Company">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="url" name="web" value="{{$user->web}}" class="form-control" placeholder="Website">
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="text" name="interest" value="{{$user->interest}}" class="form-control" placeholder="Interests">
                                                    </div>
                                                    <div class="form-group col-md-12" style="padding: 0">
                                                        <textarea name="about" class="cmnt-text form-control" rows="6" placeholder="Description about me.." maxlength="255">{{$user->about}}</textarea>
                                                    </div>
                                                    <div class="form-group">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <button class="btn btn-small btn-dark-solid pull-right" value="update">Update</button>
                                                    </div>
                                                </form>
                                            </dd>
                                        </dl>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->
@stop

@section('additionalJs')
    <script type="text/javascript">
        @if(count($errors) || Session::get('action') == 'epsuccess' || Session::get('action') == 'eusuccess')
            $('#tab-3-head').trigger('click');
        @elseif(Session::get('action') == 'eppsuccess')
            swal({
                title: "Success",
                text: "Your profile picture has been updated!",
                type: "success",
                confirmButtonColor: "#222"
            });
        @endif

        function uploadImage(){
            $("input[id='profile_picture']").focus().click();
        }

        function readUrl(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('.user-pp')
                        .attr('src', e.target.result)
                        .width(250)
                        .height(250);
                    $('#upload-button').show();
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        function removeProfilePicture(){
            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover your profile picture!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#222",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
                },
                function(){
                    $('#profile_picture').val('');
                    $('#remove-button').closest("form").submit();
            });
            return false;
        }

        function checkFileSize(){
            if($('#profile_picture')[0].files[0].size > 200000 || $('#profile_picture')[0].files[0].fileSize > 200000){ //200kb
                swal({
                    title: "Error",
                    text: "File can't be larger than 200 KB.",
                    type: "error",
                    confirmButtonColor: "#222"
                });
                return false;
            } else {
                return true;
            }
        }

    </script>
@stop