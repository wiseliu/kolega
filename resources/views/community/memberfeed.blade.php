@extends('community.master')

@section('title', 'Member Feed')

@section('content')
    <!--body content start-->
    <section class="body-content" style="background-color: #f2f2f2">
        <div class="page-content col-md-offset-1" @if(!Auth::check())style="padding: 50px 0;"@endif>
            <div class="container">
                <div class="row">
                    <div class="col-md-7" id="feed-parent" style="max-width: 100%">
                        @if(Auth::check())
                            <div class="row feed-item post-section" style="padding: 0px">
                                <form method="post" action="" id="feed-form" role="form" class="contact-comments" enctype="multipart/form-data">
                                    <textarea name="feed" id="text" class="cmnt-text form-control" rows="6" placeholder="What do you need right now?" style="resize: none;"></textarea>
                                    <div style="border-top: 1px solid #D9DEE4;"></div>
                                    <input type="file" name="image" id="image" class="inputfile" />
                                    <label for="image" style="text-transform: none"> <i class="fa fa-camera"></i> <span>Add Image</span></label>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button id="submit" class="btn btn-dark-solid" style="float:right;margin-right: 0">
                                        Post
                                    </button>
                                    <p id="submit-load" style="margin: 0;float:right;display:none">
                                        <img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">
                                    </p>
                                </form>
                            </div>
                        @endif
                        @include('community.feeds')
                    </div>
                    <div class="col-md-3" >
                        @if(count($query) > 0)
                            <div class="row feed-item">
                                <div class="post-desk text-center">
                                    <h4 class="text-uppercase">
                                        Top Contributor
                                    </h4>
                                    <?php $top_contributor = \App\User::find($query[0]->posted_by) ?>

                                    @if($top_contributor->photo_url != null)
                                        <img class="round-image" src="{{ URL::asset($top_contributor->photo_url) }}" alt="{{$top_contributor->firstname." ".$top_contributor->lastname}}" width="150" height="150">
                                    @else
                                        <img class="round-image default-profile-picture" data-name="{{$top_contributor->firstname}}" width="150" height="150" />
                                    @endif
                                    <div style="margin-top: 30px">

                                        <a href="/community/{{$top_contributor->id}}" style="font-size: large;"><strong>{{$top_contributor->firstname." ".$top_contributor->lastname}}</strong></a>
                                        <br>
                                        <span style="font-size: medium;">
                                            {{$top_contributor->job_title}}
                                            @if($top_contributor->company != null)
                                                {{" at ".$top_contributor->company}}
                                            @endif
                                        </span>
                                    </div>

                                    <p>{{$top_contributor->about}}<br><a href="{{$top_contributor->web}}" target="_blank">{{$top_contributor->web}}</a></p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="row" id="feed-load" style="display: none">
                    <div class="col-md-3 col-md-offset-2 text-center">
                        <p style="margin: 0"><img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">Loading older feeds..</p>
                    </div>
                </div>
                <div class="row" id="feed-empty" style="display: none">
                    <div class="col-md-7" >
                        <div class="row feed-item text-center">
                            <p style="margin: 0">No more feeds</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--body content end-->
@stop

@section('additionalJs')
    <script src="{{ URL::asset('js/jquery.timeago.js') }}"></script>
    <script src="{{ URL::asset('js/jquery.custom-file-input.js') }}"></script>
    <script type="text/javascript">

        @if(!Auth::check())
        swal({
            title: "Warning!",
            text: "You need to <a href='login' style='color:#d1ac61'><strong><u>LOGIN</u></strong></a> in order to enjoy full features of this community. Do you want to continue instead?",
            html: true,
            confirmButtonText: "YES",
            confirmButtonColor: "#222"
        });
        @endif

        function giveFunction(){
            jQuery("time.timeago").timeago();
            $('p').linkify({
                target: "_blank"
            });
            @if(Auth::check())
                assignEnterSubmit();
            @endif
            $('#feed-parent').children('.row.feed-item').each(function (indexParent) {
                @if(Auth::check())if(indexParent != 0){@endif
                    var comments = $(this).children().children().children('ul.media-list.comments-list.clearlist').children();
                    var feedContent = $(this).children().children().children('p').html();
                    var feedContentFull = $(this).children().children().children('p').html();

                    if(feedContent.length > 400){
                        $(this).children().children().children('p').html(feedContent.substr(0,400)+"... "+
                            "<a href='javascript:void(0);' onclick='showFullContent(this)'>See More</a>");
                    }

                    for(var i=0;i<comments.length;i++){
                        if(i < comments.length - 6){
                            $(comments[i]).hide();
                        }
                    }

                    if(comments.length > 7){
                        $(comments[0]).show();
                    }
                @if(Auth::check())}@endif
            });
        }

        function showFullContent(elem){
            $(elem).parent().html($(elem).parent().attr('data-full'));
            $('p').linkify({
                target: "_blank"
            });
        }

        function showAllComments(elem){
            $(elem).parent().siblings().show();
            $(elem).parent().hide();
            $('p').linkify({
                target: "_blank"
            });
        }

        giveFunction();

        setInterval(function() {
            jQuery("time.timeago").timeago();
        }, 60 * 1000);

        var page = 1;
        var isLoading = false;
        var allFeedShown = false;
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() >= $(document).height() - 100 && !isLoading && !allFeedShown) {
                page++;
                loadMoreFeeds(page);
            }
        });

        function loadMoreFeeds(page){
            isLoading = true;
            $.ajax({
                url: '?page=' + page,
                type: "get",
                beforeSend: function(){
                    $('#feed-load').show();
                }
            })
            .done(function(data){
                if(data.html == ""){
                    $('#feed-load').hide();
                    $('#feed-empty').show();
                    allFeedShown = true;
                    return;
                }
                $('#feed-load').hide();
                $("#feed-parent").append(data.html);
                giveFunction();
                $('.default-profile-picture').initial();
                isLoading = false;
            })
            .fail(function(jqXHR, ajaxOptions, thrownError){
                swal({
                    title: "Failed",
                    text: 'Server not responding...',
                    type: "error",
                    confirmButtonColor: "#222"
                });
                isLoading = false;
            });
        }

        @if(Auth::check())
            $('#submit').click(function () {
                $('#feed-form').submit(function(event){
                    event.preventDefault();
                    if($.trim($('#text').val()) == "" && $('#image').get(0).files.length == 0){
                        return false;
                    }
                    if($('#image')[0].files[0] != null && ($('#image')[0].files[0].size > 500000 || $('#image')[0].files[0].fileSize > 500000)){ //500kb
                        swal({
                            title: "Error",
                            text: "File can't be larger than 500 KB.",
                            type: "error",
                            confirmButtonColor: "#222"
                        });
                        return false;
                    }
                    $('#submit').hide();
                    $('#submit-load').show();
                    $.ajax({
                        url: '/community/post',
                        type: "POST",
                        data: new FormData($("#feed-form")[0]),
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.success) {
                                $('#feed-form').trigger("reset");
                                $("#image").val('');
                                $('.inputfile').next('label').find('span').html("Add Image");

                                var postedHtml = '<div class="row feed-item">'+
                                    '<ul class="media-list comments-list clearlist">'+
                                        '<li class="media">'+
                                            '<a class="pull-left">';
                                                @if(Auth::user()->photo_url != null)
                                                    postedHtml += '<img class="media-object comment-avatar round-image" src="{{ URL::asset(Auth::user()->photo_url) }}" alt="{{Auth::user()->firstname." ".Auth::user()->lastname}}" width="60" height="60">';
                                                @else
                                                    postedHtml += '<img class="media-object comment-avatar round-image default-profile-picture" data-name="{{Auth::user()->firstname}}" width="60" height="60" />';
                                                @endif

                                postedHtml +='</a>'+
                                            '<div class="media-body">'+
                                                '<div class="comment-info">'+
                                                    '<div class="comment-author" style="margin-bottom: -4px;">'+
                                                        '<a href="/community/{{Auth::user()->id}}" style="font-size: large;"><strong>{{Auth::user()->firstname." ".Auth::user()->lastname}}</strong></a>'+
                                                        '<div class="dropdown pull-right">'+
                                                            '<button type="button" class="close dropdown-toggle" data-toggle="dropdown">'+
                                                            '<span class="fa fa-angle-down"></span>'+
                                                            '</button>'+
                                                            '<ul class="dropdown-menu pull-right" role="menu" style="padding: 0px; min-width: 80px">'+
                                                            '<li><a href="javascript:;" onclick="editFeed(this)">Edit</a></li>'+
                                                            "<li><a href=\"javascript:;\" onclick=\"deleteFeed(this,\""+response.feed.id+"\")\">Delete</a></li>"+
                                                            '</ul>'+
                                                        '</div>'+

                                                    '</div>'+
                                                    '<span style="font-size: medium;">{{Auth::user()->job_title}}';

                                                    @if(Auth::user()->company != null)
                                                        postedHtml += '{{" at ".Auth::user()->company}}';
                                                    @endif


                                postedHtml +=       '</span>' +
                                                    '<p style="line-height: 8px; font-size: small;"><time class="timeago" datetime="'+response.feed.updated_at+'"></time></p>'+
                                                '</div>'+
                                            '</div>'+
                                            '<p style="margin-top: -10px;margin-bottom:0;line-height: 20px;"  data-full="'+response.feed.content+'">'+response.feed.content+'</p>';


                                postedHtml += "<form method=\"post\" action=\"\" id=\"feed-form\" role=\"form\" class=\"contact-comments\" style=\"display: none\">";
                                postedHtml +=    "<textarea name=\"feed\" class=\"cmnt-text form-control\" rows=\"6\" style=\"resize: none; height: 150px\">"+response.feed.content+"</textarea>";
                                postedHtml +=    "<div style=\"border-top: 1px solid #D9DEE4;\"></div>";
                                postedHtml +=    "<input type=\"hidden\" name=\"_token\" value=\"{{csrf_token()}}\">";
                                postedHtml +=    "<input type=\"hidden\" name=\"feed_id\" value=\""+response.feed.id+"\">";
                                postedHtml +=    "<button class=\"btn btn-dark-solid\" onclick=\"saveEditedFeed(this)\" style=\"float:right;margin-right: 0;margin-bottom: 20px\">";
                                postedHtml +=        "Save";
                                postedHtml +=    "</button>";
                                postedHtml +=    "<p style=\"margin: 0;float:right;display:none\">";
                                postedHtml +=        "<img src=\"{{ URL::asset('img/AjaxLoader.gif') }}\" style=\"margin-right: 10px\">";
                                postedHtml +=    "</p>";
                                postedHtml +=    "<button class=\"btn btn-dark-solid\" onclick=\"cancelEdit(this)\" style=\"float:right;margin-right: 2px;margin-bottom: 20px\">";
                                postedHtml +=        "Cancel";
                                postedHtml +=    "</button>";
                                postedHtml += "</form>";

                                if(response.feed.img_url != null){
                                    postedHtml += '<img src="{{URL::to('/')}}/'+response.feed.img_url+'" alt="{{Auth::user()->firstname."'s feed image"}}" width="100%">';
                                }

                                postedHtml += '<div class="divider before-like-comment" style="margin: 0"></div>'+
                                              '<a href="javascript:void(0);" style="color:#7e7e7e; margin-right: 30px" onclick="likeFeed(this,\''+response.feed.id+'\')"><i class="fa fa-thumbs-o-up"></i> 0</a>'+
                                              '<a style="color:#7e7e7e;" ><i class="fa fa-comment-o"></i> 0</a>'+
                                              '<div class="divider" style="margin: 0;top: -10px"></div>'+
                                              '<ul class="media-list comments-list clearlist">'+
                                                '<li class="media">'+
                                                    '<a class="pull-left" >';
                                                        @if(Auth::user()->photo_url != null)
                                                            postedHtml += '<img class="media-object comment-avatar round-image" src="{{ URL::asset(Auth::user()->photo_url) }}" alt="{{Auth::user()->firstname." ".Auth::user()->lastname}}" width="40" height="40">';
                                                        @else
                                                            postedHtml += '<img class="media-object comment-avatar round-image default-profile-picture" data-name="{{Auth::user()->firstname}}" width="40" height="40" />';
                                                        @endif
                                postedHtml += '</a>'+
                                                    '<div class="media-body">'+
                                                        '<form method="post" action="" id="comment-form" role="form" class="contact-comments">'+
                                                            '<input type="text" name="comment" id="comment" class="form-control" placeholder="Write a comment.." maxlength="255">'+
                                                            '<input type="hidden" name="feed_id" value="'+response.feed.id+'">'+
                                                            '<input type="hidden" name="_token" value="{{ csrf_token() }}">'+
                                                        '</form>'+
                                                    '</div>'+
                                                '</li>'+
                                            '</ul>'+
                                        '</li>'+
                                    '</ul>'+
                                '</div>';
                                $(".post-section").after(postedHtml);
                                $('.default-profile-picture').initial();
                                giveFunction();
                            } else {
                                var errorMessage = "";
                                $.each(response.errors, function (index, value) {
                                    errorMessage += value + "\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $('#submit').show();
                            $('#submit-load').hide();
                        },
                        error: function (response) {
                            console.log('Error:', response);
                        }
                    });
                    return false;
                });
            });

            function likeFeed(elem, id){
                $(elem).attr('onclick', 'function()');
                $.ajax({
                    url: '/community/likeFeed',
                    type: "POST",
                    data: {"feed_id":id,"liked_by":"{{Auth::user()->id}}","_token":"{{ csrf_token() }}"},
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $(elem).before('<a href="javascript:void(0);" style="color:#d6b161; margin-right: 30px" onclick="unlikeFeed(this,\''+response.feed_id+'\')"><i class="fa fa-thumbs-o-up"></i> '+response.likeCount+'</a>');
                            $(elem).remove();
                        }
                    },
                    error: function (response) {
                        console.log('Error:', response);
                    }
                });
            }

            function unlikeFeed(elem, id){
                $(elem).attr('onclick', 'function()');
                $.ajax({
                    url: '/community/unlikeFeed',
                    type: "POST",
                    data: {"feed_id":id,"liked_by":"{{Auth::user()->id}}","_token":"{{ csrf_token() }}"},
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $(elem).before('<a href="javascript:void(0);" style="color:#7e7e7e; margin-right: 30px" onclick="likeFeed(this,\''+response.feed_id+'\')"><i class="fa fa-thumbs-o-up"></i> '+response.likeCount+'</a>');
                            $(elem).remove();
                        }
                    },
                    error: function (response) {
                        console.log('Error:', response);
                    }
                });
            }

            function assignEnterSubmit(){
                $("[id=comment-form]").keypress(function(event) {
                    if (event.which == 13) {
                        event.preventDefault();

                        var comment = $(':focus');
                        if(comment.val() == ""){
                            return false;
                        }

                        var commentForm = comment.closest("form");
                        commentForm.submit(false);
                        commentForm.unbind("keypress");

                        $.ajax({
                            url: '/community/postComment',
                            type: "POST",
                            data: new FormData($(commentForm)[0]),
                            processData: false,
                            contentType: false,
                            success: function (response) {
                                if (response.success) {
                                    comment.blur();
                                    comment.val("");

                                    var parentFeed = commentForm.parent().parent().parent().parent().parent().parent();

                                    parentFeed.before(response.htmlRespond);
                                    parentFeed.remove();
                                    $('.default-profile-picture').initial();
                                    giveFunction();
                                } else {
                                    var errorMessage = "";
                                    $.each(response.errors, function (index, value) {
                                        errorMessage += value + "\n";
                                    });
                                    swal({
                                        title: "Failed",
                                        text: errorMessage,
                                        type: "error",
                                        confirmButtonColor: "#222"
                                    });
                                }
                            },
                            error: function (response) {
                                console.log('Error:', response);
                            }
                        });

                        return false;
                    }
                });
            }

            function likeComment(elem, commentId){
                $(elem).attr('onclick', 'function()');
                $.ajax({
                    url: '/community/likeComment',
                    type: "POST",
                    data: {"comment_id":commentId,"liked_by":"{{Auth::user()->id}}","_token":"{{ csrf_token() }}"},
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $(elem).before('<a href="javascript:void(0);" style="color:#d6b161; margin-right: 30px" onclick="unlikeComment(this,\''+response.comment_id+'\')"><i class="fa fa-thumbs-o-up"></i> '+response.likeCount+'</a>');
                            $(elem).remove();
                        }
                    },
                    error: function (response) {
                        console.log('Error:', response);
                    }
                });
            }

            function unlikeComment(elem, commentId){
                $(elem).attr('onclick', 'function()');
                $.ajax({
                    url: '/community/unlikeComment',
                    type: "POST",
                    data: {"comment_id":commentId,"liked_by":"{{Auth::user()->id}}","_token":"{{ csrf_token() }}"},
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            $(elem).before('<a href="javascript:void(0);" style="color:#7e7e7e; margin-right: 30px" onclick="likeComment(this,\''+response.comment_id+'\')"><i class="fa fa-thumbs-o-up"></i> '+response.likeCount+'</a>');
                            $(elem).remove();
                        }
                    },
                    error: function (response) {
                        console.log('Error:', response);
                    }
                });
            }

            function editFeed(elem){
                var contentPara = $(elem).parent().parent().parent().parent().parent().parent().next();
                var editBox = contentPara.next();

                contentPara.hide();
                editBox.show();
            }

            function cancelEdit(elem){
                $(elem).parent().submit(function(event){
                    var editBox = $(elem).parent();
                    var contentPara = editBox.prev();

                    editBox.hide();
                    contentPara.show();
                    return false;
                });
                return false;
            }

            function saveEditedFeed(elem){
                $(elem).parent().submit(function(event){
                    var newContent = $(elem).prev().prev().prev().prev();

                    if($.trim(newContent.val()) == "" ){
                        return false;
                    }

                    $(elem).hide();
                    $(elem).next().show();
                    $.ajax({
                        url: '/community/editFeed',
                        type: "POST",
                        data: new FormData($(elem).parent()[0]),
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.success) {
                                var editBox = $(elem).parent();
                                var contentPara = editBox.prev();

                                editBox.val(response.newContent);
                                contentPara.html(response.newContent);
                                contentPara.attr("data-full",response.newContent);

                                editBox.hide();
                                contentPara.show();
                                giveFunction();
                            }
                            $(elem).show();
                            $(elem).next().hide();
                        },
                        error: function (response) {
                            console.log('Error:', response);
                        }
                    });
                    return false;
                });
                return false;
            }

            function deleteFeed(elem,feedId){
                swal({
                        title: "Are you sure?",
                        text: "You will not be able to recover this feed!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#222",
                        confirmButtonText: "Yes",
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    },
                    function(){
                        $.ajax({
                            url: '/community/deleteFeed',
                            type: "POST",
                            data: {"feed_id":feedId,"_token":"{{ csrf_token() }}"},
                            dataType: 'json',
                            success: function (response) {
                                if (response.success) {
                                    $(elem).parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();
                                    swal({
                                        title: "Deleted!",
                                        type: "success",
                                        text: "Your feed was succesfully deleted!",
                                        confirmButtonColor: "#222"
                                    });
                                }
                            },
                            error: function (response) {
                                console.log('Error:', response);
                            }
                        });
                });
            }
        @endif

    </script>
@stop