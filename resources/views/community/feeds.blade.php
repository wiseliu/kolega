@foreach($feeds as $feed)
    <div class="row feed-item">
        <ul class="media-list comments-list clearlist">
            <li class="media">
                <?php $user = App\User::find($feed->posted_by) ?>
                <a class="pull-left">
                    @if($user->photo_url != null)
                        <img class="media-object comment-avatar round-image" src="{{ URL::asset($user->photo_url) }}" alt="{{$user->firstname." ".$user->lastname}}" width="60" height="60">
                    @else
                        <img class="media-object comment-avatar round-image default-profile-picture" data-name="{{$user->firstname}}" width="60" height="60" />
                    @endif
                </a>

                <div class="media-body">
                    <div class="comment-info">
                        <div class="comment-author" style="margin-bottom: -4px;">
                            <a href="/community/{{$user->id}}" style="font-size: large;"><strong>{{$user->firstname." ".$user->lastname}}</strong></a>
                            @if(Auth::check() && $user->id == Auth::user()->id)
                                <div class="dropdown pull-right">
                                    <button type="button" class="close dropdown-toggle" data-toggle="dropdown">
                                        <span class="fa fa-angle-down"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu" style="padding: 0px; min-width: 80px">
                                        <li><a href="javascript:;" onclick="editFeed(this)">Edit</a></li>
                                        <li><a href="javascript:;" onclick="deleteFeed(this,'{{$feed->id}}')">Delete</a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                        <span style="font-size: medium;">
                            {{$user->job_title}}
                        @if($user->company != null)
                            {{" at ".$user->company}}
                        @endif
                        </span>
                        <p style="line-height: 8px; font-size: small;"><time class="timeago" datetime="{{$feed->updated_at}}"></time></p>
                    </div>
                </div>

                <p style="margin-top: -10px;margin-bottom:0;line-height: 20px;" data-full="{{nl2br($feed->content)}}">{!!nl2br(str_replace('', "'",$feed->content))!!}</p>

                @if(Auth::check() && $user->id == Auth::user()->id)
                    <form method="post" action="" id="feed-form" role="form" class="contact-comments" style="display: none">
                        <textarea name="feed" class="cmnt-text form-control" rows="6" style="resize: none; height: 150px">{{$feed->content}}</textarea>
                        <div style="border-top: 1px solid #D9DEE4;"></div>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="feed_id" value="{{$feed->id}}">
                        <button class="btn btn-dark-solid" onclick="saveEditedFeed(this)" style="float:right;margin-right: 0;margin-bottom: 20px">
                            Save
                        </button>
                        <p style="margin: 0;float:right;display:none">
                            <img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">
                        </p>
                        <button class="btn btn-dark-solid" onclick="cancelEdit(this)" style="float:right;margin-right: 2px;margin-bottom: 20px">
                            Cancel
                        </button>

                    </form>
                @endif

                @if($feed->img_url != null)
                    <img src="{{ URL::asset($feed->img_url) }}" alt="{{$user->firstname."'s feed image"}}" width="100%">
                @endif

                <div class="divider before-like-comment" style="margin: 0"></div>
                @if(Auth::check())
                    @if($feed->isLikedBy($feed->id,Auth::user()->id))
                        <a href="javascript:void(0);" style="color:#d6b161; margin-right: 30px" onclick="unlikeFeed(this,'{{$feed->id}}')"><i class="fa fa-thumbs-o-up"></i> {{count($feed->likes)}}</a>
                    @else
                        <a href="javascript:void(0);" style="color:#7e7e7e; margin-right: 30px" onclick="likeFeed(this,'{{$feed->id}}')"><i class="fa fa-thumbs-o-up"></i> {{count($feed->likes)}}</a>
                    @endif
                @else
                    <a style="color:#7e7e7e; margin-right: 30px" ><i class="fa fa-thumbs-o-up"></i> {{count($feed->likes)}}</a>
                @endif

                <a style="color:#7e7e7e;" ><i class="fa fa-comment-o"></i> {{count($feed->comments)}}</a>

                <div class="divider" style="margin: 0;top: -10px"></div>
                <ul class="media-list comments-list clearlist">
                    <li class="media" style="display: none">
                        <a class="pull-left" href="javascript:;" onclick="showAllComments(this)">
                            View all comments
                        </a>
                    </li>

                    @foreach($feed->comments as $comment)
                        <li class="media">
                            <?php $comment_user = \App\User::find($comment->posted_by) ?>
                            <a class="pull-left" href="/community/{{$comment_user->id}}">
                                @if($comment_user->photo_url != null)
                                    <img class="media-object comment-avatar round-image" src="{{ URL::asset($comment_user->photo_url) }}" alt="{{$comment_user->firstname." ".$comment_user->lastname}}" width="40" height="40">
                                @else
                                    <img class="media-object comment-avatar round-image default-profile-picture" data-name="{{$comment_user->firstname}}" width="40" height="40" />
                                @endif
                            </a>
                            <div class="media-body">
                                <div class="comment-info" style="margin-top:-5px">
                                    <div class="comment-author" style="margin-bottom: -8px;">
                                        <a href="/community/{{$comment_user->id}}" style="color: #323232">{{$comment_user->firstname." ".$comment_user->lastname}}</a>
                                    </div>
                                    <span style="font-size: small;">{{$comment_user->job_title}}</span>
                                    @if($comment_user->company != null)
                                        {{" at ".$comment_user->company}}
                                    @endif
                                    <p style="line-height: 3px; font-size: smaller;margin-bottom: 10px"><time class="timeago" datetime="{{$comment->updated_at}}"></time></p>
                                </div>
                                <p style="margin:0;line-height: 20px;">
                                    {{$comment->comment}}
                                    <br>
                                    @if(Auth::check())
                                        @if($comment->isLikedBy($comment->id,Auth::user()->id))
                                            <a href="javascript:void(0);" style="color:#d6b161; margin-right: 30px" onclick="unlikeComment(this,'{{$comment->id}}')"><i class="fa fa-thumbs-o-up"></i> {{count($comment->likes)}}</a>
                                        @else
                                            <a href="javascript:void(0);" style="color:#7e7e7e; margin-right: 30px" onclick="likeComment(this,'{{$comment->id}}')"><i class="fa fa-thumbs-o-up"></i> {{count($comment->likes)}}</a>
                                        @endif
                                    @else
                                        <a style="color:#7e7e7e; margin-right: 30px" ><i class="fa fa-thumbs-o-up"></i> {{count($comment->likes)}}</a>
                                    @endif
                                </p>
                            </div>
                        </li>
                    @endforeach

                    @if(Auth::check())
                        <li class="media">
                            <a class="pull-left">
                                @if(Auth::user()->photo_url != null)
                                    <img class="media-object comment-avatar round-image" src="{{ URL::asset(Auth::user()->photo_url) }}" alt="{{Auth::user()->firstname." ".Auth::user()->lastname}}" width="40" height="40">
                                @else
                                    <img class="media-object comment-avatar round-image default-profile-picture" data-name="{{Auth::user()->firstname}}" width="40" height="40" />
                                @endif
                            </a>
                            <div class="media-body">
                                <form method="post" action="" id="comment-form" role="form" class="contact-comments">
                                    <input type="text" name="comment" id="comment" class="form-control" placeholder="Write a comment.." maxlength="255">
                                    <input type="hidden" name="feed_id" value="{{$feed->id}}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </li>
                    @endif
                </ul>
            </li>
        </ul>
    </div>
@endforeach