@foreach($provents as $provent)
<?php $user = App\User::find($provent['user_id']) ?>
<section class="container-fluid content-clearfix row feed-item" id="feed-item" style="background-color: white">
    <div class="row" id="feed-content">
        <div class="col-md-4 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
            @if($provent['img_url'] != null)
            <img src="{{ URL::asset($provent['img_url']) }}" alt="{{$user->firstname."'s feed image"}}" width="100%">
            @endif
        </div>
        <div class="col-md-8 col-sm-12 col-xs-12 content-desc" style="padding-left: 15px;">
            <a href="/provent/{{$provent['id']}}" style="font-size: x-large; font-family: 'Open Sans Condensed', sans-serif;" data-full="{{$provent['description']}}"><strong>
                    <b>{{$provent['name']}}</b>
                </strong></a>
            <p style="line-height: 8px; font-size: small; margin-bottom: 5px;"><time class="timeago" datetime="{{$provent['updated_at']}}"></time></p>
            <hr class="hr-content-desx">

            <div class="col-md-6 col-xs-6 col-sm-6" style="padding-right: 0px;">
                <a style="color:#7e7e7e;" ><i class="fa fa-calendar" style="margin-right: 10px" ></i> {{date('d/m/Y',strtotime($provent['start_date']))}}</a>
            </div>
            <div class="col-md-6 col-xs-6 col-sm-6" style="padding-right: 0px;">
                <a style="color:#7e7e7e;" ><i class="fa fa-user" style="margin-right: 10px" ></i> {{$user->firstname." ".$user->lastname}}</a>
            </div>
            
            <div class="col-md-12 col-xs-12 col-sm-12" style="padding-right: 0px;">
                <a style="color:#7e7e7e;" ><i class="fa fa-map-marker" style="margin-right: 10px" ></i> {{$provent['location']}}</a>
            </div>

            <div class="col-md-12 col-xs-12 col-sm-12" id="description" style="margin-top: 10px; padding: 0px;">
                <p style="margin:0;line-height: 20px;" data-full="{!!nl2br($provent['description'])!!}">{!!nl2br($provent['description'])!!}</p>
            </div>
        </div>
    </div>
    <hr class="hr-content-desx">
    <div class="row">
        <div class="col-md-8 col-md-offset-4 col-sm-12 col-xs-12 d-flex align-center j-content-end">
            <ul class="entry-meta pull-right nomargin clearfix">
                @if($provent['approved'])
                <a style="color:#7e7e7e; margin-right: 30px" ><i class="fa fa-check green" title="Approved Project/Event" style="margin-right: 10px" ></i></a>
                @endif
                <a style="color:#7e7e7e; margin-right: 30px" ><i class="fa fa-user"></i> {{count($provent['approvedProventMembers'])}}</a>
                <a style="color:#7e7e7e; margin-right: 30px"><i class="fa fa-comment-o"></i> {{count($provent['proventComments'])}}</a>
            </ul>
        </div>
        <hr>
    </div>
</section>
@endforeach