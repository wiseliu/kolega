@extends('provent.master')

@section('title', 'Feeds')

@section('content')
<!--body content start-->
<section class="body-content" style="background-color: #f2f2f2">
            <div id="header-wrap" class="transparent-header sticky-header" style="background-color: White !important; height: 60px;">
                <div class="container clearfix">
                    <div class="col-md-9 col-sm-12 col-xs-12 single-member-icon m-top-10" style="padding: 0px; padding-right: 5px; margin-bottom: 5px;">
                        <form action="/provent/search" method="GET">
                            <div class="col-md-10 col-sm-10 col-xs-9 nopadding" style="padding: 0px;">
                                <input placeholder="Search Project.." type="text" name="q" class="form-search-com form-control">
                            </div>
                            <div class="col-md-1 col-sm-2 col-xs-3 nopadding" style="padding: 0px;">
                                <button type="submit" class="form-control form-search-submit-com"><i class="fa fa-search fa-sm" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div>
                    <div class="topbar-right topbar-overlay-home col-md-2 pull-right visible-md visible-lg" style="padding : 0px;">
                        @if(Auth::check())
                            <a href="/logout">
                                <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                            </a>
                            <span id="login-regis-home" style="float: right"><a href="/logout">Logout</a></span>
                        @else
                            <a href="/login">
                                <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                            </a>
                            <span id="login-regis-home" style="float: right"><a href="/login">Login / Signup</a></span>
                        @endif

                    </div>
                </div>
            </div>
    
    <div class="page-content" style="padding: 50px 0;">
         <div class="container" style="width: auto!important;">
            <div class="col-md-3 col-md-push-8 col-sm-12 col-xs-12" >
                    
                    @if(Auth::check())
                        <div class="row feed-item post-section clearfix" style="padding: 0px;">
                            <h4 class="text-center">
                                Menu
                            </h4>
                            <a href="/provent/create" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%">
                                Create New Project or Event
                            </a>
                        </div>
                        
                    @endif
                </div>
            <div class="row">
                <div class="col-md-8 col-md-pull-3 col-sm-12 col-xs-12" id="feed-parent">
                    <div class="row post-section clearfix" style="padding: 0px; margin-bottom: 10px">
                        <div class="col-md-6 col-sm-6 col-xs-6" id="feed-parent">
                            <a href="/provent/events" class="btn btn-block btn-dark-solid">
                                View Events only
                            </a>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6" id="feed-parent">
                            <a href="/provent/projects" class="btn btn-block btn-dark-solid">
                                View Projects only
                            </a>
                        </div>
                    </div>
                    @include('provent.feeds')
                </div>
            </div>
            <div class="row" id="feed-load" style="display: none">
                <div class="col-md-3 col-md-offset-2 text-center">
                    <p style="margin: 0"><img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">Loading older feeds..</p>
                </div>
            </div>
            <div class="row" id="feed-empty" style="display: none">
                <div class="col-md-7" >
                    <div class="row feed-item text-center">
                        <p style="margin: 0">No more feeds</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--body content end-->
@stop

@section('additionalJs')
<script src="{{ URL::asset('js/jquery.timeago.js') }}"></script>
<script src="{{ URL::asset('js/jquery.custom-file-input.js') }}"></script>
<script type="text/javascript">
@if (!Auth::check())
        swal({
        title: "Warning!",
                text: "You need to <a href='login' style='color:#d1ac61'><strong><u>LOGIN</u></strong></a> in order to enjoy full features of this project and event. Do you want to continue instead?",
                html: true,
                confirmButtonText: "YES",
                confirmButtonColor: "#222"
        });
@endif

        function giveFunction(){
        jQuery("time.timeago").timeago();
        $('p').linkify({
        target: "_blank"
        });
            $('#feed-item').children('#feed-content').each(function (indexParent) {
                @if (Auth::check()){@endif
                var feedContent = $(this).children(".content-desc").children("#description").children('p').html();
                var feedContentFull = $(this).children().children().children('p').html();
                if (feedContent.length > 100){
                $(this).children(".content-desc").children("#description").children('p').html(feedContent.substr(0, 100) + "... " +
                        "<a href='javascript:void(0);' onclick='showFullContent(this)'>See More</a>");
                }

        @if (Auth::check())}@endif
        });
        }

function showFullContent(elem){
$(elem).parent().html($(elem).parent().attr('data-full'));
$('p').linkify({
target: "_blank"
});
}

giveFunction();
setInterval(function() {
jQuery("time.timeago").timeago();
}, 60 * 1000);
var page = 1;
var isLoading = false;
var allFeedShown = false;
$(window).scroll(function() {
if ($(window).scrollTop() + $(window).height() >= $(document).height() - 100 && !isLoading && !allFeedShown) {
page++;
loadMoreFeeds(page);
}
});
function loadMoreFeeds(page){
isLoading = true;
$.ajax({
url: '?page=' + page,
        type: "get",
        beforeSend: function(){
        $('#feed-load').show();
        }
})
        .done(function(data){
        if (data.html == ""){
        $('#feed-load').hide();
        $('#feed-empty').show();
        allFeedShown = true;
        return;
        }
        $('#feed-load').hide();
        $("#feed-parent").append(data.html);
        giveFunction();
        $('.default-profile-picture').initial();
        isLoading = false;
        })
        .fail(function(jqXHR, ajaxOptions, thrownError){
        swal({
        title: "Failed",
                text: 'Server not responding...',
                type: "error",
                confirmButtonColor: "#222"
        });
        isLoading = false;
        });
}
</script>
@stop