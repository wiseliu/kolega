@extends('provent.master')

@section('title', ucwords($provent['type']).' Detail')

@section('content')
<!--body content start-->
<section class="body-content" style="background-color: #f2f2f2">
    <div id="header-wrap" class="transparent-header sticky-header" style="background-color: White !important; height: 60px;">
        <div class="container clearfix">
            <div class="col-md-9 col-sm-9 col-xs-8 single-member-icon m-top-10" style="padding: 0px; padding-right: 5px;">
                <form action="search.php" method="GET">
                    <div class="col-md-10 col-sm-10 col-xs-9 nopadding" style="padding: 0px;">
                        <input placeholder="Search Project.." type="text" name="query" class="form-search-com form-control">
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 nopadding" style="padding: 0px;">
                        <button type="submit" class="form-control form-search-submit-com"><i class="fa fa-search fa-sm" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <div class="topbar-right topbar-overlay-home col-md-2 col-sm-3 col-xs-4 pull-right" style="padding : 0px;">
                @if(Auth::check())
                <a href="/logout">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/logout">Logout</a></span>
                @else
                <a href="/login">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/login">Login / Signup</a></span>
                @endif

            </div>
        </div>
    </div>

    <div class="page-content" style="padding: 50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-8" id="feed-parent">
                    @if(session()->has('action'))
                    <div class="alert alert-success">
                        {{ session()->get('action') }}
                    </div>
                    @endif
                    <?php $user = App\User::find($provent['user_id']) ?>
                    <section class="container-fluid content-clearfix row feed-item" style="background-color: white">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">
                                @if($provent['img_url'] != null)
                                <img src="{{URL::asset($provent['img_url']) }}" alt="{{$user->firstname."'s feed image"}}" width="100%">
                                     @endif
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 content-desc text-center" style="padding-left: 15px;">
                                <h1 data-full="{{$provent['description']}}" style="margin: 0px;"><strong>
                                        {{$provent['name']}}
                                    </strong></h1>

                                <hr class="hr-content-desx">

                                <div class="col-md-12 col-xs-12 col-sm-12 detail-row">
                                    <div class="col-md-12 col-xs-12 col-sm-12" style="padding: 0px;">
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <h5 style="margin-bottom: 0px;">
                                                <strong>{{ucwords($provent['type'])}} Detail</strong>
                                            </h5> 
                                        </div>
                                        <div class="col-md-12 col-xs-12 col-sm-12" style="padding-right: 0px;">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <a style="color:#7e7e7e;" href="/community/{{$user->id}}" ><i class="fa fa-user" style="margin-right: 10px" ></i>{{$user->firstname." ".$user->lastname}}</a>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <a style="color:#7e7e7e;" ><i class="fa fa-clock-o" style="margin-right: 10px" ></i> Last updated <time class="timeago" datetime="{{$provent['updated_at']}}"></time></a>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <a style="color:#7e7e7e;" >
                                                    <i class="fa fa-flag" style="margin-right: 10px" ></i>
                                                    @if($provent['approved'])
                                                    {{"Approved"}} <i class="fa fa-check green" style="margin-right: 10px" ></i>
                                                    @else
                                                    {{"Not Yet Approved"}} <i class="fa fa-times red" style="margin-right: 10px"></i>
                                                    @endif
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xs-12 col-sm-12 detail-row">
                                    <h5 style="margin-bottom: 0px;">
                                        <strong>{{ucwords($provent['type'])}} Time</strong>
                                    </h5> 

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <a style="color:#7e7e7e;" ><i class="fa fa-calendar" style="margin-right: 10px" ></i> <b>Start</b> : {{date('D, d F Y',strtotime($provent['start_date']))}}</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <a style="color:#7e7e7e;" ><i class="fa fa-clock-o" style="margin-right: 10px" ></i> {{date('h:i A',strtotime($provent['start_date']))}}</a>
                                    </div>

                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <a style="color:#7e7e7e;" ><i class="fa fa-calendar" style="margin-right: 10px" ></i> <b>End</b> : {{date('D, d F Y',strtotime($provent['end_date']))}}</a>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <a style="color:#7e7e7e;" ><i class="fa fa-clock-o" style="margin-right: 10px" ></i> {{date('h:i A',strtotime($provent['end_date']))}}</a>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xs-12 col-sm-12 detail-row" style="padding: 0px;">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <h5 style="margin-bottom: 0px;">
                                            <strong>{{ucwords($provent['type'])}} Location</strong>
                                        </h5> 
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12" style="padding-right: 0px;">
                                        <a style="color:#7e7e7e;" ><i class="fa fa-map-marker" style="margin-right: 10px" ></i> {{$provent['location']}}</a>
                                    </div>
                                </div>

                                <div class="col-md-12 col-xs-12 col-sm-12 detail-row" style="margin-top: 20px; padding: 0px;">
                                    <div class="col-md-12 col-xs-12 col-sm-12">
                                        <h5 style="margin-bottom: 0px;">
                                            <strong>{{ucwords($provent['type'])}} Description</strong>
                                        </h5> 
                                    </div>
                                    <div class="col-md-12" style="margin:0;line-height: 20px;">
                                        {!!nl2br($provent['description'])!!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hr-content-desx">
                        <div class="row">
                            <div class="col-md-8 pull-left">
                                @if(count($provent['approvedProventMembers'])==1)
                                <a style='color: black'>
                                    <?php $mem = App\User::find($provent['approvedProventMembers'][0]['user_id']) ?>
                                    <a href="/community/{{$user->id}}"><strong>{{$mem->firstname." ".$mem->lastname}}</strong></a>
                                </a>
                                {{ "has joined this"}}
                                @elseif(count($provent['approvedProventMembers'])==2)
                                <a style='color: black'>
                                    <?php $mem1 = App\User::find($provent['approvedProventMembers'][0]['user_id']) ?>
                                    <a href="/community/{{$user->id}}"><strong>{{$mem1->firstname." ".$mem1->lastname}}</strong></a>
                                    {{" and "}}
                                    <?php $mem2 = App\User::find($provent['approvedProventMembers'][1]['user_id']) ?>
                                    <a href="/community/{{$user->id}}"><strong>{{$mem2->firstname." ".$mem2->lastname}}</strong></a>
                                </a>
                                {{ "have joined this"}}
                                @elseif(count($provent['approvedProventMembers'])>=2)
                                <a style='color: black'>
                                    <?php $mem1many = App\User::find($provent['approvedProventMembers'][0]['user_id']) ?>
                                    <a href="/community/{{$user->id}}"><strong>{{$mem1many->firstname." ".$mem1many->lastname}}</strong></a>
                                    {{" and "}}
                                    <?php $mem2many = App\User::find($provent['approvedProventMembers'][1]['user_id']) ?>
                                    <a href="/community/{{$user->id}}"><strong>{{$mem2many->firstname." ".$mem2many->lastname}}</strong></a>
                                </a>
                                {{ "and ".(count($provent['approvedProventMembers'])-2)." others have joined this"}}
                                @endif
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12 d-flex align-center j-content-end">
                                <ul class="entry-meta pull-right nomargin clearfix">
                                    <a style="color:#7e7e7e; margin-right: 30px" ><i class="fa fa-user"></i> {{count($provent['approvedProventMembers'])}}</a>
                                    <a style="color:#7e7e7e; margin-right: 30px"><i class="fa fa-comment-o"></i> {{count($provent['proventComments'])}}</a>
                            </div>
                            <hr>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">

                                <form method="post" action="" id="feed-form" role="form" enctype="multipart/form-data">
                                    @if($errors->first()!="")
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="description" style="text-transform: none"><span>Comments</span></label>
                                        <textarea style="width:100%; padding: 15px;" name="comment" class="cmnt-text form-control" rows="3" placeholder="Write a comment...." required></textarea>
                                    </div>

                                    <button id="submit" class="btn btn-dark-solid" style="float:right;margin-right: 0">
                                        Add Comment
                                    </button>
                                    <p id="submit-load" style="margin: 0;float:right;display:none">
                                        <img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">
                                    </p>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 25px;">
                            <ul class="media-list comments-list clearlist">
                                @foreach($provent['proventComments'] as $comment)
                                <li class="media" style="border-bottom: 1px solid #D9DEE4;">
                                    <?php $comment_user = \App\User::find($comment['user_id']) ?>
                                    <a class="pull-left" href="/community/{{$comment_user->id}}">
                                        @if($comment_user->photo_url != null)
                                        <img class="media-object comment-avatar round-image" src="{{ URL::asset($comment_user->photo_url) }}" alt="{{$comment_user->firstname." ".$comment_user->lastname}}" width="40" height="40">
                                        @else
                                        <img class="media-object comment-avatar round-image default-profile-picture" data-name="{{$comment_user->firstname}}" width="40" height="40" />
                                        @endif
                                    </a>
                                    <div class="media-body">
                                        <div class="comment-info" style="margin-top:-5px">
                                            <div class="comment-author" style="margin-bottom: -8px;">
                                                <a href="/community/{{$comment_user->id}}"><strong>{{$comment_user->firstname." ".$comment_user->lastname}}</strong></a>
                                            </div>
                                            <span style="font-size: small;">{{$comment_user->job_title}}</span>
                                            @if($comment_user->company != null)
                                            {{" at ".$comment_user->company}}
                                            @endif
                                            <p style="line-height: 3px; font-size: smaller;margin-bottom: 10px"><time class="timeago" datetime="{{$comment->updated_at}}"></time></p>
                                        </div>
                                        <p style="margin:0;line-height: 20px; padding-bottom: 10px;">
                                            {{$comment->comment}}
                                            <br>
                                        </p>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </section>





                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="row feed-item post-section clearfix" style="padding: 0px;">
                        <h4 class="text-center">
                            User Menu
                        </h4>
                        @if(Auth::check() && Auth::user()->id != $provent['user_id']  && !in_array(Auth::user()->id,$provent_member))
                        <form method="post" action="/provent/join" role="form" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            <input type="hidden" name="provent_id" value="{{$provent['id']}}">
                            <button id="submit" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%;">
                                Request Join {{$provent['type']}}
                            </button>
                        </form>
                        @endif
                        @if(Auth::check() && Auth::user()->id == $provent['user_id'])
                        <a href="/provent/{{$provent['id']}}/edit" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%;">
                            Edit {{$provent['type']}}
                        </a>
                        @if(count($provent['nonApprovedProventMembers'])>0)
                        <a href="#" data-toggle="modal" data-target="#requestModal" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%;">
                            Join Request ({{count($provent['nonApprovedProventMembers'])}})
                        </a>
                        @endif
                        @if(count($provent['approvedProventMembers'])>0)
                        <a href="#" data-toggle="modal" data-target="#membersModal" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%;">
                            View All Joined Members ({{count($provent['approvedProventMembers'])}})
                        </a>
                        @endif
                        @endif
                    </div>

                    @if(Auth::check() && Auth::user()->privilege == 'admin' && Auth::user()->is_verified)
                    <div class="row feed-item post-section clearfix" style="padding: 0px;">
                        <h4 class="text-center">
                            Admin Menu
                        </h4>
                        @if(!$provent['approved'])
                        <a href="/provent/{{$provent['id']}}/approve" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%;background-color: lightgreen">
                            Approve {{$provent['type']}}
                        </a>
                        <a href="/provent/{{$provent['id']}}/reject" class="btn btn-block btn-dark-solid" style="margin: 5px; width: 97%; background-color: lightcoral">
                            Reject {{$provent['type']}}
                        </a>
                        @endif
                    </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="requestModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Join Requests</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Detail</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($provent['nonApprovedProventMembers'] as $member)
                        <tr>
                            <?php $user = App\User::find($member['user_id']) ?>
                            <td><a href="/community/{{$member['user_id']}}" style="color: black;">{{$user->firstname." ".$user->lastname}}</a></td>
                            <td>{{$user->job_title}}
                                            @if($user->company != null)
                                            {{" at ".$user->company}}
                                            @endif
                            </td>
                            <td>
                                <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left; background-color: lightgreen" onclick="showApprovalDialog({{$member['provent_id']}}, {{$member['user_id']}}, '{{$user->firstname." ".$user->lastname}}')">Approve</a>
                                <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left; background-color: lightcoral" onclick="showRejectDialog('{{$provent['name']}}', {{$provent['id']}})">Reject</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="membersModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Joined Members</h4>
            </div>
            <div class="modal-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Detail</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($provent['approvedProventMembers'] as $member)
                        <tr>
                            <?php $user = App\User::find($member['user_id']) ?>
                            <td>{{$user->firstname." ".$user->lastname}}</td>
                            <td>{{$user->job_title}}
                                            @if($user->company != null)
                                            {{" at ".$user->company}}
                                            @endif
                            </td>
                            <td><a href="/community/{{$member['user_id']}}">Check Profile</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--body content end-->
@stop

@section('additionalJs')
<script src="{{URL::asset('js/jquery.timeago.js') }}"></script>
<script src="{{ URL::asset('js/jquery.custom-file-input.js') }}"></script>
<script type="text/javascript">
                                    jQuery("time.timeago").timeago();
                                    
   function showApprovalDialog(provent_id, user_id, user_name){
//    var provent = JSON.parse(provent);
    var user_name = user_name;
    var user_id = user_id;
    var provent_id = provent_id;
    swal.withFormAsync({
    title: 'Approve ' + user_name + '?',
            showCancelButton: true,
            confirmButtonColor: '#222',
            confirmButtonText: 'Approve',
            closeOnConfirm: true,
            formFields: [
            { id: 'provent_approval', type:'hidden', name:'provent_approval', required: true, value:1 }
            ]
    }).then(function (context) {

    if (context._isConfirm){

    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');
    var formData = {
    privilege: context.swalForm.privilege,
            _token: '{{csrf_token()}}'
    }

    $.ajax({
    type: 'PUT',
            url: '/provent_action/' + provent_id + '/' + user_id + '/approve',
            data: formData,
            dataType: 'json',
            success: function (data) {
            if (data.success){
            swal({
            title: "Success",
                    text: "Request has been approved!",
                    type: "success",
                    confirmButtonColor: "#222"
            },
                    function(){
                    location.reload();
                    });
            } else {
            var errorMessage = "";
            $.each(data.errors, function(index, value) {
            errorMessage += value + "\n";
            });
            swal({
            title: "Failed",
                    text: errorMessage,
                    type: "error",
                    confirmButtonColor: "#222"
            });
            }
            $(".tb-preloader-wave").fadeOut();
            $("#tb-preloader").delay(200).fadeOut("slow").remove();
            },
            error: function (data) {
            console.log('Error:', data);
            }
    });
    }

    })
    }
</script>

@stop