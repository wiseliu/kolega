@extends('provent.master')

@section('title', 'Edit '.ucwords($provent['type']))

@section('content')
<!--body content start-->
<section class="body-content" style="background-color: #f2f2f2">
    <div id="header-wrap" class="transparent-header sticky-header" style="background-color: White !important; height: 60px;">
        <div class="container clearfix">
            <div class="col-md-9 col-sm-9 col-xs-8 single-member-icon m-top-10" style="padding: 0px; padding-right: 5px;">
                <form action="search.php" method="GET">
                    <div class="col-md-10 col-sm-10 col-xs-9 nopadding" style="padding: 0px;">
                        <input placeholder="Search Project.." type="text" name="query" class="form-search-com form-control">
                    </div>
                    <div class="col-md-1 col-sm-2 col-xs-3 nopadding" style="padding: 0px;">
                        <button type="submit" class="form-control form-search-submit-com"><i class="fa fa-search fa-sm" aria-hidden="true"></i></button>
                    </div>
                </form>
            </div>
            <div class="topbar-right topbar-overlay-home col-md-2 col-sm-3 col-xs-4 pull-right" style="padding : 0px;">
                @if(Auth::check())
                <a href="/logout">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/logout">Logout</a></span>
                @else
                <a href="/login">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/login">Login / Signup</a></span>
                @endif

            </div>
        </div>
    </div>

    <div class="page-content" style="padding: 50px 0;">
        <div class="container">
            <div class="row">
                <div class="col-md-8" id="feed-parent">
                    <section class="container-fluid content-clearfix row feed-item" style="background-color: white">
                        @if(Auth::check() && Auth::user()->id != $provent['user_id'])
                        <div class="alert alert-danger">
                            You cannot edit other's event or project
                        </div>
                        @elseif(Auth::check() && Auth::user()->id == $provent['user_id'])
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 10px;">

                                <h2 style="margin-bottom: 10px">
                                    Edit Event
                                </h2>

                                <hr class="hr-content-desx">

                                <form method="post" action="" id="feed-form" role="form" enctype="multipart/form-data">
                                    @if($errors->first()!="")
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach($errors->all() as $error)
                                            <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                    <input type="hidden" name="provent_id" value="{{$provent['id']}}">
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="projectname" style="text-transform: none"><span>Name</span></label>
                                        <input placeholder="Project/Event Name" name="name" id="name" type="text" value="{{old('name',$provent['name'])}}" class="form-control" required>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="type" style="text-transform: none"><span>Type</span></label>
                                        <select class="form-control" name="type" required>
                                            <option value="">Select Type... (Project/Ev                                                          ent)</option>
                                            <option value="event" <?php if (old('type', $provent['type']) == "event") echo "selected"; ?>>Event</option>
                                            <option value="project" <?php if (old('type', $provent['type']) == "project") echo "selected"; ?>>Project</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="image" style="text-transform: none"><span>Image</span></label>
                                        <input type="file" value="{{old('image',$provent['image'])}}" name="image" id="image" class="form-control" />
                                    </div>

                                    <div class="form-group col-md-8 col-sm-12 col-xs-12">
                                        <label for="start_date"><span>Start Date</span></label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                            <input type="text" name="start_date" value="{{old('start_date',explode(" ",$provent['start_date'])[0])}}" class="form-control" placeholder="2000-04-31" required>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-12 col-xs-12">
                                        <label for="start_time"><span>Time</span></label>
                                        <input type="text" name="start_time" value="{{old('start_time',substr(explode(" ",$provent['start_date'])[1],0,-3))}}" class="form-control" placeholder="22:22" required>
                                    </div>

                                    <div class="form-group col-md-8 col-sm-6 col-xs-12">
                                        <label for="end_date"><span>End Date</span></label>
                                        <div class="input-group date" data-provide="datepicker" data-date-format="yyyy-mm-dd">
                                            <input type="text" name="end_date" value="{{old('end_date',explode(" ",$provent['end_date'])[0])}}" class="form-control" placeholder="2000-04-31"  required>
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                        <label for="end_time"><span>Time</span></label>
                                        <input type="text" name="end_time" value="{{old('end_time',substr(explode(" ",$provent['end_date'])[1],0,-3))}}" class="form-control" placeholder="22:22" required>
                                    </div>


                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="location" style="text-transform: none"><span>Location</span></label>
                                        <input placeholder="Project/Event Location" value="{{old('location',$provent['location'])}}" name="location" id="location" type="text" class="form-control" required>
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                        <label for="description" style="text-transform: none"><span>Description</span></label>
                                        <textarea name="description" class="cmnt-text form-control" rows="6" placeholder="Description of the Project / Event" style="resize: none;" required>{{old('description',$provent['description'])}}</textarea>
                                    </div>

                                    <button id="submit" class="btn btn-lg btn-dark-solid" style="float:right;margin-right: 0">
                                        Edit
                                    </button>
                                    <p id="submit-load" style="margin: 0;float:right;display:none">
                                        <img src="{{ URL::asset('img/AjaxLoader.gif') }}" style="margin-right: 10px">
                                    </p>
                                </form>
                            </div>
                        </div>
                    </section>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>
<!--body content end-->
@stop

@section('additionalJs')
<script src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('js/jquery.custom-file-input.js') }}"></script>

@stop