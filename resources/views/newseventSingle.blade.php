@extends('master')

@section('title',$blog->title)

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <h4 class="text-uppercase">{{$blog->title}}</h4>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Blog</a></li>
                <li class="active">{{$blog->title}}</li>
            </ol>
        </div>
    </section>
    <!--page title end-->

    <?php $tags = explode(",",$blog->tags) ?>

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!--classic image post-->
                        <div class="blog-classic" style="margin-bottom: 0px;padding-bottom: 0px; border-bottom: 0px">
                            <div class="blog-post">
                                <div class="full-width">
                                    <img src="{{ URL::asset($blog->picture_url) }}" alt="{{$blog->title}}"/>
                                </div>
                                <h4 class="text-uppercase"><a>{{$blog->title}}</a></h4>
                                <ul class="post-meta">
                                    <li><i class="fa fa-user"></i>posted by <a>{{\App\User::find($blog->posted_by)->firstname." ".\App\User::find($blog->posted_by)->lastname}}</a></li>
                                    <li><i class="fa fa-folder-open"></i>
                                        @for($i = 0; $i<sizeof($tags); $i++)

                                            @if($i < sizeof($tags) - 1)
                                                <a href="#">{{$tags[$i].", "}}</a>
                                            @else
                                                <a href="#">{{$tags[$i]}}</a>
                                            @endif
                                        @endfor
                                    </li>
                                    <li><i class="fa fa-calendar"></i> <a>{{date('d F Y',strtotime( $blog->updated_at ))}}</a></li>
                                </ul>

                                {!! $blog->content !!}

                                <div class="inline-block">
                                    <div class="widget-tags">
                                        <h6 class="text-uppercase">Tags </h6>
                                        @foreach($tags as $tag)
                                            <a href="#">{{$tag}}</a>
                                        @endforeach
                                    </div>
                                </div>


                                <div class="clearfix inline-block m-top-50 ">
                                    <h6 class="text-uppercase">Share this Post </h6>
                                    <div class="widget-social-link circle">
                                        <a href="http://www.facebook.com/sharer.php?s=100&p" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="http://twitter.com/share?source=sharethiscom&text={{$blog->title}}" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="https://plus.google.com/share?url={{URL::current()}}" target="_blank"><i class="fa fa-google-plus"></i></a>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--classic image post-->
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!--body content end-->
@stop

@section('additionalJs')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
        tinymce.init({
            selector:'textarea',
            menubar: false
        });
    </script>
@stop