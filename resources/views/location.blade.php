@extends('master')

@section('title',$location->name)

@section('additionalCss')
    <link href="{{ URL::asset('css/foundation-datepicker.min.css') }}" rel="stylesheet">
@stop

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <h4 class="text-uppercase">{{$location->name}}</h4>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Location</a></li>
                <li class="active">{{$location->name}}</li>
            </ol>
        </div>
    </section>
    <!--page title end-->

    <!--hero section-->
    <div class="slider-boxed">
        <div class="container">
            <div class="post-slider  text-center">
                <ul class="slides">
                    @foreach($location->photos as $photo)
                        <li>
                            <img src="{{ URL::asset($photo->url) }}" alt="{{$photo->name}}" />
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <!--hero section-->

    <!--body content start-->
    <section class="body-content">
        <div class="page-content">

            <div class="container">
                <!--feature box center align start-->
                <div class="col-md-12">
                    <div class="heading-title">
                        <h3 class="text-uppercase">Amenities</h3>
                    </div>
                    @foreach($location->amenities as $amenity)
                        <div class="col-md-2">
                            <div class="featured-item text-center m-bot-10">
                                <div class="icon ">
                                    <i class="fa fa-lg {{$amenity->icon_code}}"></i>
                                </div>
                                <div class="title text-uppercase">
                                    <p>{{$amenity->name}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <div class="divider d-dashed d-single "> </div>

                <div class="col-md-12">
                    <div class="heading-title">
                        <h3 class="text-uppercase">Price List</h3>
                    </div>
                    @foreach($location->prices as $price)
                        <div class="col-md-4 m-bot-30">
                            <div class="fun-factor">
                                <div class="icon">
                                    <i class="fa fa-lg {{$price->icon_code}}"></i> {{$price->icon_note}}
                                </div>
                                <div class="fun-info">
                                    <span>{{$price->name}}</span>
                                    <h4>{{$price->price_text}}</h4>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!--feature box center align end-->
            </div>
        </div>

        <div class="testimonial-parallax ">
            <div class="overlay-dark page-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <!--testimonial start-->
                            <div id="testimonial-3" class="text-center light-txt big-icon ">
                                <div class="item">
                                    <div class="icon theme-color">
                                        <i class="icon-map"></i>
                                    </div>

                                    <div class="content">
                                        <div class="testimonial-meta col-md-4 col-md-offset-4">
                                            {{$location->address}}
                                            <br><br>
                                            {{$location->phone_number}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--testimonial end-->
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Google Map start-->
        <div id='grey-map' class="height-450"> </div>
        <!--  Google Map End-->

        <div class="page-content">

            <div class="container">

                <div class="heading-title-alt border-short-bottom text-center ">
                    <h3 class="text-uppercase">Book a tour</h3>
                    <div class="half-txt">Visit our office in {{$location->name}}. You'll love it!</div>
                </div>

                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        @if(Session::get('action') == 'success')
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-lg fa-check-circle-o"></i> <strong>Success!</strong> Your booking has been sent to our admin. Please wait until we reached you.
                            </div>
                        @elseif(count($errors))
                            <div class="alert danger-border" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                @endforeach
                            </div>
                        @endif
                        <form method="post" action="/location/book" id="form" role="form" class="contact-comments">

                            <div class="row">
                                <div class="col-md-6 ">
                                    <div class="form-group">
                                        <!-- Name -->
                                        <input type="text" name="name" id="name" class=" form-control" placeholder="Name *" maxlength="100" required="">
                                    </div>
                                    <div class="form-group">
                                        <!-- Email -->
                                        <input type="email" name="email" id="email" class=" form-control" placeholder="Email *" maxlength="100" required="">
                                    </div>
                                    <div class="form-group">
                                        <!-- phone -->
                                        <input type="text" name="visit_time" readonly id="visit_time" class=" form-control" placeholder="Visit Time *" maxlength="100">
                                    </div>
                                </div>

                                <div class="col-md-6 form-group">
                                    <div class="form-group">
                                        <!-- Comment -->
                                        <textarea name="comment" id="comment" class="cmnt-text form-control" placeholder="Comment" maxlength="400"></textarea>
                                    </div>
                                    <div class="form-group full-width">
                                        <input type="hidden" name="location" value="{{ $location->id }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-small btn-dark-solid ">
                                            Reserve
                                        </button>
                                    </div>
                                </div>

                            </div>

                        </form>
                    </div>
                </div>
            </div>

        </div>

    </section>
    <!--body content end-->
@stop

@section('additionalJs')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
    <script src="{{ URL::asset('js/jquery.gmap.min.js') }}"></script>
    <script src="{{ URL::asset('js/foundation-datepicker.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $("#grey-map").gMap({
                markers: [
                    {
                        latitude: {{$location->latitude}},
                        longitude: {{$location->longitude}},
                        html: '<div class="test_marker">{{$location->address}}</div>'

                    }
                ],
                zoom: 16,
                scrollwheel:false,
                styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
            })

            @if(Session::get('action') == 'success' || count($errors))
                $("html, body").animate({ scrollTop: $(document).height() }, 300);
            @endif
        });
        $(function(){
            $('#visit_time').fdatepicker({
                format: 'yyyy-mm-dd hh:ii:ss',
                disableDblClickSelection: true,
                pickTime: true
            });
        });
    </script>

@stop