@extends('new/master')

@section('title',"FAQ")

@section('additionalCss')
    <style>
        h3{
            font-size: 20px;
        }
        h4{
            font-size: 18px;
        }
        p{
            font-size: 16px;
            text-align: justify;
        }

        li{
            text-align: justify;
        }
    </style>
@endsection

@section('content')

<section class="location">
    <div class="wrap">
        <div class="container clearfix" style="padding-bottom: 50px; ">
            <div class="row" style="margin-top: 35px">
                <div class="col-md-6">
                    <h3>How can I become a member?</h3>
                    <p>
                        We have several packages that you could choose according to your needs. You can sign-up
                        first to our website, or you can contact us directly through this <a href="/contact">link.</a>
                    </p>
                    <h3>What does a membership at Kolega include?</h3>
                    <p>
                        When you’re a member of Koléga Coworking Space, you will be given the privileges of Koléga
                        Coworking Space locations around town. Such as Fast-Internet Connections, Meeting
                        Rooms, The use of our printing machine available in locations, amenities and all access to
                        koléga coworking space’s community events and experiences all around Koléga coworking
                        space’s territory.
                        <br><br>
                        Also by being a member of our community, you will be able to share your thoughts, connect
                        with new friends, and create new collaborations with Koléga’s vast network.
                    </p>
                    <h3>How many branches do you have?</h3>
                    <p>
                        Up until this day, we have three locations around Jakarta. They are in Tebet area, Senopati
                        area, and Antasari Area, and will open in new locations soon.
                    </p>
                    <h3>What membership plans do you offer?</h3>
                    <a href="/plans"><h4 style="margin: 0;">Kolega Membership</h4></a>
                    <p>
                        Access to our shared workspace and amenities around our locations. Have the privilege to
                        connect with other community member through our built-in networking hub. Join various events
                        held in, and by, koléga coworking space to help you and all around us to grow bigger, together.
                    </p>
                    <a href="/plans"><h4 style="margin: 0;">Dedicated Desk</h4></a>
                    <p>
                        A desk dedicated for you, located inside a specialized space for you and other dedicated
                        members. Each dedicated desk comes customizable layout, with a lockable filing cabinet,
                        and an exclusive bathroom for our exclusive dedicated members, alongside all of the basic
                        one-month- subscription amenities.
                    </p>
                    <a href="/plans"><h4 style="margin: 0;">Office Suites</h4></a>
                    <p>
                        We have fully customizable, adjustable, private offices that could fit a 2-100 seats. Suitable
                        for you and your company.
                    </p>
                    <h3>When are you open?</h3>
                    <p>
                        Kolega Coworking Spaces opens from 9 AM – 9 PM on weekdays (mon-fri) at all locations,
                        9AM -5 PM for weekend, and we are closed to observe public holiday
                    </p>
                    <h3>Can I stop in or should I make an appointment for a tour?</h3>
                    <p>
                        Tours must be scheduled in advance. This ensures our availability and helps our team focus
                        on helping you choose the best space. You can simply contact us by using “GET IN TOUCH,
                        BE OUR COLLEAGUES” in our home page to schedule a tour
                    </p>
                    <h3>What types of communities/members work there?</h3>
                    <p>
                        Our community is an immense and diverse community, holding on to the same ideas, the
                        idea to grow exponentially and to create impacts.
                    </p>
                    <h3>How fast is the internet?</h3>
                    <p>
                        Our internet connection is fast, up to 100mbps. And we provide a backup connection, just in
                        case :D
                    </p>
                    <h3>Can I use kolega to hold my event, even if I’m not a member?</h3>
                    <p>
                        Sure! Please email us your inquiry to info@kolega.co
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection