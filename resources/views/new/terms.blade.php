@extends('new/master')

@section('title',"Terms and Conditions")

@section('additionalCss')
    <style>
        p{
            text-align: justify;
        }

        ul{
            margin-left: 50px;
        }

        li{
            text-align: justify;
        }
    </style>
@endsection

@section('content')

<section class="location">
    <div class="wrap">
        <div class="container clearfix" style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <div class="col-md-12">
                    <h2>TERM</h2>
                    <p>
                        Kolega Co-Working Space own and operates the website at www.kolega.co (the site), where you can
                        find information about our product and services. These website terms and conditions describe the
                        right obligations of an unregistered website user or visitor in connection with your use of the site.
                    </p>
                    <p>
                        By accessing or using the site in any way, including as an unregistered website visitor, you agree to
                        be bound by these website terms and our privacy policy, which is available on the site. These
                        website terms apply only to your use of the site and the content made available on or through the
                        site, as an unregistered website user or visitor. If you use or access any of our physical space,
                        restricted access web based service (i.e requiring a login), the broker pr referral program or other
                        services we provide, your use of such space, services or program is subject to the terms and
                        conditions you received or accepted when you singed up for such space, services or program.
                    </p>
                    <p>
                        From time to time, we may make modifications, delections or additions to the site or these website
                        term. You continued of the site following the posting of any changes to the website terms constitues
                        acceptance of these changes.
                    </p>
                    <h2>Eligibility</h2>
                    <p>
                        You must at least 16 years old to register for the services. If you are registered as a host, you must
                        have the authority and capacity to enter into valid, legally binding license contrasts in respect of the
                        listed properties that you are listing on the platform. No one under this age may access or use the
                        site or provide any personal information through the site.
                    </p>
                    <h2>Content</h2>
                    <p>
                        The text, images, video, audioclips, software and other content generated, provided or otherwise
                        made accessible on or throught the site are contributed by our licensors. For member, the standards
                        set out in this clause apply to your use of a feature that allows you to upload content to our site
                        (member feed) where you can communicate with another user, such as by using workspace social.
                        Your content and communications must not:
                    </p>
                    <ul>
                        <li>Contain any material which is defamatory of any person;</li>
                        <li>Contain any material which is obscene, offensive, hateful or inflammatory;</li>
                        <li>Promote sexually explicit material;</li>
                        <li>Promote violence;</li>
                        <li>Promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li>
                        <li>Promote any illegal activity;</li>
                        <li>Infringe any copyright, database right or trademark of any other person;</li>
                        <li>Be likely to deceive any person;</li>
                        <li>Be made in breach of any legal duty owed to a third party, such as a contractual duty or a duty of confidence;</li>
                        <li>Be threating, abuse or invade another’s privacy or cause annoyance, inconvenience or needles anxiety;</li>
                        <li>Be likely to harass, upset, embarrass alarm or annoy any other person;</li>
                        <li>Be used to impersonate any person, or to misrepresent your identity or affiliation with any person;</li>
                        <li>Give the impression that they emanate from us, if this is not the case;</li>
                        <li>Advocate, promote or assist any unlawful act such (by way of example only) copyright is fringement or computer misuse.</li>
                    </ul>
                    <br>
                    <p>In addition to and above, where you communicate with other users, you must not:</p>
                    <ul>
                        <li>Lie to or mislead other users;</li>
                        <li>Distribute unsolicited or unauthorised advertising or promotional material or any junk mail, spam or chain letters;</li>
                        <li>Use personal information about other users, including but not limited to their names, email addresses and postal addresses, except with their express consent.</li>
                    </ul>
                    <br>
                    <p>
                        You warrant that your use of a feature that allows you to upload content to our Site and/or
                        communicate with other user complies with the Standards, and you will be liable to us and
                        indemnify us for any breach of that warranty. Any content you upload to our Site will be considered
                        non-confidential and non-proprietary. You retain all of your ownership rights in your content, but
                        you grant us a perpetual, worldwide, non-exclusive, royalty-free, transferable licence to use, store
                        and copy that content and to distribute and make it available to third parties. We also have the right
                        to disclose your identity to any third party who is claiming that any content posted or uploaded by
                        you to our Site constitutes a violation of their intellectual property rights, or of their right to
                        privacy. We will not be responsible, or liable to any third party, for the content or accuracy of any
                        content posted by you or any other user of our Site. We have the right to remove any posting you
                        make on our Site if, in our opinion, your post does not comply with the Standards. We are not
                        directly involved in the discussions or transactions between users, and do not provide any
                        assurances that any user will fulfil their obligations to other users. The actions of other users are
                        their own, and we do not support, endorse or join them. The views expressed by other users on our
                        Site do not represent our views or values. You are solely responsible for securing and backing up
                        your content.
                    </p>
                    <h2>Miscellaneous</h2>
                    <p>
                        These terms shall be governed by, and construed in accordance with, the laws of Indonesia and you
                        agree that the Indonesia’s courts shall have exclusive juristiction in any dispute. If any provision of
                        these Website Terms is held to be invalid or unenforceable, that provision shall be limited or
                        elimited to the minimum extent necessary so that these Website Terms shall otherwise remain in
                        full force and effect and enforceable. In order for any waiver of compliance with these Website
                        Terms to be binding, we must provide you with notice of such waiver. The failur of either party to
                        enforce its rights under these Website Terms at any time for any period will not be construed as a
                        waiver of such rights.
                    </p>
                    <h2>Contact</h2>
                    <p>
                        If you have any questions, complaints or claims with respect to the site, you may contact us at
                        website, Kolega Co-Working Space Company, Tebet Dalam Raya street no. 53D, limatiga building,
                        2nd floor, 12810, Tebet, South Jakarta, Jakarta, Indonesia or <a href="mailto:info@kolega.co">info@kolega.co</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection