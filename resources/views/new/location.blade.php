@extends('new/master')

@section('title',$location->name)

@section('additionalCss')
    <style>
        .fa-3x {
            color: orange;
        }
    </style>
@endsection

@section('content')

    <section class="location-single">
        <div id="slide-location">
            <?php $i = 1;?>
            @foreach($location->photos as $photo)
                <div class="item">
                    <a href="{{ URL::asset($photo->url) }}" data-caption="Kolega {{$location->name}} Photo {{$i}}">
                        <img width="100%" src="{{ URL::asset($photo->url) }}" alt="Kolega {{$location->name}} Photo {{$i}}">
                    </a>
                </div>
            <?php $i++; ?>
            @endforeach
        </div>

        <div class="container">
            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-5-5 pure-u-lg-4-5 ">
                    <div class="location-single-header pure-g">
                        @if($location->slug == 'tebet' || $location->slug == 'antasari' || $location->slug == 'senopati')
                            <div class="pure-u-1 pure-u-md-3-5">
                                <div class="description">
                                    <div class="info-title">Kolega</div>
                                    <h1 class="title">{{$location->name}}</h1>
                                    @if(strlen($desc) > 210)
                                        <p id="location-description" data-full="{{$desc}}">{{substr($desc,0,200)."..."}}</p>
                                        <a class="readmore" href="javascript:readMore()">READ MORE</a>
                                    @else
                                        <p>{{$desc}}</p>
                                    @endif
                                </div>
                            </div>
                            <div class="pure-u-1 pure-u-md-2-5">
                                <div class="details">
                                    <div class="address">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>{{$location->address}}</span>
                                    </div>
                                    <div class="phone">
                                        <i class="fa fa-phone" aria-hidden="true"></i>
                                        <span>{{$phone}}</span>
                                    </div>
                                    <div class="time">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <span>
                                            Weekdays {{$monfri}}
                                            <br>
                                            Weekends {{$satsun}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="pure-u-1 pure-u-md-3-5">
                                <div class="description">
                                    <div class="info-title">Kolega</div>
                                    <h1 class="title">{{$location->name}}</h1>
                                    <p id="location-description">
                                        The office location is customizeable and perfect for companies with 50+ seats.
                                        For more information on this location, contact us at <b>info@kolega.co</b>
                                    </p>
                                    <a class="readmore" href="javascript:contactUs()">CONTACT US</a>
                                </div>
                            </div>
                            @if($location->address != 'Coming soon')
                                <div class="pure-u-1 pure-u-md-2-5">
                                    <div class="details">
                                        <div class="address">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            <span>{{$location->address}}</span>
                                        </div>
                                        <div class="phone">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>Coming soon</span>
                                        </div>
                                        <div class="time">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>
                                                Weekdays {{$monfri}}
                                                <br>
                                                Weekends {{$satsun}}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                    </div>
                </div>
            </div>

            <!-- Amenities -->
            <div class="amenities">
                <div class="pure-g">
                    <div class="pure-u-1 pure-u-md-1-5">
                        <h3 class="title">Amenities</h3>
                    </div>
                    <div class="pure-u-1 pure-u-md-4-5">
                        <ul class="amenities-list">
                            @foreach($facilities as $facility)
                                <li>
                                    {!! $facility !!}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <hr>

            <!-- RATE -->
            <div class="pure-g">
                <div class="pure-u-1 pure-u-md-13-24">
                    <div class="rate">
                        <div class="info-title">RATE</div>

                        <div class="rate-wrapper">
                            <h3 class="headline-sub">Membership Plans</h3>
                            <ul class="rate-list">
                                <li class="pure-g">
                                    <span class="pure-u-1 pure-u-sm-10-24">Daily Access</span>
                                    <span class="pure-u-1 pure-u-sm-10-24">{{$dailyAccessPrice}} / day</span>
                                    <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                    <div class="details">
                                        <p>Drop by our location in {{$location->name}} and feel the experience working with high mobility in our coworking area.</p>
                                        <div class="info-title">FACILITIES</div>
                                        <ul>
                                            <li>High Speed Internet Connection</li>
                                            <li>Free Flow Mineral Water (Self-service)</li>
                                            <li>The chance to grow yourself through networking</li>
                                        </ul>

                                        <div class="info-title">OTHER LOCATION</div>
                                        <ul>
                                            @foreach($otherLocations as $otherLoc)
                                                <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                <li class="pure-g">
                                    <span class="pure-u-1 pure-u-sm-10-24">Coworking Area</span>
                                    <span class="pure-u-1 pure-u-sm-10-24">{{$coworkingAreaPrice}} / month</span>
                                    <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                    <div class="details">
                                        <p>Join Kolega Coworking Space as a member of the growing community. Access to the coworking area in your preferred location with no assigned desk. Get a fresh new desk every day.</p>
                                        <div class="info-title">FACILITIES</div>
                                        <ul>
                                            <li>Full-Month coworking space access</li>
                                            <li>12 hours a day access to preferred location</li>
                                            <li>Special price to access any other locations</li>
                                            <li>Fast Internet connection (up to 100mbps)</li>
                                            <li>Personal locker storage</li>
                                            <li>100 sheets of free printing</li>
                                            <li>Exclusive access to events by Koléga</li>
                                            <li>Free mineral water</li>
                                        </ul>

                                        <div class="info-title">OTHER LOCATION</div>
                                        <ul>
                                            @foreach($otherLocations as $otherLoc)
                                                <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </li>
                                @if($location->slug != 'tebet' && $location->slug != 'antasari')
                                    <li class="pure-g">
                                        <span class="pure-u-1 pure-u-sm-10-24">Dedicated Desks</span>
                                        <span class="pure-u-1 pure-u-sm-10-24">{{$dedicatedDeskPrice}} / month per desk</span>
                                        <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                        <div class="details">
                                            <p>Access to the coworking area and dedicated area with one assigned desk for the period of this membership. Get together with your colleagues and fellow coworkers, while having a personalized desk to organize your business.</p>
                                            <div class="info-title">FACILITIES</div>
                                            <ul>
                                                <li>12 hours a day access to preferred location</li>
                                                <li>Special price to access any other locations</li>
                                                <li>Customizable desk layout</li>
                                                <li>Additional meeting room slot</li>
                                                <li>Fast Internet connection (up to 100mbps)</li>
                                                <li>Personal locker storage</li>
                                                <li>100 sheets of free printing</li>
                                                <li>Exclusive access to events by Koléga</li>
                                                <li>Free mineral water</li>
                                            </ul>

                                            <div class="info-title">OTHER LOCATION</div>
                                            <ul>
                                                @foreach($otherLocations as $otherLoc)
                                                    @if($otherLoc->slug != 'tebet' && $otherLoc->slug != 'antasari')
                                                        <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @endif
                                @if($location->slug != 'tebet')
                                    <li class="pure-g">
                                        <span class="pure-u-1 pure-u-sm-10-24">Office Suites</span>
                                        <span class="pure-u-1 pure-u-sm-10-24">{{$officeSuitesPrice}}</span>
                                        <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                        <div class="details">
                                            <p>{{$officeSuitesDesc}}</p>
                                            <div class="info-title">FACILITIES</div>
                                            <ul>
                                                <li>12 hours a day access to preferred location</li>
                                                <li>Special price to access any other locations</li>
                                                <li>Fully Customizable private office</li>
                                                <li>Additional meeting room slot</li>
                                                <li>Fast Internet connection (up to 100mbps)</li>
                                                <li>Personal locker storage</li>
                                                <li>100 sheets of free printing</li>
                                                <li>Exclusive access to events by Koléga</li>
                                                <li>Free mineral water</li>
                                            </ul>

                                            <div class="info-title">OTHER LOCATION</div>
                                            <ul>
                                                @foreach($otherLocations as $otherLoc)
                                                    @if($otherLoc->slug != 'tebet')
                                                        <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>

                        @if($location->address != 'Coming soon')
                            @if(!empty($meetingRooms))
                                <div class="rate-wrapper">
                                    <h3 class="headline-sub">Meeting Room</h3>
                                    <ul class="rate-list">
                                        @foreach($meetingRooms as $meetingRoom)
                                            <li class="pure-g">
                                                <span class="pure-u-1 pure-u-sm-10-24">{{$meetingRoom->size}}</span>
                                                <span class="pure-u-1 pure-u-sm-10-24">{{$meetingRoom->price}}</span>
                                                <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                                <div class="details">
                                                    <p>{{$meetingRoom->desc}}</p>
                                                    <div class="info-title">FACILITIES</div>
                                                    <ul>
                                                        @foreach($meetingRoom->facilities as $facility)
                                                            <li>{{$facility}}</li>
                                                        @endforeach
                                                    </ul>

                                                    <div class="info-title">OTHER LOCATION</div>
                                                    <ul>
                                                        @foreach($otherLocations as $otherLoc)
                                                            <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(!empty($eventSpaces))
                                <div class="rate-wrapper">
                                    <h3 class="headline-sub">Event Space</h3>
                                    <ul class="rate-list">
                                        @foreach($eventSpaces as $eventSpace)
                                            <li class="pure-g">
                                                <span class="pure-u-1 pure-u-sm-10-24">{{$eventSpace->name}}</span>
                                                <span class="pure-u-1 pure-u-sm-10-24">{{$eventSpace->price}}</span>
                                                <span class="pure-u-1 pure-u-sm-4-24 btn-details">Details</span>
                                                <div class="details">
                                                    <p>{{$eventSpace->desc}}</p>
                                                    <div class="info-title">FACILITIES</div>
                                                    <ul>
                                                        @foreach($eventSpace->facilities as $facility)
                                                            <li>{{$facility}}</li>
                                                        @endforeach
                                                    </ul>

                                                    <div class="info-title">OTHER LOCATION</div>
                                                    <ul>
                                                        @foreach($otherLocations as $otherLoc)
                                                            <li><a href="/location/{{$otherLoc->slug}}">{{$otherLoc->name}}</a></li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                        @endif

                    </div>
                </div>
                <div class="pure-u-1 pure-u-md-11-24">

                    <sidebar class="sidebar">
                        <div class="sidebar-banner"><a href="/plans"><img src="{{asset('img/new/banner-sidebar.jpg')}}"></a></div>
                        @if($location->address != 'Coming soon')
                            <div class="sidebar-map">
                                <div id="cd-google-map">
                                    <div id="google-container"></div>
                                    <div id="cd-zoom-in"></div>
                                    <div id="cd-zoom-out"></div>
                                </div>
                            </div>
                        @endif
                    </sidebar>
                </div>
            </div>

        </div>

    </section>

@endsection

@section('additionalJs')

<script src="{{asset('js/new/slick-lightbox.min.js')}}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6ZlU2KnNx71GM10gWutqG4CpzPBbv4zE"></script>

<script type="text/javascript">
    $('#slide-location').slick({
        dots: true,
        arrows: false
    });
    $('#slide-location').slickLightbox({
        caption: 'caption'
    });

    $( ".rate-list .btn-details" ).click(function() {
        $(this).next(".details").slideToggle( "fast");
        $(this).toggleClass("open")
    });
    
    function readMore() {
        $('#location-description').html($('#location-description').attr('data-full'));
        $('.readmore').hide();
    }

    function contactUs(){
        $("html, body").animate({ scrollTop: $(document).height() }, 1000);
    }

    jQuery(document).ready(function($){
        //set your google maps parameters,
        var latitude = {{$location -> latitude}},
            longitude = {{$location -> longitude}},
            map_zoom = 14;

        //google map custom marker icon - .png fallback for IE11
        var is_internetExplorer11= navigator.userAgent.toLowerCase().indexOf('trident') > -1;
        var marker_url = ( is_internetExplorer11 ) ? "{{asset('img/new/cd-icon-location.png')}}" : "{{asset('img/new/cd-icon-location.png')}}";

        //define the basic color of your map, plus a value for saturation and brightness
        var main_color = '#FFAB40',
            saturation_value= 0,
            brightness_value= 5;

        //we define here the style of the map
        var style= [
            {
                //set saturation for the labels on the map
                elementType: "labels",
                stylers: [
                    {saturation: saturation_value}
                ]
            },
            {   //poi stands for point of interest - don't show these lables on the map
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    {visibility: "off"}
                ]
            },
            {
                //don't show highways lables on the map
                featureType: 'road.highway',
                elementType: 'labels',
                stylers: [
                    {visibility: "off"}
                ]
            },
            {
                //don't show local road lables on the map
                featureType: "road.local",
                elementType: "labels.icon",
                stylers: [
                    {visibility: "off"}
                ]
            },
            {
                //don't show arterial road lables on the map
                featureType: "road.arterial",
                elementType: "labels.icon",
                stylers: [
                    {visibility: "off"}
                ]
            },
            {
                //don't show road lables on the map
                featureType: "road",
                elementType: "geometry.stroke",
                stylers: [
                    {visibility: "off"}
                ]
            },
            //style different elements on the map
            {
                featureType: "transit",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "poi",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "poi.government",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "poi.sport_complex",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "poi.attraction",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "poi.business",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "transit",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "transit.station",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "landscape",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]

            },
            {
                featureType: "road",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "road.highway",
                elementType: "geometry.fill",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            },
            {
                featureType: "water",
                elementType: "geometry",
                stylers: [
                    { hue: main_color },
                    { visibility: "on" },
                    { lightness: brightness_value },
                    { saturation: saturation_value }
                ]
            }
        ];

        //set google map options
        var map_options = {
            center: new google.maps.LatLng(latitude, longitude),
            zoom: map_zoom,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false,
            styles: style
        };
        //inizialize the map
        var map = new google.maps.Map(document.getElementById('google-container'), map_options);
        //add a custom marker to the map
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(latitude, longitude),
            map: map,
            visible: true,
            icon: marker_url
        });

        //add custom buttons for the zoom-in/zoom-out on the map
        function CustomZoomControl(controlDiv, map) {
            //grap the zoom elements from the DOM and insert them in the map
            var controlUIzoomIn= document.getElementById('cd-zoom-in'),
                controlUIzoomOut= document.getElementById('cd-zoom-out');
            controlDiv.appendChild(controlUIzoomIn);
            controlDiv.appendChild(controlUIzoomOut);

            // Setup the click event listeners and zoom-in or out according to the clicked element
            google.maps.event.addDomListener(controlUIzoomIn, 'click', function() {
                map.setZoom(map.getZoom()+1)
            });
            google.maps.event.addDomListener(controlUIzoomOut, 'click', function() {
                map.setZoom(map.getZoom()-1)
            });
        }

        var zoomControlDiv = document.createElement('div');
        var zoomControl = new CustomZoomControl(zoomControlDiv, map);

        //insert the zoom div on the top left of the map
        map.controls[google.maps.ControlPosition.LEFT_TOP].push(zoomControlDiv);
    });
</script>
@stop