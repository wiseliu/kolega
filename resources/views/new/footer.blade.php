<footer id="footer" class="dark">
    <div id="copyrights">
        <div class="container clearfix">
            <div class="col-md-9">
                <p>© Koléga 2017.</p>
                <p>SHARE THE SPACE, MEET NEW PEOPLE, MAKE NEW FRIENDS, GROW EVEN BIGGER.</p>
                <div class="socmed-footer" style="margin-top: 0;text-align: left;">
                    <a href="https://www.instagram.com/kolegaco/"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                    <a href="https://www.facebook.com/kolegaco/"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                    <a href="https://plus.google.com/u/1/105455213192384785868"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a>
                </div>
                <a href="https://play.google.com/store/apps/details?id=co.kolega.android" target="_blank">
                    <img style="max-width: 180px; margin-top: 10px; margin-bottom: 15px" src="{{asset("/img/googleplay.png")}}" alt="Get in on Google Play">
                </a>
            </div>
            <div class="col-md-3">
                <a href="/location" class="pull-right" style="color: white;">Kolega Locations</a><br>
                <a href="/blog" class="pull-right" style="color: white;">Blog</a><br>
                <a href="/faq" class="pull-right" style="color: white;">FAQ</a><br>
                <a href="/contact" class="pull-right" style="color: white;">Contact Us</a><br>
                <a href="/privacy-policy" class="pull-right" style="color: white;">Privacy Policy</a><br>
                <a href="/terms" class="pull-right" style="color: white;">Terms</a><br>
                <a href="/career" class="pull-right" style="color: white;">Career</a>
            </div>

        </div>
    </div>
</footer>