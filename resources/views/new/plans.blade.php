@extends('new/master')

@section('title',"Plans")

@section('content')
    <section class="location">

        <div class="container">
            <div class="l-box">
                <div class="page-header">
                    <div class="page-header-name">PLANS</div>
                    <h1 class="page-header-title">Better plans, <br>better work.</h1>
                    <p>Koléga is more than best place for work, connect, collaborate, and create impacts through innovations.</p>
                </div>
            </div>
        </div>

        <!-- PLANS PACKAGE -->
        <div class="planspackage">
            <div class="container">
                <div class="l-box-xl">
                    <div class="jscroll">
                        <div class="container-scroll">
                            <div class="pure-g">
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan1.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Koléga Everywhere</h3>
                                                <small>A Whole New Level of Co-Working Space</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content orange parent">
                                            <p>Working in the city means that you need a lot of options for places to work. With this plan, you could access Kolega locations anywhere, anytime! Make your day more efficient by working in the nearest Kolega from your place. Or meet with clients.</p>
                                            <div class="info-title">PRIVILEGE</div>
                                            <ul>
                                                <li>Become an Exclusive Member of Kolegas' Community</li>
                                                <li>Get A Cool Membership Card*</li>
                                                <li>Have Access To Partner Benefits*</li>
                                                <li>Work Anytime, Anywhere, Your Call</li>
                                                <li>Access To All Events Held By Kolega</li>
                                                <li>Get special prices forever<br><i>Terms & Conditions Applied</i></li>
                                            </ul>
                                            <div class="join-now"><a href="mailto:info@kolega.co" class="info-title">JOIN NOW</a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan2.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Coworking Area</h3>
                                                <small>Starts from Rp. 1.300.000,- per month - Full Access</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content parent">
                                            <p>Join Kolega Coworking Space as a member of the growing community. Access to the coworking area in your preferred location with no assigned desk. Get a fresh new desk every day.</p>
                                            <div class="info-title">BEST FOR</div>
                                            <ul>
                                                <li>Remote and part-time workers</li>
                                                <li>Client meetings</li>
                                                <li>More than a week per month</li>
                                            </ul>
                                            <div class="list-location">
                                                <div class="info-title center">AVAILABLE LOCATION</div>
                                                <div class="slide-location">
                                                    @foreach($locations as $locationArray)
                                                        <ul>
                                                            @foreach($locationArray as $location)
                                                                <li><a href="/location/{{$location['slug']}}">{{$location['name']}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- <div class="more-location down"><i class="fa fa-angle-down" aria-hidden="true"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan3.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Dedicated Desks</h3>
                                                <small>Starts from Rp. 3.000.000,- per month - Full Access</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content parent">
                                            <p>Access to the coworking area and dedicated area with one assigned desk for the period of this membership. Get together with your colleagues and fellow coworkers, while having a personalized desk to organize your business.</p>
                                            <div class="info-title">BEST FOR</div>
                                            <ul>
                                                <li>Startups and small agencies</li>
                                                <li>Collaboration and growth</li>
                                                <li>Everyday use</li>
                                            </ul>

                                            <div class="list-location">
                                                <div class="info-title center">AVAILABLE LOCATION</div>
                                                <div class="slide-location">
                                                    @foreach($locations as $locationArray)
                                                        <ul>
                                                            @foreach($locationArray as $location)
                                                                @if($location['slug'] != 'tebet' && $location['slug'] != 'antasari')
                                                                    <li><a href="/location/{{$location['slug']}}">{{$location['name']}}</a></li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- <div class="more-location down"><i class="fa fa-angle-down" aria-hidden="true"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan4.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Office Suite</h3>
                                                <small>Starts from Rp 8.000.000,- / unit</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content parent">
                                            <p>Access to the selected, tailor-made, private office suite(s) for you and your team, while being able to access the coworking area to huddle-up with the community and colleagues.</p>
                                            <div class="info-title">BEST FOR</div>
                                            <ul>
                                                <li>Companies of 1 - 10+</li>
                                                <li>Satellite and established teams</li>
                                                <li>Extra autonomy and security</li>
                                            </ul>
                                            <div class="list-location">
                                                <div class="info-title center">AVAILABLE LOCATION</div>
                                                <div class="slide-location">
                                                    @foreach($locations as $locationArray)
                                                        <ul>
                                                            @foreach($locationArray as $location)
                                                                @if($location['slug'] != 'tebet')
                                                                    <li><a href="/location/{{$location['slug']}}">{{$location['name']}}</a></li>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- <div class="more-location down"><i class="fa fa-angle-down" aria-hidden="true"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan5.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Meeting Rooms</h3>
                                                <small>Starts from Rp 150.000 / 2 hours</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content parent">
                                            <p>Need a comfy place to do your meetings? We have meeting rooms that could fit 4 to 30 persons each room.</p>
                                            <div class="info-title">INCLUDING</div>
                                            <ul>
                                                <li>Internet Connection</li>
                                                <li>Self Service Mineral Water/Coffee/Tea</li>
                                                <li>White Board</li>
                                            </ul>
                                            <div class="list-location">
                                                <div class="info-title center">AVAILABLE LOCATION</div>
                                                <div class="slide-location">
                                                    @foreach($locations as $locationArray)
                                                        <ul>
                                                            @foreach($locationArray as $location)
                                                                <li><a href="/location/{{$location['slug']}}">{{$location['name']}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- <div class="more-location down"><i class="fa fa-angle-down" aria-hidden="true"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="pure-u-1 pure-u-sm-1-2 pure-u-lg-1-3">
                                    <div class="item">
                                        <div class="thumb">
                                            <img src="{{asset('img/new/plan6.jpg')}}">
                                            <div class="caption">
                                                <h3 class="title">Event Spaces</h3>
                                                <small>Plenty spaces for everyone! Starts from Rp 1.500.000 / 2 hours</small>
                                            </div>
                                        </div>
                                        <div class="l-box-lrg content parent">
                                            <p>For your event needs, we have modular spaces that could accommodate up to 70 person capacity events. In the spaces, you will find inspiration and experience.</p>
                                            <div class="info-title">INCLUDING</div>
                                            <ul>
                                                <li>Internet Connection</li>
                                                <li>Self Service Mineral Water/Coffee/Tea</li>
                                                <li>White Board</li>
                                            </ul>

                                            <div class="list-location">
                                                <div class="info-title center">AVAILABLE LOCATION</div>
                                                <div class="slide-location">
                                                    @foreach($locations as $locationArray)
                                                        <ul>
                                                            @foreach($locationArray as $location)
                                                                <li><a href="/location/{{$location['slug']}}">{{$location['name']}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    @endforeach
                                                </div>
                                                <!-- <div class="more-location down"><i class="fa fa-angle-down" aria-hidden="true"></i></div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="loadmore"><a href="#">Load more</a></div>--}}
                    </div>
                </div>
            </div>
        </div>

        <!-- //END BOOK A PLAN -->

    </section>
@endsection

@section('additionalJs')
    <script src="{{asset('js/new/slick-lightbox.min.js')}}"></script>
    <script src="{{asset('js/new/jquery.matchHeight-min.js')}}"></script>
    <script src="{{asset('js/new/jquery.jscroll.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            $('.slide-location').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
            });
            $('.content').matchHeight();


            $('.jscroll').jscroll({
                contentSelector: '.container-scroll',
                autoTrigger: false,
                callback: function(){
                    // $('.slide-location').slick('unslick')
                    // $('.slide-location').slick('reInit');
                    $('.content').matchHeight();
                }
            });

        });
    </script>
@endsection