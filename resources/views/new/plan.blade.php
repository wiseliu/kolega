@extends('new/master')

@section('title',$name)

@section('content')
    @include('new/header')

    <section id="slider" class="slider-parallax swiper_wrapper full-screen clearfix">

        <div class="slider-parallax-inner">

            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <div class="swiper-slide dark" style="background-image: url('{{ asset($img) }}');">
                        <div class="container clearfix">
                            <div class="slider-caption slider-caption-center">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <a href="#" data-scrollto="#content" data-offset="100" class="dark one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>
    </section>

    <section id="content">
        <div class="wrap">
            <div class="container clearfix" style="padding-bottom: 50px;">
                <a href="/location">
                    <div class="row section-form-singleloc">
                        <div class="col-xs-12 hidden-md hidden-sm hidden-lg">
                            <div class="col-md-6">
                                <img src="{{asset('/images/Logo-kolega-black.png')}}">
                            </div>
                            <div class="col-md-6">
                                <h4 style="margin: 0px">PICK<br>LOCATION</h4>
                            </div>
                        </div>

                        <div id="sidebar-kolega" class="col-md-3 hidden-xs hidden-sm">
                            <div class="col-md-6">
                                <img src="{{asset('/images/Logo-kolega-black.png')}}">
                            </div>
                            <div class="col-md-6">
                                <h4 style="margin: 0px">PICK<br>LOCATION</h4>
                            </div>
                        </div>
                    </div>
                </a>

                <div class="row" style="padding-top: 125px;">
                    <div class="col-md-9 title-single-loc1 nopadding">
                        <h2 style="margin: 10px 0px; text-align: center;">{{$name}}</h2>
                    </div>
                    <div class="col-md-3 title-single-loc3 nopadding" style="height: 67px;">
                        <p style="margin: 0px; text-align: center;">Start from IDR 1,3mio/mo</p>
                    </div>
                </div>

                <div class="row" style="padding-top: 25px;">
                    <div class="col-md-12">
                        <p>{{$desc}}</p>
                    </div>
                </div>

                <div class="row" style="padding-top: 25px;">
                    <h2>WHAT YOU GET</h2>
                    <hr>
                </div>

                <div class="row" style="padding-top: 25px;">

                    <div class="col-md-4 text-left descp-plans" style="height: 310px">
                        <h4 style="color: white">Access</h4>
                        <p style="color: white">{!! $access !!}</p>
                    </div>

                    <div class="col-md-4 text-left descp-plans" style="height: 310px">
                        <h4 style="color: white">Included Amenities</h4>
                        <p style="color: white">{!! $amenities !!}</p>
                    </div>

                    <div class="col-md-4 text-left descp-plans" style="height: 310px">
                        <h4 style="color: white">Benefits</h4>
                        <p style="color: white">{!! $benefits !!}</p>
                    </div>
                </div>

                <div class="row" style="padding-top: 25px;">
                    <h2>BERANGKAT DARI TEMAN</h2>
                    <hr>
                    <P>
                        Be a part of the community where friendship makes your day merrier.
                        With Kolega's diverse community, we are giving you the best way to grow
                        your business with a familiar atmosphere of gotong royong.
                    </P>
                </div>

                <div class="row" style="padding-top: 25px;">
                    <h2 class="text-center">SEE OTHER PLANS</h2>
                    <hr>
                </div>


            </div>

            <div class="container-fluid nopadding">

                <div class="row" style="padding-top: 25px;">

                    <a href="/plan/{{$other_slug1}}">
                        <div class="col-md-6 nopadding">
                            <img src="{{asset($other_img1)}}" style="width:100%">
                            <div class="col-md-12 col-xs-12 button-floating-singleloc">
                                <button style="width: 40%;" class="button button-3d nomargin button-form-home-custom" value="submit">{{$other_name1}}</button>
                            </div>
                        </div>
                    </a>

                    <a href="/plan/{{$other_slug2}}">
                        <div class="col-md-6 nopadding">
                            <img src="{{asset($other_img2)}}" style="width:100%">
                            <div class="col-md-12 col-xs-12 button-floating-singleloc">
                                <button style="width: 40%;" class="button button-3d nomargin button-form-home-custom" value="submit">{{$other_name2}}</button>
                            </div>
                        </div>
                    </a>
                </div>

            </div>

        </div>

    </section><!-- #content end -->

    @include('new/footer')
@stop

@section('additionalJs')

    <script type="text/javascript">
        $(function() {
            var offset = $("#sidebar-kolega").offset();
            var topPadding = 550;
            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top) {
                    $("#sidebar-kolega").stop().animate({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                } else {
                    $("#sidebar-kolega").stop().animate({
                        marginTop: 0
                    });
                };
            });
        });
    </script>
@stop