@extends('new/master')

@section('title',"Jakarta Coworking Space")

@section('content')
    <section class="home">

        <!-- BERANGKAT -->
        <div class="berangkat">
            <div class="container">
                <div class="photo photo-first"><img src="{{ URL::asset('img/new/foto-one.jpg')}}"></div>
                <div class="l-box">
                    <h1 class="title">Berangkat <br>dari Teman</h1>
                    <a class="pure-button" href="/plans">JOIN US</a>
                </div>
            </div>
        </div>
        <!-- //END BERANGKAT -->

        <div class="bg">
            <div class="pure-g">
                <div class="left pure-u-1 pure-u-md-1-2"></div>
                <div class="right pure-u-1 pure-u-md-1-2"></div>
            </div>
        </div>

        <!-- LET'S WORK -->
        <div class="letswork">
            <div class="container">
                <div class="pure-g">
                    <div class="left pure-u-1 pure-u-md-1-2">
                        <div class="photo photo-second">
                            <img src="{{ URL::asset('img/new/foto-two.jpg')}}">
                        </div>
                    </div>
                    <div class="right pure-u-1 pure-u-md-1-2">
                        <div class="l-box">
                            <h3 class="title">Let’s work <br>together!</h3>
                            <p>Work amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works.</p>
                            <a class="pure-button" href="/plans">PLANS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //END LET'S WORK -->

        <!-- PLAYGROUND -->
        <div class="playground">
            <div class="container">
                <div class="pure-g">
                    <div class="left pure-u-1 pure-u-md-1-2">
                        <div class="l-box">
                            <h3 class="title">Not just <br>a space, <br>it’s a <br>playground.</h3>
                            <a class="pure-button" href="/community">COMMUNITY</a>
                        </div>
                    </div>
                    <div class="right pure-u-1 pure-u-md-1-2">
                        <div class="photo photo-third">
                            {{--<div class="video-responsive">--}}
                                {{--<iframe width="420" height="315" src="http://www.youtube.com/embed/a8ZpAf_tNh0" frameborder="0" allowfullscreen></iframe>--}}
                            {{--</div>--}}
                            <img src="{{ URL::asset('img/new/foto-three.jpg')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //PLAYGROUND -->

        <!-- ABOUT US -->
        <div class="aboutus" id="aboutus">
            <div class="container">
                <div class="l-box">
                    <h3>Our Communities are based on <strong>friendship</strong> in which you are able to grow as a person. Here, in Koléga friendship is embodied in working alongside your partners and those who shares the same space with you.</h3>
                    <a class="pure-button" href="/#aboutus">ABOUT US</a>
                </div>
                <hr>
            </div>
        </div>
        <!-- //END ABOUT US -->


        <!-- BOOK A PLAN -->
        <div class="bookplan">
            <div class="container">
                <div class="l-box homepage">
                    <h3 class="title">Book a plan? See our <a href="/location">location.</a></h3>
                    <div class="slide-bookplan">
                        @foreach($locations as $location)
                            <div class="item" style="width:344px; overflow: hidden">
                                <img class="place" src="{{$location->photos->first()->url}}" style="width: auto">
                                <a class="location" href="/location/{{$location->slug}}">
                                    {{--<div class="name">{{$location->name}} {!! ($location->slug != 'antasari' && $location->slug != 'tebet' && $location->slug != 'senopati' ? '<br>(Coming Soon)' : '')!!}</div>--}}
                                    <div class="name">{{$location->name}}<br><br></div>
                                    <div class="address">
                                        <i class="fa fa-map-marker" aria-hidden="true"></i>
                                        <span>
                                            {{$location->address}}
                                        </span>
                                    </div>
                                    @if($location->address != 'Coming soon')
                                        <div class="phone">
                                            <i class="fa fa-phone" aria-hidden="true"></i>
                                            <span>{{$location->phone_number}}</span>
                                        </div>
                                        <div class="time">
                                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>
                                                @if($location->slug == 'tebet')
                                                    Weekdays 10AM-9PM
                                                    <br>
                                                    Weekends 10AM-5PM
                                                @else
                                                    Weekdays 9AM-9PM
                                                    <br>
                                                    Weekends 9AM-5PM
                                                @endif
                                            </span>
                                        </div>
                                    @endif

                                    <span class="btn"><img src="{{ URL::asset('img/new/ic-arrow-location.svg')}}"></span>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <!-- //END BOOK A PLAN -->

    </section>

@endsection

@section('additionalJs')
    <script type="text/javascript">
        $(document).ready(function(){

            $(".berangkat").scroll(function () {

                var s = $(".berangkat").scrollTop();
                console.log(s)
                $(".photo-first img").css("-webkit-transform","translateY(" +  (s/s)  + "px)");
            })

            // $(".slide-bookplan .item").each(function(){
            //     $(this).on("click", function(){
            //         $(this).find("a").trigger( "click" );
            //     });
            // });


            $('.slide-bookplan').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: false,
                autoplaySpeed: 2000,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    },
                    {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        });
    </script>
@endsection