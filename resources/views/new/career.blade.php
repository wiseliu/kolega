@extends('new/master')

@section('title',"Careers")

@section('additionalCss')
<style>
    h1, h2, h3, h4, h5, h6 {
        color: #333;
        font-weight: 600;
        line-height: 1.5;
        margin: 0 0 10px 0;
        font-family: 'Raleway', sans-serif;
    }
    p{
        text-align: justify;
    }

    li{
        text-align: justify;
    }
</style>
@endsection

@section('content')

<section id="slider" class="boxed-slider">
</section>

<section id="content">
    <div class="wrap">
        <div class="container clearfix" style="padding-bottom: 50px; padding-top: 50px;; width: 55%">
            <div class="row" style="background-color: orange; padding: 5px">
                <div class="col-md-3" style="padding: 0px" >
                    <img src="{{asset('/img/new/about-side.jpg')}}" style="width:100%">
                </div>
                <div class="col-md-9">
                    <p style="color: white; font-size: 21px; text-align: right">
                        If you're <b>passionate</b>, adaptable to a fast-paced environment and want to <b>gain lots of experience</b>, then this path are for YOU to <b>conquer</b>.
                    </p>
                </div>
            </div>
            <div class="row" style="margin-top: 20px">
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#DMA" style="color: black">
                                <h4 class="panel-title">
                                    DIGITAL MARKETING ASSOCIATES
                                </h4>
                            </a>
                        </div>
                        <div id="DMA" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Strategic repositioning of Kolega’s social media content and branding.
                                    </li>
                                    <li>
                                        Oversee digital marketing performance, working closely with community managers to implement space based campaigns.
                                    </li>
                                    <li>
                                        Work across division to develop strategies to drive new member acquisition across a variety of channels.
                                    </li>
                                    <li>
                                        Manage the development of creative assets for sales collateral, website, email marketing, and more.
                                    </li>
                                    <li>
                                        Develop and share marketing processes and best practices with Community Management Team.
                                    </li>
                                    <li>
                                        Analyze performance metrics and develop comprehensive reporting on marketing growth and sales.
                                    </li>
                                    <li>
                                        Develop content calendars on a weekly and monthly basis
                                    </li>
                                    <li>
                                        Create engaging social media content and engage digitally with customer trough Kolega digital presents
                                    </li>
                                    <li>
                                        Consistent updates of social media accounts and website and the general distribution of media alerts
                                    </li>
                                    <li>
                                        Work with staff on new ideas, directions for marketing and communications
                                    </li>
                                    <li>
                                        Manage Kolega weekly newsletter with contents and updates
                                    </li>
                                </ul> 
                                <ul style="padding: inherit">
                                    <b>
                                        Req.
                                    </b>
                                    <li>
                                        Minimum 2 years experience in a performance marketing role ideally at a recognized agency, consumer brand or growth stage startup.
                                    </li>
                                    <li>
                                        Preferably a major in Marketing, Communications or related field.
                                    </li>
                                    <li>    
                                        Fluent in English
                                    </li>
                                    <li>
                                        Excellent oral and written skills
                                    </li>
                                    <li>
                                        Must be entrepreneurial in spirit and highly self-motivated
                                    </li>
                                    <li>
                                        Familiarity with web analytics (Google Analytics, Omniture), CRMs and/or other marketing technology platforms (Mailchimp, Exact Target) is a plus.
                                    </li>
                                    <li>
                                        Hands-on experience in management of SEM, SEO, paid social, display, retargeting and mobile advertising platforms
                                    </li>
                                    <li>
                                        Confident Individual with a proactive attitude
                                    </li>
                                    <li>
                                        Proven team player and able to work in fast paced environment
                                    </li>
                                    <li>
                                        Exceptional interpersonal skills , demonstrating professionalism in all dealings
                                    </li>
                                    <li>
                                        Need to be detail oriented and focused on execution of tasks
                                    </li>
                                    <li>
                                        Flexible in work functions
                                    </li>
                                    <li>
                                        Ability to manage multiple tasks
                                    </li>
                                    <li>
                                        Proficient in managing large amounts of data
                                    </li>
                                    <li>
                                        Exceptional time management skills
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#SP" style="color: black">
                                <h4 class="panel-title">
                                    SALES PERSON
                                </h4>
                            </a>
                        </div>
                        <div id="SP" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        To plan and carry out all sales activities on assigned accounts or areas
                                    </li>
                                    <li>
                                        Responsible for ensuring customer satisfaction and managing quality of product and service delivery.
                                    </li>
                                    <li>
                                        Generate and qualify leads
                                    </li>
                                    <li>
                                        Prepare sales action plans and strategies. Develop and maintain a customer database
                                    </li>
                                    <li>
                                        Develop and maintain sales and promotional materials
                                    </li>
                                    <li>
                                        Plan and conduct direct marketing activities
                                    </li>
                                    <li>
                                        Develop and make presentations of company products and services to current and potential clients
                                    </li>
                                    <li>
                                        Negotiate with clients
                                    </li>
                                    <li>
                                        Develop sales proposals
                                    </li>
                                    <li>
                                        Prepare and present sales contracts
                                    </li>
                                    <li>
                                        Conduct product training
                                    </li>
                                    <li>
                                        Maintain sales activity records and prepare sales reports
                                    </li>
                                    <li>
                                        Respond to sales inquiries and concerns by phone, electronically or in person
                                    </li>
                                    <li>
                                        Ensure customer service satisfaction and good client relationships
                                    </li>
                                    <li>
                                        follow up on sales activity
                                    </li>
                                    <li>
                                        Perform quality checks on product and service delivery
                                    </li>
                                    <li>
                                        Monitor and report on sales activities and follow up for management
                                    </li>
                                    <li>
                                        Carry out market research and surveys
                                    </li>
                                    <li>
                                        Participate in sales events
                                    </li>
                                    <li>
                                        Monitor competitors, market conditions and product development
                                    </li>
                                </ul>
                                <ul style="padding: inherit">
                                    <b>
                                        Req.
                                    </b>
                                    <li>
                                        Maximum 28 years old, Diploma in any field.
                                    </li>
                                    <li>
                                        Minimum 1 year experience of working experience in the related field is required for this position.
                                    </li>
                                    <li>
                                        Having knowledge of relevant computer applications, principles and practices sales, customer service principles, and basic business principles.
                                    </li>
                                    <li>
                                        Require skill(s) : planning and strategizing, persuasiveness, adaptability, verbal and written communication, and negotiation skills.
                                    </li>
                                    <li>
                                        Stress tolerance and goal driven.
                                    </li>
                                    <li>
                                        Experience in making presentations.
                                    </li>
                                    <li>
                                        Proven ability to achieve sales targets.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#CMI" style="color: black">
                                <h4 class="panel-title">
                                    COMMUNITY MANAGER INTERN
                                </h4>
                            </a>
                        </div>
                        <div id="CMI" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Managing and monitoring of our social media channels.
                                    </li>
                                    <li>
                                        Assist with development and execution of our monthly broadcaster update.
                                    </li>
                                    <li>
                                        Blog content creation.
                                    </li>
                                    <li>
                                        Assist with marketing campaigns ranging from product launches to bespoke retention emails.
                                    </li>
                                    <li>
                                        Manage all user support enquiries, queries, questions and feedback.
                                    </li>
                                    <li>
                                        Identify frequently asked questions within the community and creating relevant articles, tutorials, videos.
                                    </li>
                                    <li>
                                        Assist with monitoring and reporting on key performance indicators.
                                    </li>
                                </ul>
                                <ul style="padding: inherit">
                                    <b>
                                        Req.
                                    </b>
                                    <li>
                                        University students/fresh graduate majoring in Management or any field.
                                    </li>
                                    <li>
                                        Excellent written and verbal communication skills.
                                    </li>
                                    <li>
                                        Motivated with the ability to work independently and effectively manage workload.
                                    </li>
                                    <li>
                                        A strong understanding of coworking space business.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#ComM" style="color: black">
                                <h4 class="panel-title">
                                    COMMUNITY MANAGER
                                </h4>
                            </a>
                        </div>
                        <div id="ComM" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Working alongside the Digital Marketing Associate to generate new ideas for social content to drive communications.
                                    </li>
                                    <li>
                                        Planning social content and maintaining the content calendar ensuring social media content is regular, relevant and engaging.
                                    </li>
                                    <li>
                                        Engaging with Fans and Followers to build relationships with the community and encourage engagement.
                                    </li>
                                    <li>
                                        Weekly reporting on social media engagements within the community.
                                    </li>
                                    <li>
                                        Monitoring the success of the community and of social media in terms of the overall marketing strategy.
                                    </li>
                                    <li>
                                        Liaising with internal stakeholders to relay customer feedback insights gained from online conversations within the community.
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Working alongside the Digital Marketing Associate to generate new ideas for social content to drive communications.
                                    </li>
                                    <li>
                                        Bachelor’s Degree majoring Marketing, Communication, Arts, and IT.
                                    </li>
                                    <li>
                                        Previous experience working as a community manager in a similar role or alternatively a background in social media.
                                    </li>
                                    <li>
                                        Having the ability to analyze the implications of online communications and messages.
                                    </li>
                                    <li>
                                        Must have a cutting edge interest in social media and be abreast with the fast changing nature of social media and ensuring that new opportunities for engaging with customers are realised.
                                    </li>
                                    <li>
                                        Fluent in English.
                                    </li>
                                    <li>
                                        Excellent written and spoken communication skills.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#FM" style="color: black">
                                <h4 class="panel-title">
                                    FACILITY MANAGER
                                </h4>
                            </a>
                        </div>
                        <div id="FM" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Maintain the buildings and grounds of an organization, directing staff and overseeing the upkeep of equipment and supplies.
                                    </li>
                                    <li>
                                        Make sure the buildings and grounds are maintained, which entails daily and weekly cleaning schedules as well as determining and scheduling repairs, renovation projects, waste reduction improvements and safety inspections.</li>
                                    <li>
                                        In charge of a budget and must negotiate with outside vendors for supplies, repairs and other measures. 
                                    </li>
                                    <li>
                                        Oversee groundskeepers, maintenance workers, and custodial staff.
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Minimum 5 years experience in related field.
                                    </li>
                                    <li>
                                        Bachelor’s Degree in Business Management or similar field.
                                    </li>
                                    <li>
                                        Demonstrate managerial experience and abilities, as well as knowledge of purchasing, supplies, groundskeeping, and equipment repair.
                                    </li>
                                    <li>
                                        Required skill(s) : analytical, communication, detailed, and leadership skills.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#GS" style="color: black">
                                <h4 class="panel-title">
                                    GENERAL SECRETARY
                                </h4>
                            </a>
                        </div>
                        <div id="GS" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Prepare and handle general admin.
                                    </li>
                                    <li>
                                        Prepare and manage correspondence, reports, and documents.
                                    </li>
                                    <li>
                                        Responsible for all secretarial duties and all administration job.
                                    </li>
                                    <li>
                                        Report to management
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Minimum 3 years experience, Diploma in any field.
                                    </li>
                                    <li>
                                        Fast learner, energetic, and patience.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#FO" style="color: black">
                                <h4 class="panel-title">
                                    FINANCE OFFICER
                                </h4>
                            </a>
                        </div>
                        <div id="FO" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Keep accurate records for all daily transactions
                                    </li>
                                    <li>
                                        Prepare balance sheets
                                    </li>
                                    <li>
                                        Process invoices
                                    </li>
                                    <li>
                                        Record accounts payable and accounts receivable
                                    </li>
                                    <li>
                                        Update internal systems with financial data
                                    </li>
                                    <li>
                                        Prepare monthly, quarterly, and annual financial reports
                                    </li>
                                    <li>
                                        Reconcile bank statements
                                    </li>
                                    <li>
                                        Participate in financial audits
                                    </li>
                                    <li>
                                        Track bank deposits and payments
                                    </li>
                                    <li>
                                        Assist with budget preparation
                                    </li>
                                    <li>
                                        Review and implement financial policies
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Minimum 3 years experience, Diploma in any field.
                                    </li>
                                    <li>
                                        Minimal 1 year experience as a Finance Officer or similar role</li>
                                    <li>
                                        Have knowledge of financial regulations and accounting procedures</li>
                                    <li>
                                        Experience using financial software</li>
                                    <li>
                                        Advanced Microsoft Excel skills</li>
                                    <li>
                                        Excellent analytical and numerical skills</li>
                                    <li>
                                        Able to manage confidential data
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#ATO" style="color: black">
                                <h4 class="panel-title">
                                    ACCOUNTING & TAX OFFICER
                                </h4>
                            </a>
                        </div>
                        <div id="ATO" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Maintain and update all daily transactions
                                    </li>
                                    <li>
                                        Manage accounting and tax
                                    </li>
                                    <li>
                                        Prepare and calculate monthly and annual tax report
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Candidate must possess at least Bachelor's Degree in Finance/Accountancy/Banking or equivalent from reputable university
                                    </li>
                                    <li>
                                        At least 1 Year of working experience in the related field is required for this position
                                    </li>
                                    <li>
                                        Familiar with accounting software
                                    </li>
                                    <li>
                                        Computer literate (Ms. Office package)
                                    </li>
                                    <li>
                                        Focus and organized
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#IDint" style="color: black">
                                <h4 class="panel-title">
                                    INTERIOR DESIGN INTERN
                                </h4>
                            </a>
                        </div>
                        <div id="IDint" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Assist creating visual fit-out office
                                    </li>
                                    <li>
                                        Assist creating office drawing
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        University student/fresh graduates majoring Interior Design/Architecture
                                    </li>
                                    <li>
                                        Required skill(s) : AutoCAD, Adobe Photoshop, 3DsMAX, Sketchup
                                    </li>
                                    <li>
                                        Strong personality, detail oriented, independent, disciplined, responsible, and honest
                                    </li>
                                    <li>
                                        Ability to work with a tenacious, energetic, creative, and communicative
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#BED" style="color: black">
                                <h4 class="panel-title">
                                    BACK-END DEVELOPER
                                </h4>
                            </a>
                        </div>
                        <div id="BED" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Participate in the entire application lifecycle, focusing on coding and debugging
                                    </li>
                                    <li>
                                        Write clean code to develop functional web applications
                                    </li>
                                    <li>
                                        Troubleshoot and debug applications
                                    </li>
                                    <li>
                                        Perform UI tests to optimize perfomance
                                    </li>
                                    <li>
                                        Manage cutting-edge technologies to improve legacy applications
                                    </li>
                                    <li>
                                        Collaborate with Front-end developer to integrate user-facing elements with server side logic
                                    </li>
                                    <li>
                                        Gather and address technical and design requirements
                                    </li>
                                    <li>
                                        Build reusable code and libraries for future use
                                    </li>
                                    <li>
                                        Liaise with developers, designers, and system administrators to indentify new features
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Bachelor’s Degree in Computer Science or relevant field
                                    </li>
                                    <li>
                                        Proven work experience as a Back-end developer
                                    </li>
                                    <li>
                                        In-depth understanding of the entire web development process
                                    </li>
                                    <li>
                                        Hands on experience with programming languages like Java, PHP, and Python
                                    </li>
                                    <li>
                                        Working knowledge of CMS framework
                                    </li>
                                    <li>
                                        Familiarity with front-end languages (e.g. HTML, JavaScript, and CSS)
                                    </li>
                                    <li>
                                        Excellent analytical and time management skills
                                    </li>
                                    <li>
                                        Teamwork skills with a problem-solving attitude
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#FED" style="color: black">
                                <h4 class="panel-title">
                                    FRONT-END DEVELOPER
                                </h4>
                            </a>
                        </div>
                        <div id="FED" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Use markup languages like HTML to create user-friendly web pages
                                    </li>
                                    <li>
                                        Maintain and improve website
                                    </li>
                                    <li>
                                        Optimize applications for maximum speed
                                    </li>
                                    <li>
                                        Design mobile-based features
                                    </li>
                                    <li>
                                        Collaborate with Back-end Developers and web designers to improve usability
                                    </li>
                                    <li>
                                        Get feedback from, and build solutions for, users and customers
                                    </li>
                                    <li>
                                        Write functional requirement documents and guides
                                    </li>
                                    <li>
                                        Create quality mockups and prototypes
                                    </li>
                                    <li>
                                        Help back-end developers with coding and troubleshooting
                                    </li>
                                    <li>
                                        Ensure high quality graphic standards and brand consistency
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Bachelor’s Degree in computer science or relevant field
                                    </li>
                                    <li>
                                        Minimum 1 year experience as a Front-end developer
                                    </li>
                                    <li>
                                        Hands on experience with markup languages
                                    </li>
                                    <li>
                                        Experience with JavaScript, CSS, and jQuery
                                    </li>
                                    <li>
                                        Familiarity with browser testing and debugging
                                    </li>
                                    <li>
                                        In-depth understanding of the entire web development process
                                    </li>
                                    <li>
                                        Understanding of layout aesthetics
                                    </li>
                                    <li>
                                        Knowledge of SEO principles
                                    </li>
                                    <li>
                                        Familiarity with software like Adobe Suite, Photoshop, and content management systems
                                    </li>
                                    <li>
                                        An ability to perform well in fast-paced environment
                                    </li>
                                    <li>
                                        Excellent analytical and multitasking skills
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#GDI" style="color: black">
                                <h4 class="panel-title">
                                    GRAPHIC DESIGNER INTERN
                                </h4>
                            </a>
                        </div>
                        <div id="GDI" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Assist cultivate a solid body of work
                                    </li>
                                    <li>
                                        Take the design “brief” to record requirements and clients needs
                                    </li>
                                    <li>
                                        Assist schedule project implementation and define budget constraints
                                    </li>
                                    <li>
                                        Work with a wide range of range and use graphic design software
                                    </li>
                                    <li>
                                        Think creatively and develop new design concepts, graphics, and layouts
                                    </li>
                                    <li>
                                        Prepare rough drafts and present ideas
                                    </li>
                                    <li>
                                        Amend final designs to clients comments and gain full approval
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Proficient in Adobe InDesign, Ilustrator, & Photoshop and/or other common design and layout Applications
                                    </li>
                                    <li>
                                        Having a computer and access to graphic design and layout software is required
                                    </li>
                                    <li>
                                        Proficient command of English grammar and spelling skills
                                    </li>
                                    <li>
                                        Strong communication and organizational skills
                                    </li>
                                    <li>
                                        Ability to work with various departments to finish necessary projects
                                    </li>
                                    <li>
                                        Ability to work independently
                                    </li>
                                    <li>
                                        Self starter, able to think creatively to solve problems
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#AD" style="color: black">
                                <h4 class="panel-title">
                                    ANDROID DEVELOPER
                                </h4>
                            </a>
                        </div>
                        <div id="AD" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Design and build advanced applications for the Android platform
                                    </li>
                                    <li>
                                        Collaborate with cross-functional teams to define, design, and ship new features
                                    </li>
                                    <li>
                                        Work with outside data sources and APIs
                                    </li>
                                    <li>
                                        Unit-test code for robustness, including edge cases, usability, and general reliability
                                    </li>
                                    <li>
                                        Work on bug fixing and improving application performance
                                    </li>
                                    <li>
                                        Continuously discover, evaluate, and implement new technologies to maximize development efficiency
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Bachelor’s Degree in Computer Science, Engineering, or other related subject
                                    </li>
                                    <li>
                                        Proven software development experience and Android skills development
                                    </li>
                                    <li>
                                        Proven working experience in Android app development
                                    </li>
                                    <li>
                                        Experience with Android SDK
                                    </li>
                                    <li>
                                        Experience working with remote data via REST and JSON
                                    </li>
                                    <li>
                                        Experience with third-party libraries and APIs
                                    </li>
                                    <li>
                                        Working knowledge of the general mobile landscape, architectures, trends, and emerging technologies
                                    </li>
                                    <li>
                                        Solid understanding of the full mobile development life cycle
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#IOSD" style="color: black">
                                <h4 class="panel-title">
                                    IOS DEVELOPER
                                </h4>
                            </a>
                        </div>
                        <div id="IOSD" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Design and build advanced applications for the iOS platform
                                    </li>
                                    <li>
                                        Collaborate with cross-functional teams to define, design, and ship new features
                                    </li>
                                    <li>
                                        Unit-test code for robustness, including edge cases, usability, and general reliability
                                    </li>
                                    <li>
                                        Work on bug fixing and improving application performance
                                    </li>
                                    <li>
                                        Continuously discover, evaluate, and implement new technologies to maximize development efficiency
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Bachelor’s Degree in Computer Science, Engineering, or other related subject
                                    </li>
                                    <li>
                                        Proven working experience in software development
                                    </li>
                                    <li>
                                        Working experience in iOS development
                                    </li>
                                    <li>
                                        Have published one or more iOS apps in the app store
                                    </li>
                                    <li>
                                        A deep familiarity with Objective-C and Cocoa Touch
                                    </li>
                                    <li>
                                        Experience working with iOS frameworks such as Core Data, Core Animation, Core Graphics, and Core Text
                                    </li>
                                    <li>
                                        Experience with third-party libraries and APIs
                                    </li>
                                    <li>
                                        Working knowledge of the general mobile landscape, architectures, trends, and emerging technologies
                                    </li>
                                    <li>
                                        Solid understanding of the full mobile development life cycle
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#BPA" style="color: black">
                                <h4 class="panel-title">
                                    BUSINESS PARTNERSHIP ASSOCIATE
                                </h4>
                            </a>
                        </div>
                        <div id="BPA" class="panel-collapse collapse">
                            <div class="panel-body">
                                <b>
                                    Job Desc.
                                </b>
                                <ul style="padding: inherit"> 
                                    <li>
                                        Explore and design program, event, or integration needed for partnership implementation
                                    </li>
                                    <li>
                                        Develop and engage partners negotiation and deals
                                    </li>
                                    <li>
                                        Deliver and documents the partnership
                                    </li>
                                    <li>
                                        Keep track on partnership executions
                                    </li>
                                    <li>
                                        Maintain and evaluate partners relation
                                    </li>
                                </ul>

                                <b>
                                    Job Req.
                                </b>
                                <ul style="padding: inherit">
                                    <li>
                                        Bachelor’s degree in any field.
                                    </li>
                                    <li>
                                        Experience in project management role
                                    </li>
                                    <li>
                                        Have an excellent interpersonal, time management, and communication skills
                                    </li>
                                    <li>
                                        Analytical thinker, creative, flexible, multi-tasker, proactive, and reliable
                                    </li>
                                    <li>
                                        Proficient in presentation making tools, words and number processing tools
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" style="padding-top: 25px;">
                <p style="margin-bottom: 10px;text-align: center; font-size: 20px">
                    Instructions: To apply, you MUST
                    <a href="/download/application-form">download</a> the form available below,
                    and send it along wih your CV to
                    <a href="mailto:career@kolega.co">career@kolega.co.</a>
                </p>
                <a href="/download/application-form"><h2 class="text-center"><span style="border: 2px solid orange;padding: 10px;color: #333;">DOWNLOAD APPLICATION FORM</span></h2></a>
            </div>
        </div>
    </div>
</section>

@endsection

@section('additionalJs')
<script type="text/javascript">
    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("a#anchor-section").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });

        var maxrow1 = Math.max($('#col-1-row-1').height(), $('#col-2-row-1').height(), $('#col-3-row-1').height());
        $('#col-1-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-2-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-3-row-1').css("min-height", function () {
            return maxrow1;
        });

        var maxrow2 = Math.max($('#col-1-row-2').height(), $('#col-2-row-2').height(), $('#col-3-row-2').height());
        $('#col-1-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-2-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-3-row-2').css("min-height", function () {
            return maxrow2;
        });

        var maxrow3 = Math.max($('#col-1-row-3').height(), $('#col-2-row-3').height(), $('#col-3-row-3').height());
        $('#col-1-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-2-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-3-row-3').css("min-height", function () {
            return maxrow3;
        });
    });

    $(window).resize(function () {
        $('#col-1-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-1-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-1-row-3').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-3').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-3').css("min-height", function () {
            return 0;
        });

        var maxrow1 = Math.max($('#col-1-row-1').height(), $('#col-2-row-1').height(), $('#col-3-row-1').height());
        $('#col-1-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-2-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-3-row-1').css("min-height", function () {
            return maxrow1;
        });

        var maxrow2 = Math.max($('#col-1-row-2').height(), $('#col-2-row-2').height(), $('#col-3-row-2').height());
        $('#col-1-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-2-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-3-row-2').css("min-height", function () {
            return maxrow2;
        });

        var maxrow3 = Math.max($('#col-1-row-3').height(), $('#col-2-row-3').height(), $('#col-3-row-3').height());
        $('#col-1-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-2-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-3-row-3').css("min-height", function () {
            return maxrow3;
        });
    });

</script>
@endsection
