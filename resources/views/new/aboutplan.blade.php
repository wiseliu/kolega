@extends('new/master')

@section('title',"Story and Plans")

@section('additionalCss')
<style>
    h1, h2, h3, h4, h5, h6 {
        color: #333;
        font-weight: 600;
        line-height: 1.5;
        margin: 0 0 10px 0;
        font-family: 'Raleway', sans-serif;
    }
    p{
        text-align: justify;
    }

    li{
        text-align: justify;
    }
</style>
@endsection

@section('content')
@include('new/header')

<section id="slider" class="boxed-slider">
    <div class="container-fluid nopadding clearfix">
        <div class="fslider" data-easing="easeInQuad">
            <div class="flexslider sliderbox-loc">
                <div class="slider-wrap">
                    <div class="slide" data-thumb="{{asset('/img/new/about-header.jpg')}}">
                        <img src="{{asset('/img/new/about-header.jpg')}}" alt="About Background">
                        <div class="col-md-4 col-md-offset-4 caption-box-title">
                            <h2 class="captiontitle">ABOUT KOLEGA</h2>
                            <p class="captiontitle">

                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="content">
    <div class="wrap">
        <div class="container clearfix" style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{asset('/img/new/about-side.jpg')}}" style="width:100%">
                </div>
                <div class="col-md-6">
                    <h2>WHY KOLEGA</h2>
                    <p>
                        Koléga Coworking Space is a place where our communities are based on
                        friendship in which you are able to grow, not only your business, but
                        also yourself as a person. In Koléga , friendship is embodied in working
                        alongside your partners, and those who share the workspace with you. Get
                        inspired by our unique design of workspace, and engage with all of the
                        members of Koléga from all locations in our exclusive community page.
                    </p>
                    <p>
                        <strong>Events</strong> for you, and your colleagues are in reach every month. To help you find new ways, both in growing your skills, and also your potential networks.<br><br>
                        <strong>Work</strong> amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works.<br><br>
                        <strong>Connect</strong> to the world with ease. Koléga coworking space offers you internet speed up to 30Mbps.
                    </p>
                    <button style="width: 100%; float: none;" href="#" class="button button-3d nomargin button-form-home-custom" id="template-contactform-submit">See Our Plans</button>
                    <hr>
                </div>
            </div>
        </div>
    </div>
</section>

<div id="section-plans">
    <section id="slider" class="boxed-slider">
        <div class="container-fluid nopadding clearfix">
            <div class="fslider" data-easing="easeInQuad">
                <div class="flexslider sliderbox-loc">
                    <div class="slider-wrap">
                        <div class="slide" data-thumb="{{asset('/img/new/plans-header.jpg')}}">
                            <img src="{{asset('/img/new/plans-header.jpg')}}" alt="Plans Background">
                            <div class="col-md-4 col-md-offset-4 caption-box-title">
                                <h2 class="captiontitle">PLANS & PRICING</h2>
                                <p class="captiontitle">

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <div class="container-fluid nopadding">
        <div style="padding-left: 7%; padding-right: 7%">
            <div class="col-md-4" style="padding: 5px">
                <div style="padding: 10px; background-color: #333333">
                    <img src="{{asset('/img/new/plan1.jpg')}}" style="max-height: 300px; margin: auto; display: block;">
                    <div class="row nopadding">
                        <h2 class="text-center text-uppercase" style="color: white;  margin: 0;margin-top: 10px;">Coworking Area</h2>
                    </div>
                </div>
                <div style="padding: 10px; background-color: #FF9935">
                    <p style="font-size: 20px;color: white;margin: 0;margin-top: 20px;">
                        from<br>
                        <b>IDR 1,300,000 a month</b><br>
                        per person
                    </p>
                </div>
                <div style="padding: 10px; background-color: #CCCCCC;margin-top: 10px;">
                    <h4>
                        <b>Workspace</b>
                    </h4>
                    <p id="col-1-row-1">
                        Join Kolega Coworking Space as member of the growing community. Access to the coworking area with
                        no assigned desk. Get a fresh new desk every day.<br><br>
                    </p>
                    <h4>
                        <b>Facilities</b>
                    </h4>
                    <ul id="col-1-row-2" style="margin-left: 30px;">
                        <li>12 hours a day access to preferred location</li>
                        <li>Special price to access any other locations</li>
                        <li>Fast Internet Connection (up to 100mbps)</li>
                        <li>Personal locker storage</li>
                        <li>100 sheets of free printing</li>
                        <li>Exclusive access to events by Kolega</li>
                        <li>Free mineral water</li>
                    </ul>
                    <h4>
                        <b>Available Locations</b>
                    </h4>
                    <p id="col-1-row-3">
                        This plan is available to use in all locations of Kolega Coworking Space
                    </p>
                    <a href="/contact"><button class="button" style="display: block;margin: auto;">Learn More</button></a>
                </div>
            </div>

            <div class="col-md-4" style="padding: 5px">
                <div style="padding: 10px; background-color: #333333">
                    <img src="{{asset('/img/new/plan2.jpg')}}" style="max-height: 300px; margin: auto; display: block;">
                    <div class="row nopadding">
                        <h2 class="text-center text-uppercase" style="color: white;  margin: 0;margin-top: 10px;">Dedicated Desks</h2>
                    </div>
                </div>
                <div style="padding: 10px; background-color: #FF9935">
                    <p style="font-size: 20px;color: white;margin: 0;margin-top: 20px;">
                        from<br>
                        <b>IDR 3,000,000 a month</b><br>
                        per person
                    </p>
                </div>
                <div style="padding: 10px; background-color: #CCCCCC;margin-top: 10px;">
                    <h4>
                        <b>Workspace</b>
                    </h4>
                    <p id="col-2-row-1">
                        Access to the coworking area and dedicated area with one assigned desk for
                        the period of this membership. Get together with your colleagues and fellow
                        coworkers, while having a personalized desk to organize your business
                    </p>
                    <h4>
                        <b>Facilities</b>
                    </h4>
                    <ul id="col-2-row-2" style="margin-left: 30px;">
                        <li>12 hours a day access to desk location</li>
                        <li>Special price to access any other locations</li>
                        <li>Customizable desk layout</li>
                        <li>Additional meeting room slot</li>
                        <li>Fast Internet Connection (up to 100mbps)</li>
                        <li>Personal locker storage</li>
                        <li>100 sheets of free printing</li>
                        <li>Exclusive access to events by Kolega</li>
                        <li>Free mineral water</li>
                    </ul>
                    <h4>
                        <b>Available Locations</b>
                    </h4>
                    <p id="col-2-row-3">
                        This plan is available to use in the locations below:<br><br>
                        Kolega Senopati<br>
                        Kolega Antasari<br>
                        Kolega Kuningan (Coming Soon)<br>
                        Kolega Jalan Satrio (Coming Soon)<br>
                        Kolega Radio Dalam (Coming Soon)<br>
                        Kolega Bandung (Coming Soon)
                    </p>
                    <a href="/contact"><button class="button" style="display: block;margin: auto;">Learn More</button></a>
                </div>
            </div>

            <div class="col-md-4" style="padding: 5px">
                <div style="padding: 10px; background-color: #333333">
                    <img src="{{asset('/img/new/plan3.jpg')}}" style="max-height: 300px; margin: auto; display: block;">
                    <div class="row nopadding">
                        <h2 class="text-center text-uppercase" style="color: white;  margin: 0;margin-top: 10px;">Office Suites</h2>
                    </div>
                </div>
                <div style="padding: 10px; background-color: #FF9935">
                    <p style="font-size: 20px;color: white;margin: 0;margin-top: 20px;">
                        from<br>
                        <b>IDR 8,000,000 a month</b><br>
                        per unit
                    </p>
                </div>
                <div style="padding: 10px; background-color: #CCCCCC;margin-top: 10px;">
                    <h4>
                        <b>Workspace</b>
                    </h4>
                    <p id="col-3-row-1">
                        Access to the selected, tailor-made, private office suite(s) for you and
                        your team, while being able to access the coworking area to huddle-up with
                        the community and colleagues.
                    </p>
                    <h4>
                        <b>Facilities</b>
                    </h4>
                    <ul id="col-3-row-2" style="margin-left: 30px;">
                        <li>12 hours a day access to office location</li>
                        <li>Special price to access any other locations</li>
                        <li>Fully Customizable private office</li>
                        <li>Additional meeting room slot</li>
                        <li>Fast Internet Connection (up to 100mbps)</li>
                        <li>Personal locker storage</li>
                        <li>100 sheets of free printing</li>
                        <li>Exclusive access to events by Kolega</li>
                        <li>Free mineral water</li>
                    </ul>
                    <h4>
                        <b>Available Locations</b>
                    </h4>
                    <p id="col-3-row-3">
                        This plan is available to use in the locations below:<br><br>
                        Kolega Senopati<br>
                        Kolega Antasari<br>
                        Kolega Kemang (Coming Soon)<br>
                        Kolega Kuningan (Coming Soon)<br>
                        Kolega Jalan Satrio (Coming Soon)<br>
                        Kolega Radio Dalam (Coming Soon)<br>
                        Kolega Bandung (Coming Soon)
                    </p>
                    <a href="/contact"><button class="button" style="display: block;margin: auto;">Learn More</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

@include('new/footer')

@section('additionalJs')
<script type="text/javascript">
    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("a#anchor-section").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 800, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
        
        var maxrow1 = Math.max($('#col-1-row-1').height(), $('#col-2-row-1').height(), $('#col-3-row-1').height());
        $('#col-1-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-2-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-3-row-1').css("min-height", function () {
            return maxrow1;
        });

        var maxrow2 = Math.max($('#col-1-row-2').height(), $('#col-2-row-2').height(), $('#col-3-row-2').height());
        $('#col-1-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-2-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-3-row-2').css("min-height", function () {
            return maxrow2;
        });

        var maxrow3 = Math.max($('#col-1-row-3').height(), $('#col-2-row-3').height(), $('#col-3-row-3').height());
        $('#col-1-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-2-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-3-row-3').css("min-height", function () {
            return maxrow3;
        });
    });

    $(window).resize(function () {
        $('#col-1-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-1').css("min-height", function () {
            return 0;
        });
        $('#col-1-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-2').css("min-height", function () {
            return 0;
        });
        $('#col-1-row-3').css("min-height", function () {
            return 0;
        });
        $('#col-2-row-3').css("min-height", function () {
            return 0;
        });
        $('#col-3-row-3').css("min-height", function () {
            return 0;
        });
        
        var maxrow1 = Math.max($('#col-1-row-1').height(), $('#col-2-row-1').height(), $('#col-3-row-1').height());
        $('#col-1-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-2-row-1').css("min-height", function () {
            return maxrow1;
        });
        $('#col-3-row-1').css("min-height", function () {
            return maxrow1;
        });

        var maxrow2 = Math.max($('#col-1-row-2').height(), $('#col-2-row-2').height(), $('#col-3-row-2').height());
        $('#col-1-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-2-row-2').css("min-height", function () {
            return maxrow2;
        });
        $('#col-3-row-2').css("min-height", function () {
            return maxrow2;
        });

        var maxrow3 = Math.max($('#col-1-row-3').height(), $('#col-2-row-3').height(), $('#col-3-row-3').height());
        $('#col-1-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-2-row-3').css("min-height", function () {
            return maxrow3;
        });
        $('#col-3-row-3').css("min-height", function () {
            return maxrow3;
        });
    });

</script>
@stop
@stop