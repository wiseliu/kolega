@extends('new/master')

@section('title',"Booking")

@section('content')
    @include('new/header')

    <section id="content">
        <div class="wrap">
            <div class="container clearfix" style="padding-bottom: 50px;">

                <div class="row section-form-singleloc">

                    <div class="row" style="padding-top: 25px;">
                        <h2 style="text-align: center">BOOK A PLAN</h2>
                        <hr>
                    </div>

                    <div class="col-md-12">
                        @if(Session::get('action') == 'success')
                            <div class="alert alert-success" role="alert" style="font-size: 18px;">
                                {!! Session::get('msg') !!}
                            </div>
                        @elseif(count($errors))
                            <div class="alert danger-border" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                @foreach($errors->all() as $error)
                                    <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                @endforeach
                            </div>
                        @endif
                        <form class="nobottommargin form-home-custom" id="template-contactform" name="template-contactform" action="/booking" method="post">
                            <div class="col-md-6">
                                <input placeholder="Name *" type="text" id="template-contactform-name" name="name" value="{{Auth::user()->firstname." ".Auth::user()->lastname}}" class="sm-form-control required" required readonly />
                            </div>

                            <div class="col-md-6">
                                <input placeholder="Email *" type="email" id="template-contactform-email" name="email" value="{{Auth::user()->email}}" class="required email sm-form-control" required readonly />
                            </div>

                            <div class="clear"></div>

                            <div class="col-md-6" >
                                <select id="location" name="location" class="required sm-form-control" required>
                                    <option disabled selected>Select location *</option>
                                    @foreach($locations as $location)
                                        <option value="{{$location->id}}">{{$location->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-6">
                                <select id="plan" name="plan" class="required sm-form-control" required disabled>
                                    <option disabled selected>Select plan *</option>
                                    <option value="Coworking Area">Coworking Area</option>
                                    <option value="Dedicated Desk">Dedicated Desk</option>
                                    <option value="Office Suite">Office Suite</option>
                                </select>
                            </div>

                            <div class="clear"></div>

                            <div class="col-md-4 col-md-offset-4" style="margin-top: 50px">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button style="width: 100%;" class="button button-3d nomargin button-form-home-custom" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Submit</button>
                            </div>

                        </form>

                        <div class="clear"></div>
                        <div class="col-md-4 col-md-offset-4">
                            <p style="text-align: center; margin: 0px;">Or call us : (021) ⁠⁠⁠835 5216</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </section><!-- #content end -->

    @include('new/footer')
@stop

@section('additionalJs')
    <script>
        $("#location").change(function () {
            var val = $(this).find('option:selected').text().toLowerCase();
            if (val == "tebet") {
                $('#plan').enable();
                $("#plan").html("<option disabled selected>Select plan *</option><option value='Coworking Area'>Coworking Area</option>");
            } else if (val == "senopati") {
                $('#plan').enable();
                $("#plan").html("<option disabled selected>Select plan *</option><option value='Coworking Area'>Coworking Area</option><option value='Dedicated Desk'>Dedicated Desk</option> <option value='Office Suite'>Office Suite</option>");
            } else if (val == "antasari") {
                $('#plan').enable();
                $("#plan").html("<option disabled selected>Select plan *</option><option value='Coworking Area'>Coworking Area</option><option value='Office Suite'>Office Suite</option>");
            }
        });
    </script>
@endsection