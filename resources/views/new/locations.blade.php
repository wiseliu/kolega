@extends('new/master')

@section('title',"Locations")

@section('content')
    <section class="location">

        <div class="container">
            <div class="l-box">
                <div class="page-header">
                    <div class="page-header-name">LOCATION</div>
                    <h1 class="page-header-title">Perfect workspace <br>wherever you are.</h1>
                    <p>Experience a diverse community with over 600 members <br>and 9 places across Jakarta (and more to come!).</p>
                </div>
            </div>
        </div>

        <div class="bookplan">
            <div class="container">
                <div class="l-box">
                    <div class="jscroll">
                        <div class="container-scroll">
                            <div class="slide-bookplan">
                                <?php $i = 1; ?>
                                @foreach($locations as $location)
                                    <div class="slide-bookplan-item {{($i > 3 ? 'hidden' : '')}}">
                                        <img class="place" src="{{$location->photos->first()->url}}">
                                        <a class="location" href="/location/{{$location->slug}}">
                                            {{--<div class="name">{{$location->name}} {!! ($location->slug != 'antasari' && $location->slug != 'tebet' && $location->slug != 'senopati' ? '<br>(Coming Soon)' : '')!!}</div>--}}
                                            <div class="name">{{$location->name}}<br><br></div>
                                            <div class="address">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                <span>
                                                    {{$location->address}}
                                                </span>
                                            </div>
                                            @if($location->address != 'Coming soon')
                                                <div class="phone">
                                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                                    <span>{{$location->phone_number}}</span>
                                                </div>
                                                <div class="time">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    <span>
                                                        @if($location->slug == 'tebet')
                                                            Weekdays 10AM-9PM
                                                            <br>
                                                            Weekends 10AM-5PM
                                                        @else
                                                            Weekdays 9AM-9PM
                                                            <br>
                                                            Weekends 9AM-5PM
                                                        @endif
                                                    </span>
                                                </div>
                                            @endif

                                            <span class="btn"><img src="{{ URL::asset('img/new/ic-arrow-location.svg')}}"></span>
                                        </a>
                                    </div>
                                    <?php $i++; ?>
                                @endforeach


                            </div>
                        </div>
                        <div class="loadmore"><a href="javascript:loadMore()">Load more</a></div>
                        <div class="footer-form">
                            <form class="pure-form" action="contact/suggestLocation" method="post">
                                <div class="pure-g">
                                    <div class="pure-u-1 pure-u-md-1-24"></div>
                                    <div class="tagline pure-u-1 pure-u-md-1-3">
                                        <div class="l-box">
                                            <h2>We are expanding.</h2>
                                            <h3>Suggest our next location</h3>
                                            @if(Session::get('action') == 'suggest-success')
                                                <div class="alert alert-success" role="alert">
                                                    <i class="fa fa-lg fa-check-circle-o"></i> <strong>Thank you!</strong> Your suggestion has been sent to our admin.
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="pure-u-1 pure-u-md-14-24">
                                        <div class="l-box">
                                            @if(Auth::check())
                                                <div class="form-control">
                                                    <input class="pure-input-1" type="text" placeholder="NAME *" name="name" required="" maxlength="100" value="{{Auth::user()->firstname." ".Auth::user()->lastname}}">
                                                    @if ($errors->has('name'))
                                                        <span class="msg">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-control">
                                                    <input class="pure-input-1" type="text" placeholder="EMAIL *" name="email" required="" maxlength="100" value="{{Auth::user()->email}}">
                                                    @if ($errors->has('email'))
                                                        <span class="msg">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            @else
                                                <div class="form-control">
                                                    <input class="pure-input-1" value="{{old('name')}}" type="text" placeholder="NAME *" name="name" required="" maxlength="100" >
                                                    @if ($errors->has('name'))
                                                        <span class="msg">{{ $errors->first('name') }}</span>
                                                    @endif
                                                </div>
                                                <div class="form-control">
                                                    <input class="pure-input-1" value="{{old('email')}}" type="text" placeholder="EMAIL *" name="email" required="" maxlength="100" >
                                                    @if ($errors->has('email'))
                                                        <span class="msg">{{ $errors->first('email') }}</span>
                                                    @endif
                                                </div>
                                            @endif
                                            <div class="form-control">
                                                <input class="pure-input-1" type="text" value="{{old('location')}}" name="location" required="" placeholder="SUGGEST A LOCATION *">
                                                @if ($errors->has('location'))
                                                    <span class="msg">{{ $errors->first('location') }}</span>
                                                @endif
                                            </div>
                                            <div class="l-box">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button class="pure-input-1" type="submit">SUBMIT</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

@section('additionalJs')
    <script type="text/javascript">
        $('.jscroll').jscroll({
            contentSelector: '.container-scroll',
            autoTrigger: false
        });
        
        function loadMore() {
            $("div").removeClass("hidden");
            $(".loadmore").hide();
        }
    </script>
@endsection