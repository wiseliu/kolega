<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Kol&eacute;ga">

    <!--favicon icon-->
    <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

    <title>Kol&eacute;ga - @yield('title')</title>

    <link rel="stylesheet" href="{{ URL::asset('css/new/pure-min.css') }}">
    <!--[if lte IE 8]><!-->
{{--    <link rel="stylesheet" href="{{ URL::asset('css/new/grids-responsive-old-ie-min.css') }}">--}}
    <!--[endif]-->
    <!--[if gt IE 8]><!-->
    <link rel="stylesheet" href="{{ URL::asset('css/new/grids-responsive-min.css') }}">
    <!--<![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('css/new/style.css') }}">
    @yield('additionalCss')
</head>
<body class="stretched">
    <header class="header {{(Request::is('/') ? '' : 'grey')}}">
        <nav class="nav-main transparent pure-menu pure-menu-horizontal pure-menu-fixed">
            <div class="container">
                <a href="/" class="logo-kolega pure-menu-heading pure-menu-link"><img src="{{ URL::asset('img/new/logo-kolega.svg')}}"></a>
                <button class="hamburger hamburger--squeeze" type="button">
                  <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                  </span>
                </button>
                <ul class="pure-menu-list">
                    <li class="pure-menu-item"><a href="/community" class="pure-menu-link">Community</a></li>
                    <li class="pure-menu-item"><a href="/location" class="pure-menu-link">Locations</a></li>
                    <?php $locations = \App\Location::all() ?>
                    <li class="pure-menu-item"><a href="/plans" class="pure-menu-link">Plans</a></li>
                    <li class="pure-menu-item"><a href="/#aboutus" class="pure-menu-link">About Us</a></li>
                    @if(Auth::check())
                        <li class="pure-menu-item"><a href="/logout" class="pure-menu-link">Logout</a></li>
                    @else
                        <li class="pure-menu-item"><a href="/login" class="pure-menu-link">Login/Signup</a></li>
                    @endif

                    <li class="pure-menu-item">
                        <a href="https://www.facebook.com/kolegaco/" class="social"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://plus.google.com/u/1/105455213192384785868" class="social"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/kolegaco/" class="social"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="container">
            <div class="footer-form">
                <form class="pure-form" action="contact/save" method="post">
                    <div class="pure-g">
                        <div class="tagline pure-u-1 pure-u-md-1-3">
                            <div class="l-box">
                                <h2>Get in touch, <br>be our colleague.</h2>
                                @if(Session::get('action') == 'success')
                                    <div class="alert alert-success" role="alert">
                                        <i class="fa fa-lg fa-check-circle-o"></i> <strong>Success!</strong> Your message has been sent to our admin. Please wait until we reached you.
                                    </div>
                                @endif
                            </div>
                        </div>

                        <div class="pure-u-1 pure-u-md-1-3">
                            <div class="l-box">
                                @if(Auth::check())
                                    <div class="form-control">
                                        <input class="pure-input-1" type="text" placeholder="NAME *" name="name" required="" maxlength="100" value="{{Auth::user()->firstname." ".Auth::user()->lastname}}">
                                        @if ($errors->has('name'))
                                            <span class="msg">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-control">
                                        <input class="pure-input-1" type="text" placeholder="EMAIL *" name="email" required="" maxlength="100" value="{{Auth::user()->email}}">
                                        @if ($errors->has('email'))
                                            <span class="msg">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-control">
                                        <input class="pure-input-1" type="text" placeholder="COMPANY *" name="company" required="" maxlength="100" value="{{Auth::user()->company}}">
                                        @if ($errors->has('company'))
                                            <span class="msg">{{ $errors->first('company') }}</span>
                                        @endif
                                    </div>
                                @else
                                    <div class="form-control">
                                        <input class="pure-input-1" value="{{old('name')}}" type="text" placeholder="NAME *" name="name" required="" maxlength="100" >
                                        @if ($errors->has('name'))
                                            <span class="msg">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-control">
                                        <input class="pure-input-1" value="{{old('email')}}" type="text" placeholder="EMAIL *" name="email" required="" maxlength="100" >
                                        @if ($errors->has('email'))
                                            <span class="msg">{{ $errors->first('email') }}</span>
                                        @endif
                                    </div>
                                    <div class="form-control">
                                        <input class="pure-input-1" value="{{old('company')}}" type="text" placeholder="COMPANY *" name="company" required="" maxlength="100" >
                                        @if ($errors->has('company'))
                                            <span class="msg">{{ $errors->first('company') }}</span>
                                        @endif
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-3">
                            <div class="l-box">
                                <div class="form-control">
                                    <div class="select-officesize select-style pure-input-1">
                                        <select name="office_size">
                                            <option disabled selected>OFFICE SIZE *</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                            <option value="9">9</option>
                                            <option value="10">10</option>
                                            <option value="11">10+</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('office_size'))
                                        <span class="msg select">{{ $errors->first('office_size') }}</span>
                                    @endif
                                </div>
                                <div class="form-control">
                                    <div class="select-location select-style pure-input-1">
                                        <select name="location" >
                                            <option disabled selected>LOCATION *</option>
                                            @foreach($locations as $location)
                                                <option value="{{$location->id}}">{{$location->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('location'))
                                        <span class="msg select">{{ $errors->first('location') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pure-g">
                        <div class="pure-u-1 pure-u-md-8-24"></div>
                        <div class="pure-u-1 pure-u-md-14-24">
                            <div class="l-box">
                                <div class="form-control">
                                    <input class="pure-input-1" type="text" value="{{old('question')}}" name="question" placeholder="PUT YOUR ENQUIRIES HERE">
                                    @if ($errors->has('question'))
                                        <span class="msg">{{ $errors->first('question') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="pure-u-1 pure-u-md-2-24">
                            <div class="l-box">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button class="pure-input-1" type="submit">SUBMIT</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="footer-credits">
                <div class="pure-g">
                    <div class="copyright pure-u-1 pure-u-md-1-4">
                        © Koléga 2017
                    </div>
                    <div class="tagline pure-u-1 pure-u-md-1-2">
                        Share the space, meet new people, make new friends, grow even bigger.
                    </div>
                    <div class="pure-u-1 pure-u-md-1-4">
                        <nav class="social pure-menu pure-menu-horizontal">
                            <ul class="pure-menu-list">
                                <li class="pure-menu-item"><a href="https://www.facebook.com/kolegaco/" class="pure-menu-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                <li class="pure-menu-item"><a href="https://plus.google.com/u/1/105455213192384785868" class="pure-menu-link"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                <li class="pure-menu-item"><a href="https://www.instagram.com/kolegaco/" class="pure-menu-link"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <script src="{{ URL::asset('js/new/jquery-3.2.1.min.js')}}"></script>
    <script src="{{ URL::asset('js/new/jquery-migrate-1.4.1.min.js')}}"></script>
    <script src="{{ URL::asset('js/new/slick.min.js')}}"></script>
    <script src="{{ URL::asset('js/new/classie.js')}}"></script>
    <script src="{{ URL::asset('js/new/enquire.min.js')}}"></script>
    <script src="{{ URL::asset('js/new/modj.js')}}"></script>
    <script>
        @if(Session::get('action') == 'success' || count($errors))
            $("html, body").animate({ scrollTop: $(document).height() }, "slow");
        @endif
    </script>

    @yield('additionalJs')
</body>