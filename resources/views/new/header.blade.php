<header id="header" class="full-header">
    <div id="header-wrap">
        <div class="container clearfix">
            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
            <div id="logo">
                <a href="/" class="standard-logo" data-dark-logo="{{ URL::asset('images/Logo-kolega-white.png') }}"><img src="{{ URL::asset('images/Logo-kolega-black.png') }}" alt="Kolega Logo"></a>
                <a href="/" class="retina-logo" data-dark-logo="{{ URL::asset('images/Logo-kolega-white.png') }}"><img src="{{ URL::asset('images/Logo-kolega-black.png') }}" alt="Kolega Logo"></a>
            </div>

            <nav id="primary-menu">
                <ul>
                    <li><a href="/community"><div>Community.</div></a></li>
                    <li><a href="/provent"><div>Projects & Events.</div></a></li>
                    <li><a href="/location"><div>Location.</div></a>
                        <ul>
                            <?php $locations = \App\Location::all() ?>
                            @foreach($locations as $location)
                                @if($location->slug == 'antasari' || $location->slug == 'tebet' || $location->slug == 'senopati')
                                    <li><a href="/location/{{$location->slug}}"><div>{{$location->name}}</div></a></li>
                                @else
                                    <li><a href="/location/{{$location->slug}}"><div>{{$location->name}} (Coming Soon)</div></a></li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                    <li><a id="anchor-section" href="/about#section-plans"><div>Plans.</div></a></li>
                    <li class=""><a href="javascript:void(0)">About Us</a>
                        <ul class="dropdown">
                            <li>
                                <a href="/about">
                                    About Us
                                </a>
                            </li>
                            <li>
                                <a href="/privacy-policy">
                                    Privacy Policy
                                </a>
                            </li>
                            <li>
                                <a href="/terms">
                                    Terms
                                </a>
                            </li>
                            <li>
                                <a href="/career">
                                    Career
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li><a href="/contact"><div>Contact us.</div></a></li>
                </ul>
            </nav>

            <div class="topbar-right topbar-overlay-home topbar-location">
                <a href="https://www.instagram.com/kolegaco/"><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a>
                <a href="https://www.facebook.com/kolegaco/"><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a>
                <a href="https://plus.google.com/u/1/105455213192384785868"><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a>

                @if(Auth::check())
                <a href="/logout">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/logout">Logout</a></span>
                @else
                <a href="/login">
                    <i style="margin-right: 7px;" class="fa fa-user fa-lg" aria-hidden="true"></i>
                </a>
                <span id="login-regis-home" style="float: right"><a href="/login">Login / Signup</a></span>
                @endif

            </div>
        </div>
    </div>
</header>