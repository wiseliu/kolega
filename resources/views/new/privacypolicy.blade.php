@extends('new/master')

@section('title',"Privacy Policy")

@section('additionalCss')
    <style>
        p{
            text-align: justify;
        }

        li{
            text-align: justify;
        }
    </style>
@endsection

@section('content')

<section class="location">
    <div class="wrap">
        <div class="container clearfix" style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <div class="col-md-12">
                    <h2>Your privacy is important</h2>
                    <p>
                        Preserving your privacy is important to us. Kolega is committed to maintaining your trust by protecting personal information that we collect and use.
                    </p>
                    <p>
                        This Privacy Policy (“Policy”) describes how PT. Kolega Coworking Indonesia and its affiliated companies and subsidiaries ("Kolega", “we” or “us”)
                        collect, use, and disclose your personal information in connection with any website, application, or other service that refers to or links to the
                        Policy (collectively, our “Services”).Please read the Policy carefully before you use our Services. You should not use our Services if you are not 
                        comfortable with what is stated below, and your continued use of Services constitutes consent to the practices we describe in this Policy and the
                        use and sharing of your information as provided herein. This Privacy Policy is governed and construed under the law of the Republic of Indonesia.
                    </p>
                    <p>
                        When the text of this Policy is available in multiple languages, the Indonesian version is the authoritative version, except where prohibited by applicable law.
                    </p>
                </div>
            </div>
            <div class="row" style="margin-top: 35px">
                <div class="col-md-12">
                    <h2>INFORMATION WE COLLECT</h2>
                    <p>
                        We collect various types of personal information in connection with the Services, namely:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Information you provide us directly;
                        </li>
                        <li>
                            Information we collect about your device(s) and your use of our Services, including through cookies, web beacons, and other internet technologies; and
                        </li>
                        <li>
                            Information we obtain from third-party sources.
                        </li>
                    </ul>
                    <br>

                    <h2>
                        Personal Information You Provide Us Directly
                    </h2>
                    <p>
                        We collect various types of information and content that you provide us directly. For example, in order to enroll as a member or to access some of our Services, you will be requested to provide us with your name, birth date, telephone number, email address, postal address, and where applicable company or organization name. We also collect other content and information that you provide us directly, including the content and information you may add to your member network or other community profile and photographs, video or other information or documentation you submit or provide us and the communications that you transmit through our networks. We collect billing and payment information you provide when you access some of our Services or purchase products and services. We also collect information that you provide us when you participate in our surveys, promotion or events. If you do not provide us with the requested information, we may be unable to deliver you the Services in full.
                    </p>
                    <p>
                        To the extent that you disclose to us any personal information of another individual, we assume that you have obtained such individual’s consent for the disclosure of such personal information as well as the processing of the same in accordance with the terms of this Policy.
                    </p>


                    <h2>
                        Information About Your Device(s) and Your Use of the Services
                    </h2>
                    <p>
                        We collect information about how you use our Services and, depending on the access permissions you have chosen for your device, other information, as specified below, from and about the computers, phones, and other devices where you install or access our Services. We use standard internet technologies, such as cookies and web beacons, to collect information about your computer or device and your online activity, as explained in more detail in the section on cookies.
                    </p>
                    <p>
                        The information we collect in this respect is:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Your browser type and operating system;
                        </li>
                        <li>
                            IP address and device identifiers;
                        </li>
                        <li>
                            Your browsing behavior on our Services, such as the amount of time spent viewing our online Services and the links you click within our online Services;
                        </li>
                        <li>
                            Websites you visit before or after our websites;
                        </li>
                        <li>
                            Whether you have opened or forwarded our e-mails or connected to offers or links that we send you; and
                        </li>
                        <li>
                            Your general or specific geographic location, such as through GPS, Bluetooth or WiFi signals to the extent permitted by the settings of your devices.
                        </li>
                    </ul>
                    <br>
                    <p>
                        If you use our internet connection, networks, telecommunications systems or information processing systems, your activity and any files or messages on those systems may also be monitored by Kolega at any time, in accordance with applicable law, for purposes of an investigation or to ensure compliance with company policies.
                    </p>


                    <h2>
                        Information From Third-Party Sources
                    </h2>
                    <p>
                        We obtain information about you from your company or organization, including when they may create or supplement your profile for you in the member network. You can review and amend this profile at any time.
                    </p>
                    <p>
                        We also receive information about you from publicly and commercially available sources and other third parties as permitted by law. We may combine this information with other information we receive from or about you, where necessary to provide the Services you requested.
                    </p>

                    <h2>
                        USE OF PERSONAL INFORMATION
                    </h2>
                    <p>
                        We use the personal information that we collect as necessary and appropriate for the following purposes:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            To provide our products and services. We use your personal information to provide you with the products, services, and features that you or your company or organization have requested; to respond to inquiries we receive from you or your company or organization; to verify your identity; in connection with a transaction that you or your company or organization has initiated; to deliver notifications and other operational communications; and for troubleshooting and others as necessary.
                        </li>
                        <li>
                            To improve our products and services and to analyze how users navigate and use our products and services and individual features.
                        </li>
                        <li>
                            To manage the performance of our products and services.
                        </li>
                        <li>
                            For audit and reporting purposes, to perform accounting and administrative tasks, and to enforce or manage legal claims.
                        </li>
                        <li>
                            To deliver advertising and promotional communications. For example, we may periodically contact you with offers and information about our products, services, features, and events; to send you newsletters or other information about topics that we believe may be of interest; to conduct online surveys; and to otherwise promote our products, services, features, and events. We also may deliver targeted advertisements to you, both on and off the Services.
                        </li>
                        <li>
                            For security and to protect, enforce, or defend legal rights, privacy, safety or property, whether our own or that of our employees or agents or others, and to enforce compliance with Kolega policies and to comply with applicable law and government requests.
                        </li>
                    </ul>
                    <br>
                    <h2>
                        SHARING OF PERSONAL INFORMATION
                    </h2>
                    <p>
                        We disclose your personal information in the following circumstances to the following parties:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Among Kolega companies/affiliated companies/subsidiary companies. Our services are provided by affiliates of PT. Kolega Coworking Indonesia depending on where you are located and the services you use. Kolega shares personal information with those affiliates for the purposes mentioned above. These affiliates use your information in accordance with this policy.
                        </li>
                        <li>
                            Our Community. Our Services include online and offline member communities, forums and networks that allow you to share and connect with others. We make this possible for Kolega members by creating a profile for new members that contains your name and the name of your company or other organization. This profile is created automatically and is accessible to other members of the forum or network, some of which include all Kolega members. You can supplement your profile by adding additional information about yourself and your company or organization and by posting content and comments and you may be able to share your profile with a broader audience.
                        </li>
                        <li>
                            Service Providers. We rely on third-party service providers to perform a variety of services on our behalf. For example, we may rely on service providers to host data and platforms, fulfill our product and service requests and answer your questions, send e-mails on our behalf, process payment card or other transactions, and analyze data to improve our products and services.
                        </li>
                        <li>
                            Other Parties When Required by Law and as Necessary to Provide and Protect Our Services. There may be instances when we disclose your information to other parties:
                        </li>
                        <li>
                            to provide you with the services you or your company or organization request, such as a disclosure of your information to a landlord in connection with a lease agreement, or to auditors or consultants;
                        </li>
                        <li>
                            to comply with the law or respond to legal process or a request for cooperation by a government entity or law enforcement;
                        </li>
                        <li>
                            to detect, suppress, and prevent fraud or verify and enforce compliance with the policies governing our Services; or
                        </li>
                        <li>
                            to protect our rights, property, and safety or that of any of our respective affiliates, business partners, customers or employees and where otherwise required by law.
                        </li>
                        <li>
                            Other Parties in Connection with a Corporate Transaction. We will disclose your personal information to an acquiror in the event we sell or transfer all or a portion of a business or assets to that third party, such as in connection with a merger or in the event of a bankruptcy reorganization or liquidation.
                        </li>
                        <li>
                            Third-Party Partners, With Your Consent. We may request your consent to share personal information about you with third parties so that they may provide you with special offers, promotional materials, and other materials that may be of interest to you.
                        </li>
                        <li>
                            Other Parties at Your Company’s or Organization’s Direction. In addition to the disclosures described in this Policy, we may share information about you with third parties when your company or organization requests such sharing. For example, we periodically may partner with third parties to make products or services available to individual members or participating companies and organizations. If you or your company or organization requests to participate, we may share your information with the relevant third party in connection with the requested product or service.
                        </li>
                        <li>
                            Aggregated and Non-Personal Information. We also share with third parties information in a manner that does not identify particular individuals, for example, information that has been aggregated with other records.
                        </li>
                    </ul>
                    <br>
                    <p>
                        Our Services contain links to other sites that we do not own or operate. We provide links to these third-party sites as a convenience to our members. They are not intended as an endorsement of or referral to the linked services. The linked services are subject to their separate and independent privacy statements, notices, and terms, which we recommend you read carefully. The collection, use, and disclosure of your personal information will be subject to the privacy policies of the third party and not this Policy.
                    </p>

                    <h2>
                        THIRD PARTIES THAT PROVIDE CONTENT OR FUNCTIONALITY ON OUR SERVICES
                    </h2>
                    <p>
                        We also partner with certain third parties to collect, analyze, and use some of the personal information described in this Policy. For example:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Third-parties that provide features and functionality on the Services by means of plug-ins. Even if you do not click on or interact with social networking services or other plug-ins, they may collect information about you, such as your IP address and the pages that you view.
                        </li>
                        <li>
                            Advertising providers help us and our advertisers provide advertisements on our Services or elsewhere, including advertisements that are targeted based on your online behavior, and analytics companies help us measure and evaluate the usage of our services.
                        </li>
                        <li>
                            Other content providers may offer products and services on our Services and may operate contents, sweepstakes, or surveys on our Services.
                        </li>
                        <li>
                            These third parties collect or receive certain information about your use of our Services, including through the use of cookies, web beacons, and Internet technologies and this information may be collected over time and combined with information collected across different websites and online services.
                        </li>
                    </ul>
                    <br>
                    <p>
                        Some of these companies participate in industry-developed programs designed to provide consumers choices about whether to receive targeted advertising. Please visit the websites operated by the [∙] to learn more.
                    </p>


                    <h2>
                        INFORMATION SECURITY
                    </h2>
                    <p>
                        We have in place various procedures to preserve your information. Although we take such steps to protect your information, no security program is foolproof and thus we cannot guarantee the absolute security of your personal or other information.
                    </p>

                    <h2>
                        REVIEWING AND UPDATING YOUR INFORMATION
                    </h2>
                    <p>
                        In accordance with applicable law, you may have the right to access, update, or correct inaccuracies in your personal information in our custody and control, subject to certain exceptions prescribed by law. If you would like to access, review, or update your information, please e-mail <b>kolega.app@gmail.com</b>.
                    </p>


                    <h2>
                        ACCESSING, REVIEWING, AND UPDATING YOUR PERSONAL INFORMATION
                    </h2>
                    <p>
                        To the extent provided for under applicable law, you may have the right to access your personal information and to update or correct inaccuracies in your personal information in accordance with the nature of our certain aspects of the services.
                    </p>
                    <p>
                        If you would like to exercise any of these rights, please send an e-mail to <b>kolega.app@gmail.com</b> or a letter to <b>Jl. Tebet Raya No. 53C RT 003 RW 002, Tebet Timur, Tebet, Jakarta Selatan, DKI Jakarta, Indonesia 12810</b>.
                    </p>

                    <h2>
                        INTERNATIONAL TRANSFERS
                    </h2>
                    <p>
                        The Services are headquartered in the Republic of Indonesia. Please be aware that information you provide to us or that we obtain as a result of your use of the Services may be collected in your country and subsequently transferred to another country in accordance with applicable law. By using the Services, you consent to the collection, international transfer, storage, and processing of your information.
                    </p>

                    <h2>
                        COLLECTION OF INFORMATION FROM CHILDREN
                    </h2>
                    <p>
                        The Services are not directed to children under the age of 18 and we do not knowingly collect personal information from anyone under the age of 18. Persons over 18 but under 21 years of age should ask their parents for permission before using the Services or sending any information about themselves to anyone over the Internet.
                    </p>

                    <h2>
                        CHANGES TO THIS PRIVACY POLICY IN THE FUTURE
                    </h2>
                    <p>
                        Privacy laws and guidelines are part of a constantly changing environment. We reserve the right, at our discretion, to change, modify, add, or remove portions of this Policy at any time. We encourage you to review this Policy periodically to ensure that you are aware of our current privacy practices, although we may also elect to notify you by e-mail or by posting something on some or all of our online Services. Your continued use of our Services following any changes signifies your acceptance of these changes.
                    </p>

                    <h2>
                        QUESTIONS OR COMMENTS
                    </h2>
                    <p>
                        If you have any questions or comments regarding our Policy, please contact us at:
                    </p>
                    <p>
                        <b>
                            Legal, PT. Kolega Coworking Indonesia, Jl. Tebet Raya No. 53C RT 003 RW 002, Tebet Timur, Tebet, Jakarta Selatan, DKI Jakarta, Indonesia 12810 Email: kolega.app@gmail.com
                        </b>
                    </p>


                    <h2>
                        COOKIES, WEB BEACONS, MOBILE APPLICATION, AND OTHER INTERNET TECHNOLOGIES
                    </h2>
                    <p>
                        We, as well as certain third parties that provide content and other functionality on our Services, may use cookies, web beacons and other similar technologies on our online Services.
                    </p>

                    <h2>
                        Cookies 
                    </h2>
                    <p>
                        A cookie is a small file that may be stored on your computer or other device. A cookie enables the entity that put the cookie on your device to recognize it across different websites, services, devices, and browsing sessions.
                    </p>
                    <p>
                        When you use a web browser to access the Services, some browsers may allow you to configure your browser to accept all cookies, reject all cookies, or notify you when a cookie is sent. Click the “Help” menu of your browser to learn more about how to change your cookie preferences. The operating system of your device may contain additional controls for cookies. Please note that disabling cookies may affect your ability to access and use certain features of the Services. To learn more about cookies and how to manage them, please click here.
                    </p>

                    <h2>
                        Web Beacons
                    </h2>
                    <p>
                        Web beacons and similar technologies are small bits of code, which are embedded in web pages, ads, and e-mail, that communicate with third parties. We may use web beacons, for example, to count the number of users who have visited a particular web page, to deliver or communicate with cookies, and to understand usage patterns. We also may include web beacons in e-mails to understand whether messages have been opened, acted on, or forwarded.
                    </p>

                    <h2>
                        Mobile Application
                    </h2>
                    <p>
                        Mobile Application refer to app provided by Kolega Developer team at Google Play Store with the name of Kolega. This mobile application provided for customer as alternative facet to access functionality provided by Kolega website at <a href="http://kolega.co">www.kolega.co</a>. However, we don’t guarantee the exact same functionality between mobile application and website. we also collect user information on the mobile application to improve user experience on using the application. The data we collect on application includes:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Your browser type and operating system;
                        </li>
                        <li>
                            IP address and device identifiers;
                        </li>
                        <li>
                            Your general or specific geographic location, such as through GPS, Bluetooth or WiFi signals to the extent permitted by the settings of your devices.
                        </li>
                    </ul>
                    <br>
                    <p>
                        If you have any inquiries about our mobile application, please email us at: <b>kolega.app@gmail.com</b>
                    </p>

                    <h2>
                        Other Technologies
                    </h2>
                    <p>
                        There are other local storage and Internet technologies, such as Local Shared Objects (also referred to as “Flash cookies”) and HTML5 local storage, that operate similarly to the cookies discussed above in that they are stored on your device and can be used to store certain information about your activities and preferences across different services and sessions. Please note that these technologies are distinct from cookies, and you may not be able to control them using standard browser tools and settings. For information about disabling or deleting information contained in Flash cookies, please click here.
                    </p>

                    <h2>
                        How We Use These Technologies
                    </h2>
                    <p>
                        Our websites use these technologies for the following general purposes:
                    </p>
                    <ul style="margin-left: 50px">
                        <li>
                            Administering and improving our Services, including helping us measure and research the effectiveness of our content, features, advertisements, and other communications. For example, we measure which pages and features website visitors are accessing and how much time they are spending on our webpages. We may include web beacons in e-mails, for example, to understand whether messages have been opened, acted on, or forwarded.
                        </li>
                        <li>
                            Storing your sign-in credentials and preferences so that you don’t have to enter those credentials and preferences every time you log on to a Service.
                        </li>
                        <li>
                            Helping us and third parties provide you with relevant content and advertising by collecting information about your use of our Services and other websites.
                        </li>
                    </ul>
                    <br>
                    <p>
                        Please see “Use of Information” for details about how we use the information that we collect. By continuing to use the Services, you consent to our use of cookies, web beacons, and other Internet technologies as described above.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

