@extends('admin.master')

@section('title', 'User Credit Logs')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">User Management</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">User Credits and Subscription</a></li>
                        <li class="active">Logs</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">
        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left">
                            <h4 class="text-uppercase">{{$user->firstname." ".$user->lastname}}'s Credit Logs</h4>
                            @if ( !empty($user->subscribed_until) )
                                <a href="/admin/user/{{$user->id}}/creditlogs/exportCsv" class="btn btn-rounded btn-dark-solid">Export CSV</a>
                            @endif
                        </div>
                        @if ( empty($user->subscribed_until) )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> This user has not made any transactions.
                            </div>
                        @else

                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Date / Time</th>
                                    <th>Location</th>
                                    <th>Credit Amount</th>
                                    <th>Package</th>
                                    <th>Description</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $userLogs as $log )
                                    <tr class="{{$log["class"]}}">
                                        <td>{{$log["datetime"]}}</td>
                                        <td>{{$log["location"]}}</td>
                                        <td>{{$log["creditamount"]}}</td>
                                        <td>{{$log["package"]}}</td>
                                        <td>{!! $log["description"] !!}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showSubscribeDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            swal.withFormAsync({
                title: 'Subscription: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'month', name:'month', placeholder: 'How many months?', required: true, type: 'number' },
                    { id: 'location', name:'location', type: 'select', options: locationSelects }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        month: context.swalForm.month,
                        location: context.swalForm.location,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/subscribe',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "User's subscription has been updated!",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showTopUpDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            var packageSelects = [];
            @foreach($purchasePackages as $package)
                packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." @ IDR ".$package->price}}'});
            @endforeach

            swal.withFormAsync({
                title: 'Top Up: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Top Up',
                closeOnConfirm: true,
                formFields: [
                    { id: 'location', name:'location', type: 'select', options: locationSelects },
                    { id: 'ppackage', name:'ppackage', type: 'select', options: packageSelects }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        location: context.swalForm.location,
                        ppackage: context.swalForm.ppackage,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/topup',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "Top Up Success",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showUseCreditDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            locationSelects.push({value: '', text:'ALL'});
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            var packageSelects = [];
            @foreach($usagePackages as $package)
                @if($package->hour_duration != null)
                    packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." credits / ".$package->hour_duration." hours"}}'});
                @else
                    packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." credits"}}'});
                @endif
            @endforeach

            swal.withFormAsync({
                title: 'Use Credit: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                    formFields: [
                        { id: 'location', name:'location', type: 'select', options: locationSelects },
                        { id: 'upackage', name:'upackage', type: 'select', options: packageSelects }
                    ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        location: context.swalForm.location,
                        upackage: context.swalForm.upackage,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/usecredit',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "Package has been successfully bought",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }
    </script>
@stop
