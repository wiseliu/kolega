@extends('admin.master')

@section('title', 'Edit Blog')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Edit Blog</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">Edit</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Edit {{$blog->title}}</h4>
                        </div>

                        <form method="post" action="/admin/newsevent/{{$blog->id}}/edit" id="form" role="form" class="contact-comments" enctype="multipart/form-data">
                            @if(count($errors))
                                <div class="alert danger-border" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                    @endforeach
                                </div>
                            @elseif(Session::get('action') == 'success')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> Item successfully updated.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" value="{{$blog->title}}" name="title" id="title" class="form-control" placeholder="Title *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" value="{{$blog->slug}}" name="slug" id="slug" class="form-control" placeholder="Slug *" readonly maxlength="100" required="">
                                </div>

                                <div class="form-group col-md-12">
                                    <textarea name="content" id="content" class="cmnt-text form-control" rows="6" placeholder="Content *">{{$blog->content}}</textarea>
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" value="{{$blog->summary}}" name="summary" id="summary" class="form-control" placeholder="Summary *" maxlength="255" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" value="{{$blog->tags}}" name="tags" id="tags" class="form-control" placeholder="Tags (separated by comma)*" maxlength="255" required="">
                                </div>

                                <div class="col-md-12 form-group">
                                    @if($blog->is_active)
                                        <input type="checkbox" name="isactive" id="isactive" checked> Is Active
                                    @else
                                        <input type="checkbox" name="isactive" id="isactive"> Is Active
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    <p style="margin: 0px;">Picture * : </p>
                                    <label class="col-md-12"><input type="file" name="picture" /></label>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div class="portfolio col-6 gutter " style="margin-top: 0px">
                                                <div class="portfolio-item">
                                                    <div class="thumb">
                                                        <img src="{{ URL::asset($blog->picture_url) }}" alt="{{$blog->title}}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Send Button -->
                                <div class="form-group col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="reset" class="btn btn-small btn-dark-solid" style="float:right">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-small btn-dark-solid" style="float:right">
                                        Submit
                                    </button>
                                </div>

                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->
@stop

@section('additionalJs')
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
@stop