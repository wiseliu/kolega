@extends('admin.master')

@section('title', 'Credit Usage Package Management')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Credit Usage Package Management</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">Credit Management</a></li>
                        <li class="active">Usage Menu</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Add New Usage Menu</h4>
                        </div>

                        <form method="post" action="/admin/credit/usagemenu/add" id="form" role="form" class="contact-comments">
                            @if(count($errors))
                                <div class="alert danger-border" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                    @endforeach
                                </div>
                            @elseif(Session::get('action') == 'success')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> New usage menu successfully added.
                                </div>
                            @elseif(Session::get('action') == 'dsuccess')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> Usage menu successfully deleted.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name *" maxlength="255" required="">
                                </div>

                                <div class="form-group col-md-12">
                                    <textarea name="description" id="description" class="cmnt-text form-control" rows="6" placeholder="Description" maxlength="255"></textarea>
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="number" name="hour" id="hour" class="form-control" placeholder="Duration (hrs)">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="number" name="credit" id="credit" class="form-control" placeholder="Credit Amount *" required="">
                                </div>

                                <!-- Send Button -->
                                <div class="form-group col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="reset" class="btn btn-small btn-dark-solid" style="float:right">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-small btn-dark-solid" style="float:right">
                                        Submit
                                    </button>
                                </div>
                            </div>

                        </form>

                        <div class="divider d-solid d-single text-center">
                            <span class="dot"> </span>
                        </div>

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Usage Menu List</h4>
                        </div>
                        @if ( !$credits->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no usage menu, please add a new one.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Duration (hrs)</th>
                                    <th>Credit Amount</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $credits as $credit )
                                    <tr>
                                        <td>{{$credit->name}}</td>
                                        <td>{{$credit->hour_duration}}</td>
                                        <td>{{$credit->credit_amount}}</td>
                                        <td>{{$credit->description}}</td>
                                        <td>
                                            <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditDialog('{{json_encode($credit)}}')">Edit</a>
                                            <form method="post" action="/admin/credit/usagemenu/{{$credit->id}}/delete" style="float: left">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" class="btn btn-extra-small btn-rounded btn-dark-solid">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showEditDialog(credit){
            var credit = JSON.parse(credit);

            swal.withFormAsync({
                title: 'Edit '+credit.name,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'name', name:'name', placeholder: 'Name', required: true, value:credit.name },
                    { id: 'description', name:'description', placeholder: 'Description', value:credit.description },
                    { id: 'hour', name:'hour', placeholder: 'Duration (hrs)', value:credit.hour_duration },
                    { id: 'credit', name:'credit', placeholder: 'Credit Amount', required: true, value:credit.credit_amount }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        name: context.swalForm.name,
                        hour: context.swalForm.hour,
                        credit: context.swalForm.credit,
                        description: context.swalForm.description,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'PUT',
                        url: '/admin/credit/usagemenu/'+credit.id+'/edit',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "Usage menu has been updated!",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }
    </script>
@stop