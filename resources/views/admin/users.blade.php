@extends('admin.master')

@section('title', 'User Management')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">User Management</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">User List</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">
        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">User List</h4>
                        </div>
                        @if ( !$users->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no user registered.
                            </div>
                        @else
                            @if(Auth::user()->privilege == 'mainadmin')
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Verified</th>
                                        <th>Privilege</th>
                                        <th>Promo Code</th>
                                        <th>Code Used Count</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $users as $user )
                                        <tr>
                                            <td>{{$user->firstname." ".$user->lastname}}</td>
                                            <td>{{$user->email}}</td>
                                            @if($user->is_verified)
                                                <td>YES</td>
                                            @else
                                                <td>NO</td>
                                            @endif
                                            <td>{{$user->privilege}}</td>
                                            @if($user->promo_code != null)
                                                <td>{{$user->promo_code}}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>{{$user->promoCodes->count()}}</td>
                                            <td>
                                                <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;" onclick="showEditDialog('{{json_encode($user)}}')">Edit Privilege</a>
                                                <br>
                                                @if($user->promo_code != null)
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditKTPDialog('{{json_encode($user)}}')">Show / Edit KTP Number</a>
                                                @else
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showRegisterKTPDialog('{{json_encode($user)}}')">Register KTP & Generate Code</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @elseif(Auth::user()->privilege == 'admin')
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Verified</th>
                                        <th>Promo Code</th>
                                        <th>Code Used Count</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $users as $user )
                                        <tr>
                                            <td>{{$user->firstname." ".$user->lastname}}</td>
                                            <td>{{$user->email}}</td>
                                            @if($user->is_verified)
                                                <td>YES</td>
                                            @else
                                                <td>NO</td>
                                            @endif
                                            @if($user->promo_code != null)
                                                <td>{{$user->promo_code}}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>{{$user->promoCodes->count()}}</td>
                                            <td>
                                                @if($user->promo_code != null)
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditKTPDialog('{{json_encode($user)}}')">Show / Edit KTP Number</a>
                                                @else
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showRegisterKTPDialog('{{json_encode($user)}}')">Register KTP & Generate Code</a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @elseif(Auth::user()->privilege == 'subadmin')
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Verified</th>
                                        <th>Promo Code</th>
                                        <th>Code Used Count</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach( $users as $user )
                                        <tr>
                                            <td>{{$user->firstname." ".$user->lastname}}</td>
                                            <td>{{$user->email}}</td>
                                            @if($user->is_verified)
                                                <td>YES</td>
                                            @else
                                                <td>NO</td>
                                            @endif
                                            @if($user->promo_code != null)
                                                <td>{{$user->promo_code}}</td>
                                            @else
                                                <td>-</td>
                                            @endif
                                            <td>{{$user->promoCodes->count()}}</td>
                                            @if($user->promo_code != null)
                                                <td>
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditKTPDialog('{{json_encode($user)}}')">Show / Edit KTP Number</a>
                                                </td>
                                            @else
                                                <td>
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showRegisterKTPDialog('{{json_encode($user)}}')">Register KTP & Generate Code</a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            @endif
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showEditDialog(user){
            var user = JSON.parse(user);

            swal.withFormAsync({
                title: 'Edit Privilege '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'privilege', name:'privilege', placeholder: 'Privilege', required: true, value:user.privilege }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        privilege: context.swalForm.privilege,
                        _token: '{{csrf_token()}}'
                    }

                    $.ajax({
                        type: 'PUT',
                        url: 'user/'+user.id+'/editPrivilege',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "User has been updated!",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showRegisterKTPDialog(user){
            var user = JSON.parse(user);

            swal.withFormAsync({
                title: 'Register KTP '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Register & Generate Code',
                closeOnConfirm: true,
                formFields: [
                    { id: 'ktp', name:'ktp', placeholder: 'KTP Number', required: true },
                    { id: 'code', name:'code', placeholder: 'Referral Code', required: false }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        ktp: context.swalForm.ktp,
                        code: context.swalForm.code,
                        _token: '{{csrf_token()}}'
                    }

                    $.ajax({
                        type: 'PUT',
                        url: 'user/'+user.id+'/registerKTP',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "KTP registered successfully! CODE: "+data.code,
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showEditKTPDialog(user){
            var user = JSON.parse(user);

            swal.withFormAsync({
                title: 'Edit KTP Number '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'ktp', name:'ktp', placeholder: 'KTP Number', required: true, value:user.ktp_number }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        ktp: context.swalForm.ktp,
                        _token: '{{csrf_token()}}'
                    }

                    $.ajax({
                        type: 'PUT',
                        url: 'user/'+user.id+'/editKTP',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "User has been updated!",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }
    </script>
@stop
