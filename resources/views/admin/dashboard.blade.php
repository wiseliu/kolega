@extends('admin.master')

@section('title', 'Dashboard')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Dashboard</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="heading-title text-center">
                        <span id="date" class="text-uppercase"></span>
                        <h3 id="time"></h3>
                    </div>

                    <div class="col-md-6">
                        <!--info-->
                        <div class="alert alert-info" role="alert">
                            <strong>{{$count_registered_users}}</strong> users have registered.
                        </div>
                        <!--success-->
                        <div class="alert alert-success" role="alert">
                            <strong>{{$count_verified_users}}</strong> users have verified their email.
                        </div>
                    </div>

                    <div class="col-md-6">
                        <!--warning-->
                        <div class="alert alert-warning" role="alert">
                            <strong>{{$count_contacts}}</strong> questions or enquiries have been sent.
                        </div>
                        <!--danger-->
                        <div class="alert alert-danger" role="alert">
                            <strong>{{$count_booktours}}</strong> tours have been booked.
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>
    <!--body content end-->

@stop

@section('additionalJs')

    <script type="text/javascript">
        var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
        var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
        setInterval( function() {
            var newDate = new Date();
            newDate.setDate(newDate.getDate());
            $('#date').html(dayNames[newDate.getDay()] + ", " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

            // Create a newDate() object and extract the hours of the current time on the visitor's
            var hours = newDate.getHours();
            var minutes = newDate.getMinutes();
            var seconds = newDate.getSeconds();
            // Add a leading zero to the hours value
            $("#time").html(( hours < 10 ? "0" : "" ) + hours+":"+( minutes < 10 ? "0" : "" )+ minutes+":"+( seconds < 10 ? "0" : "" ) + seconds);
        }, 1000);
    </script>

@stop