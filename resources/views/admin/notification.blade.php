@extends('admin.master')

@section('title', 'Send Push Notification')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Send Push Notification</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">Push Notification</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Send Push Notification</h4>
                        </div>

                        <form method="post" action="/admin/notification/sendAll" id="form" role="form" class="contact-comments">
                            @if(count($errors))
                                <div class="alert danger-border" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                    @endforeach
                                </div>
                            @elseif(Session::get('action') == 'success')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> Notification successfully sent.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <input type="text" name="summary" id="summary" class="form-control" placeholder="Headline or message summary" maxlength="255" required="">
                                </div>
                                <div class="form-group col-md-12">
                                    <textarea name="message" id="message" class="cmnt-text form-control" rows="6" placeholder="Type the message you want to send.." maxlength="255"></textarea>
                                </div>

                                <!-- Send Button -->
                                <div class="form-group col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="reset" class="btn btn-small btn-dark-solid" style="float:right">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-small btn-dark-solid" onclick="return populatePriceList()" style="float:right">
                                        Send
                                    </button>
                                </div>

                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

@stop

@section('additionalJs')
    <script type="text/javascript">

    </script>
@stop