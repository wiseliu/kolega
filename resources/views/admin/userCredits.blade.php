@extends('admin.master')

@section('title', 'User Credits and Subscription')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">User Management</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">User Credits and Subscription</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">
        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">User Credits and Subscription</h4>
                        </div>
                        @if ( !$users->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no user registered.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Subscribed Until</th>
                                    <th>Usable Credit</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $users as $user )
                                    <tr>
                                        <td>{{$user->firstname." ".$user->lastname}}</td>
                                        <td>{{$user->email}}</td>
                                        @if($user->subscribed_until != null)
                                            <td>{{$user->subscribed_until}}</td>
                                        @else
                                            <td>-</td>
                                        @endif
                                        <?php
                                            $credit_permanent = 0;
                                            $credit_temporary = array();
                                            foreach ($user->creditAdditions as $creditAddition){
                                                if($creditAddition->is_permanent){
                                                    $credit_permanent += $creditAddition->amount;
                                                } else {
                                                    array_push($credit_temporary,array(
                                                        "amount" => $creditAddition->amount,
                                                        "valid_until" => $creditAddition->valid_until
                                                    ));
                                                }
                                            }

                                            foreach ($user->creditSubstractions as $creditSubstraction){
                                                foreach ($creditSubstraction->payments as $subtractionPayment){
                                                    if($subtractionPayment->is_taken_from_permanent_credit){
                                                        $credit_permanent -= $subtractionPayment->amount;
                                                    }else{
                                                        $substractAmount = $subtractionPayment->amount;
                                                        foreach ($credit_temporary as $key => $ctemp){
                                                            if($creditSubstraction->created_at <= $ctemp["valid_until"] && $ctemp["amount"] > 0){
                                                                if($ctemp["amount"] >= $substractAmount){
                                                                    $credit_temporary[$key]["amount"] -= $substractAmount;
                                                                    break;
                                                                } else{
                                                                    $substractAmount -= $ctemp["amount"];
                                                                    $credit_temporary[$key]["amount"] = 0;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            $usableCredit = $credit_permanent;
                                            foreach ($credit_temporary as $ctemp){
                                                if($ctemp['valid_until'] >= date('Y-m-d')){
                                                    $usableCredit += $ctemp["amount"];
                                                }
                                            }
                                        ?>
                                        <td>{{$usableCredit}}</td>
                                        <td>
                                            @if($user->subscribed_until != null)
                                                <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;" onclick="showSubscribeDialog('{{json_encode($user)}}')">Extend Subscription</a>
                                                @if($user->subscribed_until >= date("Y-m-d"))
                                                    <br>
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;margin-right: 2px" onclick="showTopUpDialog('{{json_encode($user)}}')">Top Up</a>
                                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;" onclick="showUseCreditDialog('{{json_encode($user)}}')">Use Credit</a>
                                                    <br>
                                                    <a href="/admin/user/{{$user->id}}/creditlogs" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;">View Log</a>
                                                @endif
                                            @else
                                                <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;margin-bottom: 1px;" onclick="showSubscribeDialog('{{json_encode($user)}}')">Subscribe</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showSubscribeDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            swal.withFormAsync({
                title: 'Subscription: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'month', name:'month', placeholder: 'How many months?', required: true, type: 'number' },
                    { id: 'location', name:'location', type: 'select', options: locationSelects }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        month: context.swalForm.month,
                        location: context.swalForm.location,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/subscribe',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "User's subscription has been updated!",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showTopUpDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            var packageSelects = [];
            @foreach($purchasePackages as $package)
                packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." @ IDR ".$package->price}}'});
            @endforeach

            swal.withFormAsync({
                title: 'Top Up: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Top Up',
                closeOnConfirm: true,
                formFields: [
                    { id: 'location', name:'location', type: 'select', options: locationSelects },
                    { id: 'ppackage', name:'ppackage', type: 'select', options: packageSelects }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        location: context.swalForm.location,
                        ppackage: context.swalForm.ppackage,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/topup',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "Top Up Success",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }

        function showUseCreditDialog(user){
            var user = JSON.parse(user);

            var locationSelects = [];
            locationSelects.push({value: '', text:'ALL'});
            @foreach($locations as $location)
                locationSelects.push({value: '{{$location->id}}', text:'{{$location->name}}'});
            @endforeach

            var packageSelects = [];
            @foreach($usagePackages as $package)
                @if($package->hour_duration != null)
                    packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." credits / ".$package->hour_duration." hours"}}'});
                @else
                    packageSelects.push({value: '{{$package->id}}', text:'{{$package->name.": ".$package->credit_amount." credits"}}'});
                @endif
            @endforeach

            swal.withFormAsync({
                title: 'Use Credit: '+user.email,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                    formFields: [
                        { id: 'location', name:'location', type: 'select', options: locationSelects },
                        { id: 'upackage', name:'upackage', type: 'select', options: packageSelects }
                    ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        user_id: user.id,
                        location: context.swalForm.location,
                        upackage: context.swalForm.upackage,
                        _token: '{{csrf_token()}}'
                    };

                    $.ajax({
                        type: 'POST',
                        url: '/admin/user/credits/usecredit',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                        title: "Success",
                                        text: "Package has been successfully bought",
                                        type: "success",
                                        confirmButtonColor: "#222"
                                    },
                                    function(){
                                        location.reload();
                                    });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }
    </script>
@stop
