<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Kol&eacute;ga">

        <!--favicon icon-->
        <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

        <title>Kol&eacute;ga Admin - @yield('title')</title>

        <!--common style-->


        <link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/shortcodes/shortcodes.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.theme.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/default-theme.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/sweetalert.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/swal-forms.css') }}" rel="stylesheet">

        @yield('additionalCss')

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
        <script src="{{ URL::asset('js/respond.min.js') }}"></script>
        <![endif]-->
    </head>
    <body class="left-nav-fixed">

        <!--  preloader start -->
        <div id="tb-preloader">
            <div class="tb-preloader-wave"></div>
        </div>
        <!-- preloader end -->


        <div class="wrapper">

            <!--header start-->
            <header id="header" class=" header-full-width ">

                <div class=" light-header">

                    <div class="container">
                        <div id="massive-menu" class="menuzord">

                            <div class="logo-area">
                                <!--logo start-->
                                <a href="/" class="logo-brand">
                                    <img class="retina" src="{{ URL::asset('img/logo.png') }}" alt="Kolega logo"/>
                                </a>
                                <!--logo end-->
                            </div>

                            <!--mega menu start-->
                            <ul class="menuzord-menu pull-right ">
                                <li class="">
                                    <a href="/admin">
                                        Dashboard
                                    </a>
                                </li>
                                @if(Auth::user()->privilege == 'admin' || Auth::user()->privilege == 'mainadmin')
                                    <li><a href="#">Location Management</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/areas">Area</a></li>
                                            <li><a href="/admin/amenities">Amenities</a></li>
                                            <li><a href="#">Place</a>
                                                <ul class="dropdown">
                                                    <li><a href="/admin/places/add">Add New</a></li>
                                                    <li><a href="#">Edit</a>
                                                        <ul class="dropdown">
                                                            <?php $locations = \App\Location::all() ?>
                                                            @foreach($locations as $location)
                                                            <li><a href="/admin/places/edit/{{$location->id}}">{{$location->name}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                    <li><a href="#">Delete</a>
                                                        <ul class="dropdown">
                                                            @foreach($locations as $location)
                                                            <li><a onclick="showDeleteDialogMain({{$location->id}})">{{$location->name}}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li><a href="#">User Management</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/user">User List</a></li>
                                            <li><a href="/admin/user/credits">Credits</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Credit Management</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/credit/purchasemenu">Purchase Package Management</a></li>
                                            <li><a href="/admin/credit/usagemenu">Usage Package Mangement</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Project and Events</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/provent/">View All</a></li>
                                            <li><a href="/admin/provent/nonapproved">View Need Approval</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#">Blog</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/newsevent/add">Add</a></li>
                                            <li><a href="/admin/newsevent/show">Show</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="/admin/tourbooked">Show Tour Booked</a></li>
                                    <li><a href="/admin/contact">Show Contact Us</a></li>
                                    <li><a href="/admin/notification">Push Notifications</a></li>
                                @elseif(Auth::user()->privilege == 'subadmin')
                                    <li><a href="#">User Management</a>
                                        <ul class="dropdown">
                                            <li><a href="/admin/user">User List</a></li>
                                            <li><a href="/admin/user/credits">Credits</a></li>
                                        </ul>
                                    </li>
                                @endif
                            </ul>
                            <!--mega menu end-->
                        </div>
                    </div>
                </div>

                <div class="side-social-link clearfix visible-md visible-lg">
                    <a href="/">
                        <i class="fa fa-external-link"></i>
                    </a>

                    <a href="/logout">
                        <i class="fa fa-power-off"></i>
                    </a>
                </div>
            </header>
            <!--header end-->

            @yield('content')

        </div>


        <!-- Placed js at the end of the document so the pages load faster -->
        <script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/menuzord.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.flexslider-min.js') }}"></script>
        <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.isotope.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.countTo.js') }}"></script>
        <script src="{{ URL::asset('js/breakpoint.js') }}"></script>
        <script src="{{ URL::asset('js/smooth.js') }}"></script>
        <script src="{{ URL::asset('js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('js/imagesloaded.js') }}"></script>
        <script src="{{ URL::asset('js/sweetalert.min.js') }}"></script>
        <script src="{{ URL::asset('js/swal-forms.js') }}"></script>
        <!--common scripts-->
        <script src="{{ URL::asset('js/scripts.js?6') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
            var bodyclass = $("body").attr("class");
            $.BreakPoint({
            breakpoints: {
            tablet: {
            max: 768,
                    callback: function () {
                    $("body").removeClass(bodyclass);
                    }
            },
                    desktop: {
                    min: 769,
                            callback: function () {
                            $("body").addClass(bodyclass);
                            }
                    }
            }
            });
            });
            function showDeleteDialogMain(id){
            swal({
            title: "Are you sure?",
                    text: "You will not be able to recover this place",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#222",
                    confirmButtonText: "Yes",
                    closeOnConfirm: false
            },
                    function(){
                    var formData = {
                    _token: '{{csrf_token()}}'
                    }

                    $.ajax({
                    type: 'PUT',
                            url: '/admin/places/' + id + '/delete',
                            data: formData,
                            dataType: 'json',
                            success: function (data) {
                            if (data.success){
                            swal({
                            title: "Deleted",
                                    text: "Places has been deleted!",
                                    type: "success",
                                    confirmButtonColor: "#222"
                            },
                                    function(){
                                    location.reload();
                                    });
                            } else {
                            swal({
                            title: "Failed",
                                    text: "There is some problem when deleting this place.",
                                    type: "error",
                                    confirmButtonColor: "#222"
                            });
                            }
                            },
                            error: function (data) {
                            console.log('Error:', data);
                            }
                    });
                    });
            }
        </script>

        @yield('additionalJs')

    </body>
</html>

