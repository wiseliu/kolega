@extends('admin.master')

@section('title', 'Projects and Events Management')

@section('content')

<!--page title start-->
<section class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h4 class="text-uppercase">Projects and Events Management</h4>
                <ol class="breadcrumb">
                    <li><a href="#">Admin</a></li>
                    <li class="active">Project and Event Management</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--page title end-->

<!--body content start-->
<section class="body-content ">

    <div class="page-content">
        <div class="container">
            <div class="row">

                <div class="col-md-12">

                    <div class="heading-title-alt text-left ">
                        <h4 class="text-uppercase">Projects & Events List</h4>
                    </div>
                    @if ( !$provents->count() )
                    <div class="alert warning-border">
                        <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no project or event.
                    </div>
                    @else
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Type</th>
                                <th>User</th>
                                <th>Approved</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach( $provents as $provent )
                            <tr>
                                <td>{{$provent['name']}}</td>
                                <td>{{ucwords($provent['type'])}}</td>
                                <?php $user = App\User::find($provent['user_id']) ?>
                                <td>{{$user->firstname." ".$user->lastname}}</td>
                                @if($provent['approved'])
                                <td>YES</td>
                                <td>
                                    <a href="/provent/{{$provent['id']}}" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;">View</a>
                                </td>
                                @else
                                <td>NO</td>
                                <td>
                                    <a href="/provent/{{$provent['id']}}" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left;">View</a>
                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left; background-color: lightgreen" onclick="showApprovalDialog('{{$provent['name']}}',{{$provent['id']}})">Approve</a>
                                    <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left; background-color: lightcoral" onclick="showRejectDialog('{{$provent['name']}}',{{$provent['id']}})">Reject</a>
                                </td>
                                @endif

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

                </div>

            </div>
        </div>
    </div>
</section>
<!--body content end-->

<div id="loading">

</div>

@stop

@section('additionalJs')
<script type="text/javascript">
    function showApprovalDialog(provent_name, provent_id){
//    var provent = JSON.parse(provent);
    var provent_name = provent_name;
    var provent_id = provent_id;
    swal.withFormAsync({
    title: 'Approve ' + provent_name + '?',
            showCancelButton: true,
            confirmButtonColor: '#222',
            confirmButtonText: 'Approve',
            closeOnConfirm: true,
            formFields: [
            { id: 'provent_approval', type:'hidden', name:'provent_approval', required: true, value:1 }
            ]
    }).then(function (context) {

    if (context._isConfirm){

    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');
    var formData = {
    privilege: context.swalForm.privilege,
            _token: '{{csrf_token()}}'
    }

    $.ajax({
    type: 'PUT',
            url: '/admin/provent/' + provent_id + '/approve',
            data: formData,
            dataType: 'json',
            success: function (data) {
            if (data.success){
            swal({
            title: "Success",
                    text: "Provent has been approved!",
                    type: "success",
                    confirmButtonColor: "#222"
            },
                    function(){
                    location.reload();
                    });
            } else {
            var errorMessage = "";
            $.each(data.errors, function(index, value) {
            errorMessage += value + "\n";
            });
            swal({
            title: "Failed",
                    text: errorMessage,
                    type: "error",
                    confirmButtonColor: "#222"
            });
            }
            $(".tb-preloader-wave").fadeOut();
            $("#tb-preloader").delay(200).fadeOut("slow").remove();
            },
            error: function (data) {
            console.log('Error:', data);
            }
    });
    }

    })
    }
    
    function showRejectDialog(provent_name, provent_id){
//    var provent = JSON.parse(provent);
    var provent_name = provent_name;
    var provent_id = provent_id;
    swal.withFormAsync({
    title: 'Reject ' + provent_name + '?',
            showCancelButton: true,
            confirmButtonColor: '#222',
            confirmButtonText: 'Reject',
            closeOnConfirm: true,
            formFields: [
            { id: 'provent_approval', type:'hidden', name:'provent_approval', required: true, value:1 }
            ]
    }).then(function (context) {

    if (context._isConfirm){

    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');
    var formData = {
    privilege: context.swalForm.privilege,
            _token: '{{csrf_token()}}'
    }

    $.ajax({
    type: 'PUT',
            url: '/admin/provent/' + provent_id + '/reject',
            data: formData,
            dataType: 'json',
            success: function (data) {
            if (data.success){
            swal({
            title: "Success",
                    text: "Provent has been rejected!",
                    type: "success",
                    confirmButtonColor: "#222"
            },
                    function(){
                    location.reload();
                    });
            } else {
            var errorMessage = "";
            $.each(data.errors, function(index, value) {
            errorMessage += value + "\n";
            });
            swal({
            title: "Failed",
                    text: errorMessage,
                    type: "error",
                    confirmButtonColor: "#222"
            });
            }
            $(".tb-preloader-wave").fadeOut();
            $("#tb-preloader").delay(200).fadeOut("slow").remove();
            },
            error: function (data) {
            console.log('Error:', data);
            }
    });
    }

    })
    }
</script>
@stop