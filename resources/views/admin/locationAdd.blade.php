@extends('admin.master')

@section('title', 'Add Place')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Add Place</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">Location Management</a></li>
                        <li><a href="#">Place</a></li>
                        <li class="active">Add New</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Add New Place</h4>
                        </div>

                        <form method="post" action="/admin/places/add" id="form" role="form" class="contact-comments" enctype="multipart/form-data">
                            @if(count($errors))
                                <div class="alert danger-border" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                    @endforeach
                                </div>
                            @elseif(Session::get('action') == 'success')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> New place successfully added.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" name="slug" id="slug" class="form-control" placeholder="Slug *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="number" step=0.00000000001 name="latitude" id="latitude" class="form-control" placeholder="Latitude *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="number" step=0.00000000001 name="longitude" id="longitude" class="form-control" placeholder="Longitude *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="Phone Number *" maxlength="20" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <select class="form-control" name="area">
                                        @if ( !$areas->count() )
                                            <option disabled selected>No area found. Please add one first.</option>
                                        @else
                                            <option disabled selected>Area *</option>
                                            @foreach( $areas as $area )
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="col-md-12 form-group">
                                    <input type="text" name="address" id="address" class="form-control" placeholder="Address *" maxlength="255" required="">
                                </div>

                                <div class="form-group col-md-12">
                                    <textarea name="description" id="description" class="cmnt-text form-control" rows="6" placeholder="Description *" maxlength="255"></textarea>
                                </div>

                                <div class="form-group col-md-12">
                                    @if ( !$amenities->count() )
                                        <p style="margin: 0px;padding-left: 15px;">Amenities: <strong>Empty. Please add one first.</strong></p>
                                    @else
                                        <p style="margin: 0px;padding-left: 15px;">Amenities * : </p>
                                        @foreach( $amenities as $amenity )
                                            <label class="col-md-2"><input type="checkbox" name="amenities[]" value="{{$amenity->id}}"> {{$amenity->name}}</label>
                                        @endforeach
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    <p style="margin: 0px;padding-left: 15px;">Price List * : </p>
                                    <a class="btn btn-extra-small btn-rounded btn-dark-solid" onclick="showAddDialog()" style="margin-left: 15px">Add New</a>
                                    <table class="table table-hover" id="price-list-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Price Text</th>
                                            <th>Icon</th>
                                            <th>Icon Note</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="form-group col-md-12">
                                    <p style="margin: 0px;padding-left: 15px;">Pictures * : </p>
                                    <label class="col-md-12"><input type="file" name="pictures[]" multiple/></label>
                                </div>

                                <!-- Send Button -->
                                <div class="form-group col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="priceList" value="">
                                    <button type="reset" class="btn btn-small btn-dark-solid" style="float:right">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-small btn-dark-solid" onclick="return populatePriceList()" style="float:right">
                                        Submit
                                    </button>
                                </div>

                            </div>

                        </form>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showAddDialog(){
            swal.withFormAsync({
                title: 'Add New Price List',
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Add',
                closeOnConfirm: true,
                formFields: [
                    { id: 'name', name:'name', placeholder: 'Name *', required: true },
                    { id: 'priceText', name:'priceText', placeholder: 'Price Text *', required: true },
                    { id: 'iconCode', name:'iconCode', placeholder: 'Icon Code *', required: true},
                    { id: 'iconNote', name:'iconNote', placeholder: 'Icon Note'}
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $("#price-list-table tbody").append(
                        "<tr>"+
                        "<td>"+context.swalForm.name+"</td>"+
                        "<td>"+context.swalForm.priceText+"</td>"+
                        "<td><i class='fa fa-lg "+context.swalForm.iconCode+" '></i></td>"+
                        "<td>"+context.swalForm.iconNote+"</td>"+
                        '<td><a class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditDialog(this)">Edit</a>' +
                        '<a class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="$(this).parent().parent().remove();">Delete</a></td>'+
                        "</tr>");
                }
            })
        }

        function showEditDialog(elem){
            var par = $(elem).parent().parent(); //tr
            var tdName = par.children("td:nth-child(1)");
            var tdPriceText = par.children("td:nth-child(2)");
            var tdIconCode = par.children("td:nth-child(3)");
            var tdIconNote = par.children("td:nth-child(4)");

            swal.withFormAsync({
                title: 'Edit Price List',
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'name', name:'name', placeholder: 'Name *', required: true, value: tdName.html() },
                    { id: 'priceText', name:'priceText', placeholder: 'Price Text *', required: true, value: tdPriceText.html() },
                    { id: 'iconCode', name:'iconCode', placeholder: 'Icon Code *', required: true, value: tdIconCode.html().split(" ")[3]},
                    { id: 'iconNote', name:'iconNote', placeholder: 'Icon Note', value: tdIconNote.html()}
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    tdName.html(context.swalForm.name);
                    tdPriceText.html(context.swalForm.priceText);
                    tdIconCode.html("<i class='fa fa-lg "+context.swalForm.iconCode+" '></i>");
                    tdIconNote.html(context.swalForm.iconNote);
                }
            })
        }

        function populatePriceList(){
            var priceListData = "";

            $("#price-list-table tbody tr").each(function() {
                priceListData += $(this).children("td:nth-child(1)").html()+"~"+
                    $(this).children("td:nth-child(2)").html()+"~"+
                    $(this).children("td:nth-child(3)").html().split(" ")[3]+"~"+
                    $(this).children("td:nth-child(4)").html()+"|";
            });

            if(priceListData == ""){
                sweetAlert("Price List is empty", "Please fill in price list first!", "error");
                return false;
            } else {
                $( "input[name='priceList']" ).val(priceListData);
                return true;
            }
        }
    </script>
@stop