@extends('admin.master')

@section('title', 'Contacts')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Contacts</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">Contacts</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Contacts List</h4>
                        </div>
                        @if ( !$contacts->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no one contacted you.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Company</th>
                                    <th>Location</th>
                                    <th>Office Size</th>
                                    <th>Question</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $contacts as $contact )
                                    <tr>
                                        <td>{{$contact->name}}</td>
                                        <td>{{$contact->email}}</td>
                                        <td>{{$contact->company}}</td>
                                        <td>{{\App\Location::find($contact->location_id)->name}}</td>
                                        @if($contact->office_size > 10)
                                            <td>10+</td>
                                        @else
                                            <td>{{$contact->office_size}}</td>
                                        @endif
                                        <td>{{$contact->question}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

@stop

@section('additionalJs')

@stop