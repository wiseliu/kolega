@extends('admin.master')

@section('title', 'Show News & Event')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Show Blog</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">Blog</a></li>
                        <li class="active">Show</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Blog List</h4>
                        </div>
                        @if ( !$blogs->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no Blog, please add a new one.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Summary</th>
                                    <th>Active</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $blogs as $blog )
                                    <tr>
                                        <td>{{$blog->title}}</td>
                                        <td>{{$blog->slug}}</td>
                                        <td>{{$blog->summary}}</td>
                                        @if($blog->is_active)
                                            <td>YES</td>
                                        @else
                                            <td>NO</td>
                                        @endif
                                        <td>
                                            <a href="/admin/newsevent/edit/{{$blog->id}}" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left">Edit</a>
                                            <form method="post" action="/admin/newsevent/{{$blog->id}}/delete" style="float: left">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" class="btn btn-extra-small btn-rounded btn-dark-solid">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')

@stop