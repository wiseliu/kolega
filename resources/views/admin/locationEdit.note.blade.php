<div class="row">
    <div class="col-md-12">

        <div class="portfolio col-6 gutter " style="margin-top: 0px">
            @foreach($location_photos as $photo)
                <div class="portfolio-item">
                    <div class="thumb">
                        <img src="{{ URL::asset($photo->url) }}" alt="{{$photo->name}}">
                        <div class="portfolio-hover">
                            <div class="action-btn">
                                <a id="{{$photo->id}}" title="{{$photo->name}}" onclick="showDeleteAlert(this)"> <i class="fa fa-lg fa-close"></i>  </a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>