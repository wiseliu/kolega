@extends('admin.master')

@section('title', 'Tour Booked')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Tour Booked</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li class="active">Tour Booked</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Booked Tour List</h4>
                        </div>
                        @if ( !$book_tours->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no tour booked.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Location</th>
                                    <th>Visit Time</th>
                                    <th>Comment</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $book_tours as $book_tour )
                                    <tr>
                                        <td>{{$book_tour->name}}</td>
                                        <td>{{$book_tour->email}}</td>
                                        <td>{{\App\Location::find($book_tour->location_id)->name}}</td>
                                        <td>{{$book_tour->visit_time}}</td>
                                        <td>{{$book_tour->comment}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

@stop

@section('additionalJs')

@stop