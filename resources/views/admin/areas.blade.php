@extends('admin.master')

@section('title', 'Area')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Area</h4>
                    <ol class="breadcrumb">
                        <li><a href="#">Admin</a></li>
                        <li><a href="#">Location Management</a></li>
                        <li class="active">Area</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <div class="page-content">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Add New Area</h4>
                        </div>

                        <form method="post" action="areas/add" id="form" role="form" class="contact-comments">
                            @if(count($errors))
                                <div class="alert danger-border" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    @foreach($errors->all() as $error)
                                        <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                    @endforeach
                                </div>
                            @elseif(Session::get('action') == 'success')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> New area successfully added.
                                </div>
                            @elseif(Session::get('action') == 'dsuccess')
                                <div class="alert alert-success" role="alert">
                                    <i class="fa fa-lg fa-check-circle-o"></i> Area successfully deleted.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <!-- Name -->
                                    <input type="text" name="name" id="name" class="form-control" placeholder="Name *" maxlength="100" required="">
                                </div>

                                <div class="col-md-6 form-group">
                                    <!-- Email -->
                                    <input type="text" name="slug" id="slug" class="form-control" placeholder="Slug *" maxlength="100" required="">
                                </div>

                                <!-- Send Button -->
                                <div class="form-group col-md-12">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <button type="reset" class="btn btn-small btn-dark-solid" style="float:right">
                                        Reset
                                    </button>
                                    <button type="submit" class="btn btn-small btn-dark-solid" style="float:right">
                                        Submit
                                    </button>
                                </div>

                            </div>

                        </form>

                        <div class="divider d-solid d-single text-center">
                            <span class="dot"> </span>
                        </div>

                        <div class="heading-title-alt text-left ">
                            <h4 class="text-uppercase">Area List</h4>
                        </div>
                        @if ( !$areas->count() )
                            <div class="alert warning-border">
                                <i class="fa fa-lg fa-warning"></i> <strong>Empty!</strong> Currently there is no area, please add a new one.
                            </div>
                        @else
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $areas as $area )
                                    <tr>
                                        <td>{{$area->name}}</td>
                                        <td>{{$area->slug}}</td>
                                        <td>
                                            <a href="#" class="btn btn-extra-small btn-rounded btn-dark-solid" style="float: left" onclick="showEditDialog('{{json_encode($area)}}')">Edit</a>
                                            <form method="post" action="areas/{{$area->id}}/delete" style="float: left">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="DELETE" />
                                                <button type="submit" class="btn btn-extra-small btn-rounded btn-dark-solid">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--body content end-->

    <div id="loading">

    </div>

@stop

@section('additionalJs')
    <script type="text/javascript">
        function showEditDialog(area){
            var area = JSON.parse(area);

            swal.withFormAsync({
                title: 'Edit '+area.name,
                showCancelButton: true,
                confirmButtonColor: '#222',
                confirmButtonText: 'Save',
                closeOnConfirm: true,
                formFields: [
                    { id: 'name', name:'name', placeholder: 'Name', required: true, value:area.name },
                    { id: 'slug', name:'slug', placeholder: 'Slug', required: true, value:area.slug }
                ]
            }).then(function (context) {

                if(context._isConfirm){

                    $('#loading').append('<div id="tb-preloader"><div class="tb-preloader-wave"></div></div>');

                    var formData = {
                        name: context.swalForm.name,
                        slug: context.swalForm.slug,
                        _token: '{{csrf_token()}}'
                    }

                    $.ajax({
                        type: 'PUT',
                        url: 'areas/'+area.id+'/edit',
                        data: formData,
                        dataType: 'json',
                        success: function (data) {
                            if(data.success){
                                swal({
                                    title: "Success",
                                    text: "Area has been updated!",
                                    type: "success",
                                    confirmButtonColor: "#222"
                                },
                                function(){
                                    location.reload();
                                });
                            } else {
                                var errorMessage = "";
                                $.each(data.errors, function(index, value) {
                                    errorMessage += value+"\n";
                                });
                                swal({
                                    title: "Failed",
                                    text: errorMessage,
                                    type: "error",
                                    confirmButtonColor: "#222"
                                });
                            }
                            $(".tb-preloader-wave").fadeOut();
                            $("#tb-preloader").delay(200).fadeOut("slow").remove();
                        },
                        error: function (data) {
                            console.log('Error:', data);
                        }
                    });
                }

            })
        }
    </script>
@stop