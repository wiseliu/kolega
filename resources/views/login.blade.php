@extends('master')

@section('title', 'Sign in / Register')

@section('additionalCss')
    <link href="{{ URL::asset('css/foundation-datepicker.min.css') }}" rel="stylesheet">
@stop

@section('content')
    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">Sign in</h4>
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Sign in</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content">
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <dl class="accordion login-accordion">
                            <dt>
                                <a href="" id="login">Login your account</a>
                            </dt>
                            <dd>
                                <div class="login register ">
                                    <div class=" btn-rounded">

                                        @if(Session::get('action') == 'success')
                                            <div class="alert alert-success" role="alert">
                                                <i class="fa fa-lg fa-thumbs-o-up"></i> <strong>Congratulations!</strong> You are successfully registered, please open your email to validate your accounts.
                                            </div>
                                        @elseif(Session::get('action') == 'vsuccess')
                                            <div class="alert alert-success" role="alert">
                                                <i class="fa fa-lg fa-check-circle-o"></i> You have successfully verified your account. <strong>You can now login.</strong><br>Come to our location to get <strong>promotion codes</strong> for <strong>discounts up to 100%!</strong>
                                            </div>
                                        @elseif(Session::get('action') == 'socialite-fail')
                                            <div class="alert danger-border" role="alert">
                                                <i class="fa fa-lg fa-times-circle"></i> Sorry the information you provided is not sufficient, please try again or use other login method.
                                            </div>
                                        @elseif(count($errors) && Session::get('action') == 'login')
                                            <div class="alert danger-border" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                @foreach($errors->all() as $error)
                                                    <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                                @endforeach
                                            </div>
                                        @endif
                                        <form  action="login" method="post">

                                            <div class="form-group">
                                                <input type="email"  name="email" value="" class="form-control" placeholder="Email" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="password" name="password" value="" class="form-control " placeholder="Password" required>
                                            </div>

                                            <div class="form-group">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button class="btn btn-small btn-dark-solid full-width"  value="login">Login
                                                </button>
                                            </div>

                                            <div class="login-social-link">
                                                <a href="auth/facebook" class="facebook">
                                                    Facebook
                                                </a>
                                                <a href="auth/google" class="google">
                                                    Google
                                                </a>
                                            </div>

                                            <div class="form-group">
                                                <input type="checkbox" value="remember" id="checkbox1"> <label for="checkbox1">Remember  me</label>
                                                <a class="pull-right" data-toggle="modal" href="#forgotPass"> Forgot Password?</a>
                                            </div>

                                        </form>
                                    </div>

                                </div>
                            </dd>
                            <dt>
                                <a href="" id="register">not a member yet? register now</a>
                            </dt>
                            <dd>
                                @if(count($errors) && Session::get('action') == 'register')
                                    <div class="alert danger-border" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        @foreach($errors->all() as $error)
                                            <i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
                                        @endforeach
                                    </div>
                                @endif

                                <form class="login" action="register" method="post">

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <input type="email" name="email" class="form-control" placeholder="Email *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="text" name="phone_number" class="form-control" placeholder="Phone or Mobile *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <input type="password" name="password" class="form-control" placeholder="Password *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <input type="text" name="firstname" class="form-control" placeholder="First Name *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="text" name="lastname" class="form-control" placeholder="Last Name">
                                    </div>

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <select class="form-control" name="gender" required>
                                            <option disabled selected>Select gender *</option>
                                            <option value="M">Male</option>
                                            <option value="F">Female</option>
                                        </select>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="text" id="datepicker" name="date_of_birth" class="form-control" placeholder="Date of Birth *" readonly required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <input type="text" name="address" class="form-control" placeholder="Address *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="text" name="company" class="form-control" placeholder="Company">
                                    </div>

                                    <div class="form-group col-md-6" style="padding-right: 10px; padding-left: 0px">
                                        <input type="text" name="organization" class="form-control" placeholder="Organization *" required>
                                    </div>

                                    <div class="form-group col-md-6" style="padding-left: 10px; padding-right: 0px">
                                        <input type="text" name="job_title" class="form-control" placeholder="Job Title *" required>
                                    </div>

                                    <div class="form-group">
                                        <p style="margin: 0;">By clicking Register, you agree to the <a href="/terms">Terms of Use</a> and <a href="/privacy-policy">Privacy Policy</a> of Kolega Coworking Space</p>
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit"  class="btn btn-small btn-dark-solid full-width" value="Register">
                                    </div>

                                </form>
                            </dd>

                        </dl>
                    </div>
                </div>
            </div>

        </div>


    </section>
    <!--body content end-->


@stop

@section('additionalJs')
    <script src="{{ URL::asset('js/foundation-datepicker.min.js') }}"></script>
    <script type="text/javascript">
        $(function(){
            $('#datepicker').fdatepicker({
                format: 'yyyy-mm-dd',
                disableDblClickSelection: true
            });
        });
    </script>
    @if(Session::get('action') == 'register')
        <script type="text/javascript">
            $(document).ready(function(){
                var current = $('#register').parent().next("dd");
                $('#register').parents(".accordion").find("dt > a").removeClass("active");
                $('#register').addClass("active");
                $('#register').parents(".accordion").find("dd").slideUp("easeInExpo");
                $('#register').parent().next().slideDown("easeOutExpo");
            });
        </script>
    @endif
@stop