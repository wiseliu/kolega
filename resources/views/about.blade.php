@extends('master')

@section('title', 'About Us')

@section('content')
    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">ABOUT KOLEGA</h4>
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">About us</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content">
        <div class="container">
            <div class="row page-content">
                <div class="post-list-aside">
                    <div class="post-single">
                        <div class="col-md-6">
                            <div class="post-slider post-img text-center">
                                <ul class="slides">
                                    <li data-thumb="{{ URL::asset('img/about_0.jpg') }}">
                                        <a href="javascript:;"><img src="{{ URL::asset('img/about_0.jpg') }}" alt="Kolega About 1"></a>
                                    </li>
                                    <li data-thumb="{{ URL::asset('img/about_1.jpg') }}">
                                        <a href="javascript:;"><img src="{{ URL::asset('img/about_1.jpg') }}" alt="Kolega About 2"></a>
                                    </li>
                                    <li data-thumb="{{ URL::asset('img/about_2.jpg') }}">
                                        <a href="javascript:;"><img src="{{ URL::asset('img/about_2.jpg') }}" alt="Kolega About 3"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="">
                                <h4 class="text-uppercase">
                                    Exceptional working spaces
                                </h4>

                                <p>
                                    Kolega are bringing together of creative, community of entrepreneurs, freelancers and independents by providing them a better place to work. Kolega is equal parts inspiring shared working space, entrepreneurial incubator and a membership-based community of socially engaged people, co-working and co-learning. It is the place where freelancers, entrepreneurs, and other independent workers pay a fee to share a workspace and benefit from working in the presence of one another.
                                </p>

                                <p>
                                    Far too many freelancers and entrepreneurs are isolated in the confines of their own home and are forced to organically build their network from the ground up. Kolega co-working define the social gathering of a group of people, who are still working independently, but who share values, and interested in the synergy. Co-working solves this problem by offering a built in professional network.
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="feature-parallax dark">
            <div class="overlay-dark">
                <div class="container ">
                    <div class="row page-content">
                        <div class="col-md-3">
                            <div class="featured-item text-center">
                                <div class="icon theme-color">
                                    <i class="icon-clock"></i>
                                </div>
                                <div class="title text-uppercase">
                                    <h4>Working Hours</h4>
                                </div>
                                <div class="desc">
                                    Monday - Sunday
                                    <br/>
                                    09.00AM - 09.00PM
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="featured-item text-center">
                                <div class="icon theme-color">
                                    <i class="icon-phone"></i>
                                </div>
                                <div class="title text-uppercase">
                                    <h4>Phone</h4>
                                </div>
                                <div class="desc">
                                    +62 21 8355216
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="featured-item text-center">
                                <div class="icon theme-color">
                                    <i class=" icon-map"></i>
                                </div>
                                <div class="title text-uppercase">
                                    <h4>Location (HQ)</h4>
                                </div>
                                <div class="desc">
                                    LIMATIGA Building 3rd floor (above Comic Cafe). Jl. Tebet Raya no. 53. Jakarta 12810 INDONESIA
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="featured-item text-center">
                                <div class="icon theme-color">
                                    <i class=" icon-envelope"></i>
                                </div>
                                <div class="title text-uppercase">
                                    <h4>Email</h4>
                                </div>
                                <div class="desc">
                                    info@kolega.co
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="heading-title text-center">
                        <h3 class="text-uppercase">WE HAVE A FABULOUS TEAM </h3>
                        <span class="text-uppercase">Nam pulvinar vitae neque et porttitor. Praesent sed nisi eleifend. </span>
                    </div>

                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/team/t-s-1.jpg" alt="">
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>I love to desing</h4>
                                    <p>I love to introduce myself as a hardcore Web Designer.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="team-title">
                            <h5>Martin Smith</h5>
                            <span>founder &amp; ceo</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/team/t-s-2.jpg" alt="">
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>I love to desing</h4>
                                    <p>I love to introduce myself as a hardcore Web Designer.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="team-title">
                            <h5>Franklin Harbet</h5>
                            <span>HR Manager</span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="team-member">
                            <div class="team-img">
                                <img src="img/team/t-s-3.jpg" alt="">
                            </div>
                            <div class="team-hover">
                                <div class="desk">
                                    <h4>I love to desing</h4>
                                    <p>I love to introduce myself as a hardcore Web Designer.</p>
                                </div>
                                <div class="s-link">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="team-title">
                            <h5>Linda Anderson</h5>
                            <span>Marketing Manager</span>
                        </div>
                    </div>
                </div>

            </div>
        </div-->

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading-title text-center">
                            <h3 class="text-uppercase"> OUR ALUMNI & PARTNERS </h3>
                        </div>

                        <div id="clients-1">
                            <div class="item"><img src="{{ URL::asset('img/alumni/makesense.jpg') }}" alt="Kolega Alumni - MakeSense"></div>
                            <div class="item"><img src="{{ URL::asset('img/alumni/studenxceos.jpg') }}" alt="Kolega Alumni - Student X CEOs"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/cariruangan.png') }}" alt="Kolega Partner - Cari Ruangan"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/enlightconsulting.jpg') }}" alt="Kolega Partner - Enlight Consulting"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/klikonsul.png') }}" alt="Kolega Partner - Klikonsul"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/maubelajarapa.jpg') }}" alt="Kolega Partner - Mau Belajar Apa"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/ruangguru.png') }}" alt="Kolega Partner - Ruangguru"></div>
                            <div class="item"><img src="{{ URL::asset('img/partners/xwork.jpg') }}" alt="Kolega Partner - Xwork"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!--div class="divider d-border d-solid d-single text-right ">
            <i class="fa fa-xing"></i>
        </div>

        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading-title text-center">
                            <h3 class="text-uppercase"> OUR ALUMNI </h3>
                        </div>

                        <div id="clients-2">
                            <div class="item"><img src="{{ URL::asset('img/alumni/makesense.jpg') }}" alt="Kolega Alumni - MakeSense"></div>
                            <div class="item"><img src="{{ URL::asset('img/alumni/studenxceos.jpg') }}" alt="Kolega Alumni - Student X CEOs"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div-->

    </section>
    <!--body content end-->
@stop