@extends('master')

@section('title', 'Jakarta coworking space')

@section('content')
	<!--hero section-->
	<div class="banner-state vertical-align banner-p height-600">
		<div class="container-mid">
			<div class="container">
				<div class="row">
					<div class="col-md-6 ">
						<div class="banner-title" >
							<h1 class="text-uppercase m-bot-10 inline-block light-txt">Kol&eacute;ga</h1>
							<p class="p-top-20 light-txt">We are bringing together of creative, community of entrepreneurs, freelancers and independents by providing them a better place to work.</p>
							<a href="about" class="btn btn-medium btn-light-solid">Learn more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--hero section-->

	<!--body content start-->
	<section class="body-content ">

		<!--ads box-->
		<div class="featured-box pro-box">
			<div class="one-third">
				<div class="fit-img">
					<a href="login"><img src="{{ URL::asset('img/cover-location.jpg') }}" alt="Kolega Locations"/></a>
				</div>
			</div>
			<div class="one-third">
				<div class="fit-img">
					<a href="community"><img src="{{ URL::asset('img/cover-community.jpg') }}" alt="Kolega Community"/></a>
				</div>
			</div>
			<div class="one-third">
				<div class="fit-img">
					<a href="blog"><img src="{{ URL::asset('img/cover-newsevent.jpg') }}" alt="Kolega Blog"/></a>
				</div>
			</div>
		</div>
		<!--ads box-->
		<div class="page-content">
			<div class="container">

				<div class="heading-title-alt border-short-bottom text-center ">
					<h3 class="text-uppercase">Get in Touch</h3>
					<span class="text-uppercase">Be Our Colleague</span>
				</div>

				<div class="row">
					<div class="col-md-10 col-md-offset-1">
						@if(Session::get('action') == 'success')
							<div class="alert alert-success" role="alert">
								<i class="fa fa-lg fa-check-circle-o"></i> <strong>Success!</strong> Your message has been sent to our admin. Please wait until we reached you.
							</div>
						@elseif(count($errors))
							<div class="alert danger-border" role="alert">
								<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								@foreach($errors->all() as $error)
									<i class="fa fa-lg fa-times-circle"></i> {{ $error }}</br>
								@endforeach
							</div>
						@endif
						<form method="post" action="contact/save" id="form" role="form" class="contact-comments m-top-20">

							<div class="row">
								@if(Auth::check())
									<div class="col-md-6 form-group">
										<!-- Name -->
										<input type="text" value="{{Auth::user()->firstname." ".Auth::user()->lastname}}" name="name" id="name" class="form-control" placeholder="Name *" maxlength="100" required="">
									</div>

									<div class="col-md-6 form-group">
										<!-- Email -->
										<input type="email" value="{{Auth::user()->email}}" name="email" id="email" class="form-control" placeholder="Email *" maxlength="100" required="">
									</div>

									<div class="col-md-6 form-group">
										<!-- Company -->
										<input type="text" value="{{Auth::user()->company}}" name="company" id="company" class="form-control" placeholder="Company name" maxlength="100">
									</div>
								@else
									<div class="col-md-6 form-group">
										<!-- Name -->
										<input type="text" name="name" id="name" class="form-control" placeholder="Name *" maxlength="100" required="">
									</div>

									<div class="col-md-6 form-group">
										<!-- Email -->
										<input type="email" name="email" id="email" class="form-control" placeholder="Email *" maxlength="100" required="">
									</div>

									<div class="col-md-6 form-group">
										<!-- Company -->
										<input type="text" name="company" id="company" class="form-control" placeholder="Company name" maxlength="100">
									</div>
								@endif

								<div class="form-group col-md-6">
									<!-- Location -->
									<select class="form-control" name="location">
										<option disabled selected>Select location *</option>
										@foreach($locations as $location)
											<option value="{{$location->id}}">{{$location->name}}</option>
										@endforeach
									</select>
								</div>

								<div class="form-group col-md-6">
									<!-- office size -->
									<select class="form-control" name="office_size">
										<option disabled selected>Office size *</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">10+</option>
									</select>
								</div>

								<!-- Comment -->
								<div class="form-group col-md-12">
									<textarea name="question" id="text" class="cmnt-text form-control" rows="6" placeholder="Put your question or enquiries here." maxlength="400"></textarea>
								</div>

								<!-- Send Button -->
								<div class="form-group col-md-12">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<button type="submit" class="btn btn-small btn-dark-solid" style="float:right">
										Submit
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</section>
	<!--body content end-->
@stop
