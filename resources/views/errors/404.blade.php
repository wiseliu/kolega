@extends('master')

@section('title', '404 Page Not Found')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <h4 class="text-uppercase">Page Not Found</h4>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">404</li>
            </ol>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content ">

        <!--error-->
        <div class="page-content">
            <div class="container">
                <div class="row page-content">
                    <div class="col-md-5 text-center">
                        <div class="error-avatar">
                            <img src="{{URL::asset('img/error-avatar.png')}}" alt="Kolega 404 page not found"/>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="error-info">
                            <div class="error404">
                                404
                            </div>
                            <div class="error-txt">
                                We are very sorry<br/>
                                The page you’re looking for is not found.
                            </div>
                            <a href="/" class="btn btn-medium  btn-theme-color ">Take Me Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--error-->


    </section>
    <!--body content end-->
@stop