@extends('master')

@section('title', 'Blog')

@section('content')

    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <h4 class="text-uppercase">Blog</h4>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li class="active">Blog</li>
            </ol>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content">
        <div class="container">
            <div class="row ">

                @if(count($blogs))
                    <div class="portfolio portfolio-with-title portfolio-masonry blog-m col-3 gutter ">
                        @foreach($blogs as $blog)
                            <div class="portfolio-item ">
                                <div class="thumb">
                                    <img src="{{ URL::asset($blog->picture_url) }}" alt="{{$blog->title}}">
                                    <div class="portfolio-hover">
                                        <div class="action-btn">
                                            <a href="/blog/{{$blog->slug}}"> <i class="icon-basic_link"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-title">
                                    <h4><a href="/blog/{{$blog->slug}}">{{$blog->title}}</a></h4>
                                    <div class="date"><a>{{\App\User::find($blog->posted_by)->firstname." ".\App\User::find($blog->posted_by)->lastname}}</a>,  {{date('d F Y',strtotime( $blog->updated_at ))}}</div>
                                    <p>{{$blog->summary}}</p>
                                    <div class="m-top-20 m-bot-20"><a href="/blog/{{$blog->slug}}" class="btn btn-extra-small btn-dark-border btn-transparent"> Read More</a></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="heading-title-alt border-short-bottom m-top-50 m-bot-50 text-center ">
                        <span class="text-uppercase">Currently there is no blogs.</span>
                    </div>
                @endif
            </div>
        </div>
    </section>
    <!--body content end-->
@stop