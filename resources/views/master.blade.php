<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Kol&eacute;ga">

        <!--favicon icon-->
        <link rel="icon" type="image/png" href="{{ URL::asset('img/favicon.png') }}">

        <title>Kol&eacute;ga - @yield('title')</title>

        <!--common style-->
        <link href='http://fonts.googleapis.com/css?family=Abel|Source+Sans+Pro:400,300,300italic,400italic,600,600italic,700,700italic,900,900italic,200italic,200' rel='stylesheet' type='text/css'>
        <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/magnific-popup.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/shortcodes/shortcodes.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.carousel.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/owl.theme.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/blog.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/style-responsive.css') }}" rel="stylesheet">
        <link href="{{ URL::asset('css/default-theme.css') }}" rel="stylesheet">

        @yield('additionalCss')

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="{{ URL::asset('js/html5shiv.js') }}"></script>
        <script src="{{ URL::asset('js/respond.min.js') }}"></script>
    
    
        <![endif]-->
    </head>
    <body>
        <!--  preloader start -->
        <div id="tb-preloader">
            <div class="tb-preloader-wave"></div>
        </div>
        <!-- preloader end -->

        <div class="wrapper">

            <!--header start-->
            <header id="header" class=" nav-center-align">

                <div class="light-header">

                    <div class="container mainmenu">
                        <div id="massive-menu" class="menuzord">

                            <!--logo start-->
                            <a href="/" class="logo-brand">
                                <img class="retina" src="{{ URL::asset('img/logo.png') }}" alt="Kolega logo"/>
                            </a>
                            <!--logo end-->

                            <!--mega menu start-->
                            <ul class="menuzord-menu border-tb " id="menu-list">

                                <li class=""><a href="javascript:void(0)">Locations</a>
                                    <ul class="dropdown">
                                        <?php $locations = \App\Location::all() ?>
                                        @foreach($locations as $location)
                                        <li><a href="/location/{{$location->slug}}">{{$location->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </li>

                                <li class=""><a href="javascript:void(0)">About Us</a>
                                    <ul class="dropdown">
                                        <li>
                                            <a href="/about">
                                                About Us
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/privacy-policy">
                                                Privacy Policy
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <li class="">
                                    <a href="/community">
                                        Community
                                    </a>
                                </li>

                                <li id="middle-logo">
                                    <a href="/">
                                        <img class="retina" src="{{ URL::asset('img/logo.png') }}" alt=""/>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="/blog">
                                        Blogs
                                    </a>
                                </li>

                                <li class="">
                                    @if(Auth::check())
                                    <a href="/logout">
                                        Log out
                                    </a>
                                    @else
                                    <a href="/login">
                                        Sign in
                                    </a>
                                    @endif
                                </li>

                                <li class="">
                                    <a href="/contact">
                                        Contact Us
                                    </a>
                                </li>
                            </ul>
                            <!--mega menu end-->
                        </div>
                    </div>

                    <!--alternate menu appear start-->
                    <div class="menu-appear-alt" >
                        <div class="container">
                            <div id="massive-menu-alt" class="menuzord" >
                                <!--alternate menu appearing here ... -->
                            </div>
                        </div>
                    </div>
                    <!--alternate menu appear end-->

                </div>

            </header>
            <!--header end-->

            @yield('content')

            <!--footer start -->
            <footer id="footer" class="dark footer-4">
                <div class="container">

                    <div class="col-md-6">
                        <div class="copyright">
                            &copy; Kol&eacute;ga 2017.
                        </div>
                        <div class="copyright-sub-title text-uppercase">
                            A Place for Work, Collaboration, Meetings and Study
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="social-link pull-right circle " >
                            <a href="https://www.facebook.com/kolegaco/"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/kolegaco/"><i class="fa fa-instagram"></i></a>
                            <a href="https://plus.google.com/u/1/105455213192384785868"><i class="fa fa-google-plus"></i></a>
                        </div>
                    </div>


                </div>
            </footer>
            <!--footer end-->

        </div>

        <!-- Placed js at the end of the document so the pages load faster -->
        <script src="{{ URL::asset('js/jquery-1.10.2.min.js') }}"></script>
        <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
        <script src="{{ URL::asset('js/menuzord.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.flexslider-min.js') }}"></script>
        <script src="{{ URL::asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.isotope.js') }}"></script>
        <script src="{{ URL::asset('js/jquery.magnific-popup.min.js') }}"></script>
        <script src="{{ URL::asset('js/smooth.js') }}"></script>
        <script src="{{ URL::asset('js/wow.min.js') }}"></script>
        <script src="{{ URL::asset('js/imagesloaded.js') }}"></script>
        <!--common scripts-->
        <script src="{{ URL::asset('js/scripts.js?6') }}"></script>

        @yield('additionalJs')
    </body>
</html>