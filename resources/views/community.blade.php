@extends('master')

@section('title', 'Community')

@section('content')
    <!--page title start-->
    <section class="page-title">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-uppercase">COMMUNITY</h4>
                    <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li class="active">Community</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!--page title end-->

    <!--body content start-->
    <section class="body-content">
        <div class="page-content">
            <div class="container">

                <div class="heading-title-alt border-short-bottom text-center ">
                    <h3 class="text-uppercase">This page is still in development</h3>
                    <span class="text-uppercase">Please be patient while we work very hard to develop this page.</span>
                </div>
            </div>
        </div>

    </section>
    <!--body content end-->
@stop